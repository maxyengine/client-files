const
  TableBody = nrgsoft.widget.table.Body,
  DisplayColumns = nrgsoft.widget.table.body.DisplayColumns,
  AlignColumns = nrgsoft.widget.table.body.AlignColumns,
  AlignHeight = nrgsoft.widget.table.body.AlignHeight,
  ActiveItem = nrgsoft.widget.grid.trait.ActiveItem,
  ActiveItemByClick = nrgsoft.widget.grid.trait.ActiveItemByClick,
  ActiveItemStyle = nrgsoft.widget.grid.trait.ActiveItemStyle,
  OpenItemByKey = nrgsoft.widget.grid.trait.OpenItemByKey,
  NavigationByKey = nrgsoft.widget.grid.trait.NavigationByKey,
  Scrolling = nrgsoft.widget.grid.trait.Scrolling,
  HotKeys = nrgsoft.sdk.gui.trigger.Hotkeys,
  SelectedItemStyle = nrgsoft.widget.grid.trait.SelectedItemStyle,
  ItemSelector = nrgsoft.widget.grid.service.ItemSelector

nrgsoft.widget.grid.Table = class extends TableBody {

  _constructor (...args) {
    this.itemSelector = new ItemSelector()
    super._constructor(...args)
  }

  get traits () {
    return super.traits.concat([
      DisplayColumns,
      ActiveItem,
      AlignColumns,
      AlignHeight,
      ActiveItemByClick,
      ActiveItemStyle,
      SelectedItemStyle,
      OpenItemByKey,
      NavigationByKey, //? todo: remove Parent class (try to use children)
      Scrolling,
      HotKeys
    ])
  }

  onBeforeClear () {
    this.itemSelector.deselectAll()
  }
}
