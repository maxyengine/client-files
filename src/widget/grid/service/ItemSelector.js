const
  Component = nrgsoft.sdk.rx.Component

const items = Symbol()

nrgsoft.widget.grid.service.ItemSelector = class extends Component {

  _constructor (...args) {
    this[items] = new Set()

    super._constructor(...args)
  }

  get items () {
    return this[items]
  }

  get length () {
    return this.items.size
  }

  get isEmpty () {
    return !this.hasSelected
  }

  get hasSelected () {
    return !!this.items.size
  }

  select (item) {
    this[items].add(item)

    this.trigger('select', {item: item})
  }

  selectAll (items) {
    for (const item of items) {
      this.select(item)
    }
  }

  deselect (item) {
    this[items].delete(item)

    this.trigger('deselect', {item: item})
  }

  deselectAll () {
    for (const item of this) {
      this.deselect(item)
    }
  }

  isSelected (item) {
    return this[items].has(item)
  }

  toggleSelection (item) {
    this.isSelected(item) ? this.deselect(item) : this.select(item)
  }

  [Symbol.iterator] () {
    return this[items].values()
  }
}
