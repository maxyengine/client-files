const
  TypeObject = nrgsoft.sdk.lang.Object

nrgsoft.widget.grid.trait.SelectAll = class extends TypeObject {

  get checkbox () {
    return this.owner.selectorOfAll
  }

  get items () {
    return this.owner.selectableItems
  }

  get itemSelector () {
    return this.owner.itemSelector
  }

  initialize () {
    this.itemSelector.on(this)
    this.checkbox.on(this)
  }

  onChange () {
    const {value: checked} = this.checkbox

    if (checked && this.allSelected() || !checked && (this.allDeselected() || this.allSelected(1))) {
      return
    }

    if (checked) {
      this.itemSelector.selectAll(this.items)
    } else {
      this.itemSelector.deselectAll()
    }
  }

  onSelect () {
    if (this.allSelected()) {
      this.checkbox.value = true
    }
  }

  onDeselect () {
    if (this.allSelected(1)) {
      this.checkbox.value = false
    }
  }

  allSelected (except = 0) {
    return this.items.length - except === this.itemSelector.length
  }

  allDeselected () {
    return 0 === this.itemSelector.length
  }
}
