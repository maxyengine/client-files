const
  Observer = nrgsoft.sdk.rx.Observer

nrgsoft.widget.grid.trait.SelectedItemStyle = class extends Observer {

  get list () {
    return this.owner
  }

  get observeFor () {
    return this.list.itemSelector
  }

  onSelect ({item}) {
    this.list.getItemByRaw(item).classList.add('selected')
  }

  onDeselect ({item}) {
    this.list.getItemByRaw(item).classList.remove('selected')
  }
}
