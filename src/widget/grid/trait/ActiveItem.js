const
    Trigger = nrgsoft.sdk.rx.Trigger;

nrgsoft.widget.grid.trait.ActiveItem = class extends Trigger {

    get assignments() {
        return [
            'activeIndex',
            'activeItem',
            'hasActiveItem',
            'activateItem',
            'deactivateItem',
            'activateNextItem',
            'activatePrevItem',
            'activateFirstItem',
            'activateLastItem'
        ];
    }

    get defaults() {
        return {
            activeIndex: -1
        };
    }

    get hasActiveItem() {
        return this.activeIndex >= 0;
    }

    get activeItem() {
        return this.owner.getRaw(this.activeIndex);
    }

    onBeforeClear() {
        this.deactivateItem();
    }

    onSetData() {
        if (!this.owner.isEmpty && !this.hasActiveItem) {
            this.activateItem(0);
        }
    }

    activateItem(index) {
        this.deactivateItem();
        this.activeIndex = index;
        this.trigger('activateItem', {index: index});
    }

    deactivateItem() {
        if (this.hasActiveItem) {
            const index = this.activeIndex;
            this.activeIndex = -1;
            this.trigger('deactivateItem', {index: index});
        }
    }

    activateNextItem() {
        if (this.hasActiveItem && this.activeIndex < this.owner.length - 1) {
            this.activateItem(this.activeIndex + 1);
        }
    }

    activatePrevItem() {
        if (this.hasActiveItem && this.activeIndex > 0) {
            this.activateItem(this.activeIndex - 1);
        }
    }

    activateFirstItem() {
        if (!this.owner.isEmpty) {
            this.activateItem(0);
        }
    }

    activateLastItem() {
        if (!this.owner.isEmpty) {
            this.activateItem(this.owner.length - 1);
        }
    }
}
