const
    Observer = nrgsoft.sdk.rx.Observer;

nrgsoft.widget.grid.trait.ActiveItemStyle = class extends Observer {

    onActivateItem(event) {
        this.owner.getItem(event.index).classList.add('active');
    }

    onDeactivateItem(event) {
        this.owner.getItem(event.index).classList.remove('active');
    }
}
