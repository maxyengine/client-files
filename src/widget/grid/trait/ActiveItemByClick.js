const
    Observer = nrgsoft.sdk.rx.Observer;

nrgsoft.widget.grid.trait.ActiveItemByClick = class extends Observer {

    onAddItem(event) {
        const index = this.owner.lastIndex;

        event.item.element.addEventListener('click', () => {
            this.owner.activateItem(index);
        });
    }
}
