const
    Observer = nrgsoft.sdk.rx.Observer;

nrgsoft.widget.grid.trait.Scrolling = class extends Observer {

    onActivateItem(event) {
        const
            list = this.owner.element,
            item = this.owner.getItem(event.index).element,
            relativeTop = item.getBoundingClientRect().top - list.getBoundingClientRect().top,
            screenHeight = list.offsetHeight - item.offsetHeight;

        if (relativeTop < 0 || relativeTop >= screenHeight) {
            list.scrollTop = item.offsetTop - screenHeight + 5;
        }
    }
}
