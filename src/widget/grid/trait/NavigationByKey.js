const
    Observer = nrgsoft.sdk.rx.Observer,
    Parent = nrgsoft.fileManager.entity.Parent;

const toggleSelection = Symbol();

nrgsoft.widget.grid.trait.NavigationByKey = class extends Observer {

    get hotkeys() {
        return {
            nextItemKey: 40,
            prevItemKey: 38,
            firstItemKey: '37-Ctrl',
            lastItemKey: '39-Ctrl'
        };
    }

    get list() {
        return this.owner;
    }

    initialize() {
        super.initialize();

        this.list.hotkeys = this.list.hotkeys ? {...this.list.hotkeys, ...this.hotkeys} : this.hotkeys;
    }

    onNextItemKey(event) {
        if (event.shiftKey) {
            this[toggleSelection](this.list.activeIndex);
        }

        this.list.activateNextItem();
    }

    onPrevItemKey(event) {
        if (event.shiftKey) {
            this[toggleSelection](this.list.activeIndex);
        }

        this.list.activatePrevItem();
    }

    onFirstItemKey(event) {
        if (event.shiftKey) {
            for (const [index] of this.list) {
                if (index <= this.list.activeIndex) {
                    this[toggleSelection](index);
                }
            }
        }

        this.list.activateFirstItem();
    }

    onLastItemKey(event) {
        if (event.shiftKey) {
            for (const [index] of this.list) {
                if (index >= this.list.activeIndex) {
                    this[toggleSelection](index);
                }
            }
        }

        this.list.activateLastItem();
    }

    [toggleSelection](index) {
        const file = this.list.getRaw(index);

        if (file instanceof Parent) {
            return;
        }

        this.list.itemSelector.toggleSelection(file);
    }
}
