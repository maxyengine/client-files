const
  Trigger = nrgsoft.sdk.rx.Trigger

nrgsoft.widget.grid.trait.OpenItemByKey = class extends Trigger {

  static get services () {
    return {
      router: 'router'
    }
  }

  get hotkeys () {
    return {
      openKey: 'Enter'
    }
  }

  get list () {
    return this.owner
  }

  initialize () {
    super.initialize()

    this.owner.hotkeys = this.owner.hotkeys ? {...this.owner.hotkeys, ...this.hotkeys} : this.hotkeys
  }

  onOpenKey (event) {
    if (!this.owner.hasActiveItem) {
      return
    }

    const
      entity = this.list.getRaw(this.owner.activeIndex),
      openUrl = entity.openUrl

    if ('external' === openUrl.options.rel) {
      window.open(openUrl.href, openUrl.options.target)
    } else {
      event.stopTrigger = true

      const openEvent = {entity, params: {}, body: {}}
      this.trigger('beforeOpen', openEvent)

      this.router.goTo(openUrl, openEvent.params, openEvent.body)
    }
  }
}
