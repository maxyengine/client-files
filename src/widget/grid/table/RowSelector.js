const
  Component = nrgsoft.sdk.rx.Component,
  Template = nrgsoft.widget.grid.table.RowSelectorTpl

nrgsoft.widget.grid.table.RowSelector = class extends Component {

  get traits () {
    return [Template]
  }

  onToggleSelection () {
    this.itemSelector.toggleSelection(this.entity)
  }
}
