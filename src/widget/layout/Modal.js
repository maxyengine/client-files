const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftWidgetLayoutBehaviourActivateContent = nrgsoft.widget.layout.behaviour.ActivateContent,
    NrgsoftWidgetLayoutBehaviourAppend = nrgsoft.widget.layout.behaviour.Append,
    NrgsoftWidgetLayoutModalHeader = nrgsoft.widget.layout.modal.Header,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.widget.layout.Modal = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.widget.layout.behaviour.ActivateContent,
			nrgsoft.widget.layout.behaviour.Append
        ];
    }
                
    get _e2_() {
                
        if (!this.__e2_) {
            const
                properties = {},
                services = {
                    t: this.it.t
                };
            
            this.__e2_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.layout.modal.Header, properties, services) :
                new nrgsoft.widget.layout.modal.Header(properties, services);
        }

        return this.__e2_;
    
    }
                
    get title() {
                return this._e2_.title;
    }
                
    get content() {
                return this._content;
    }
    
    set title(value) {
                this._e2_.title = value;
    }
    
    set content(value) {
                this._content = Element.insert(this._e3, value);
                
        this.it.trigger('setContent', {value: value});
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = document.createElement('div');
            
        this._e1.classList.add('nrgsoft-widget-layout-Modal__window');
        this._element.appendChild(this._e1);
        this._e2 = this._e2_.element;
        this._e1.appendChild(this._e2);
        this._e3 = document.createElement('div');
            
        this._e3.classList.add('nrgsoft-widget-layout-Modal__content');
        this._e1.appendChild(this._e3);
            
    this._element.classList.add('nrgsoft-widget-layout-Modal');
    
        super._constructor(properties);
        this.it.mapEvent(this._e2_, 'close', 'close');
    
    }
}
