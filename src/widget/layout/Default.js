const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftWidgetLayoutBehaviourInsert = nrgsoft.widget.layout.behaviour.Insert,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.widget.layout.Default = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.widget.layout.behaviour.Insert
        ];
    }
                
    get content() {
                return this._content;
    }
    
    set content(value) {
                this._content = Element.insert(this._element, value);
                
        this.it.trigger('setContent', {value: value});
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
            
    this._element.classList.add('nrgsoft-widget-layout-Default');
    
        super._constructor(properties);
    
    }
}
