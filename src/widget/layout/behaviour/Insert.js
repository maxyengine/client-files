const
    Value = nrgsoft.sdk.lang.Value;

nrgsoft.widget.layout.behaviour.Insert = class extends Value {

    get assignments() {
        return [
            'show'
        ];
    }

    show(wrapper) {
        wrapper.insert(this.owner);
    }
}
