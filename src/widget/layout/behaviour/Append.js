const
    Value = nrgsoft.sdk.lang.Value;

nrgsoft.widget.layout.behaviour.Append = class extends Value {

    get assignments() {
        return [
            'show'
        ];
    }

    show(wrapper) {
        wrapper.append(this.owner);
    }
}
