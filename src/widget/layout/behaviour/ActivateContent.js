const
    Observer = nrgsoft.sdk.rx.Observer;

nrgsoft.widget.layout.behaviour.ActivateContent = class extends Observer {

    onSetContent(event) {
        event.value.activateWidget();
    }
}
