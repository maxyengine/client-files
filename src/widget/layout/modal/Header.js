const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.widget.layout.modal.Header = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
                
    get title() {
        return this._e2.nodeValue;
    }
    
    set title(value) {
        
        this._e2.nodeValue = this.it.t ? this.it.t(value) : value;
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = document.createElement('div');
            
        this._e1.classList.add('nrgsoft-widget-layout-modal-Header__title');
        this._element.appendChild(this._e1);
        this._e2 = document.createTextNode('');
        this._e1.appendChild(this._e2);
        this._e3 = document.createElement('div');
        this._element.appendChild(this._e3);
        this._e4 = document.createElement('span');
            
        this._e4.classList.add('nrgsoft-widget-layout-modal-Header__close');
        this._e3.appendChild(this._e4);
            
    this._element.classList.add('nrgsoft-widget-layout-modal-Header');
    
        super._constructor(properties);
        this.it.mapEvent(this._e4, 'click', 'close');
    
    }
}
