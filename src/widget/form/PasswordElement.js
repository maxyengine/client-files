const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkFormElement = nrgsoft.sdk.form.Element,
    NrgsoftSdkGuiWidgetAbility = nrgsoft.sdk.gui.WidgetAbility,
    NrgsoftWidgetFormFocus = nrgsoft.widget.form.Focus;

nrgsoft.widget.form.PasswordElement = class extends nrgsoft.sdk.form.Element {
        
    static get services() {
        return {
            ...nrgsoft.sdk.form.Element.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.sdk.gui.WidgetAbility,
			nrgsoft.widget.form.Focus
        ];
    }
                
    get isValid() {
                
    return this._element.classList.contains('is-valid');

    }
                
    get isRequired() {
                
    return this._element.classList.contains('is-required');

    }
                
    get label() {
        return this._e2.nodeValue;
    }
                
    get input() {
                return this._e3;
    }
                
    get value() {
                return this._e3.value;
    }
                
    get placeholder() {
                return this._e3.placeholder;
    }
                
    get errorMessage() {
        return this._e5.nodeValue;
    }
    
    set isValid(value) {
            if (undefined === value) {
        this._element.classList.remove('is-valid');
        this._element.classList.remove('is-invalid');
    } else if (value) {
        this._element.classList.remove('is-invalid');
        this._element.classList.add('is-valid');
    } else {
        this._element.classList.remove('is-valid');
        this._element.classList.add('is-invalid');
    }
        
    }
    
    set isRequired(value) {
            if (value) {
            this._element.classList.add('is-required');
        } else {
            this._element.classList.remove('is-required');
        }
        
    }
    
    set label(value) {
        
        this._e2.nodeValue = this.it.t ? this.it.t(value) : value;
    }
    
    set value(value) {
                this._e3.value = value;
    }
    
    set placeholder(value) {
                this._e3.placeholder = value;
    }
    
    set errorMessage(value) {
        
        this._e5.nodeValue = this.it.t ? this.it.t(value) : value;
                
        this.it.trigger('setErrorMessage', {value: value});
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._element.setAttribute('class', 'form-element');
        this._e1 = document.createElement('label');
        this._element.appendChild(this._e1);
        this._e2 = document.createTextNode('');
        this._e1.appendChild(this._e2);
        this._e3 = document.createElement('input');
        this._e3.setAttribute('type', 'password');
        this._element.appendChild(this._e3);
        this._e4 = document.createElement('div');
        this._e4.setAttribute('class', 'error-message');
        this._element.appendChild(this._e4);
        this._e5 = document.createTextNode('');
        this._e4.appendChild(this._e5);
            
    this._element.classList.add('nrgsoft-widget-form-PasswordElement');
    
        super._constructor(properties);
        this.isRequired = !!this.isRequired; 
    
    }
}
