const
  Value = nrgsoft.sdk.lang.Value

nrgsoft.widget.form.Focus = class extends Value {

  get assignments () {
    return [
      'focus'
    ]
  }

  focus () {
    this.owner.input.focus()
  }
}
