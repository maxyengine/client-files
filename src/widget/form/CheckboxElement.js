const
    Element = nrgsoft.sdk.form.Element,
    Template = nrgsoft.widget.form.checkboxElement.Tpl;

nrgsoft.widget.form.CheckboxElement = class extends Element {

    get traits() {
        return [Template];
    }

    get value() {
        return !!this.checked;
    }

    set value(value) {
        this.checked = !!value;

        this.trigger('change');
    }
}
