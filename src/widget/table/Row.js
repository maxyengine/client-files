const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkDataList = nrgsoft.sdk.data.List,
    NrgsoftSdkGuiWidgetAbility = nrgsoft.sdk.gui.WidgetAbility,
    NrgsoftSdkDataObserverVisualList = nrgsoft.sdk.data.observer.VisualList,
    NrgsoftWidgetTableColumn = nrgsoft.widget.table.Column;

nrgsoft.widget.table.Row = class extends nrgsoft.sdk.data.List {
        
    get itemClass() {
        return nrgsoft.widget.table.Column;
    }
            
    static get services() {
        return {
            ...nrgsoft.sdk.data.List.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.sdk.gui.WidgetAbility,
			nrgsoft.sdk.data.observer.VisualList
        ];
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('tr');
            
    this._element.classList.add('nrgsoft-widget-table-Row');
    
        super._constructor(properties);
    
    }
}
