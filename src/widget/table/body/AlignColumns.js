const
    Observer = nrgsoft.sdk.rx.Observer;

nrgsoft.widget.table.body.AlignColumns = class extends Observer {

    get list() {
        return this.owner;
    }

    initialize() {
        super.initialize();

        window.addEventListener('resize', () => {
            this.alignColumns();
        });
    }

    onSetData() {
        this.alignColumns();
    }

    alignColumns() {
        const header = this.list.header;

        header.table.style.width = this.owner.element.clientWidth + 'px';

        if (this.owner.isEmpty) {
            return;
        }

        for (const [index, column] of this.owner.lastItem) {
            const
                head = header.getItem(index).wrapper,
                col = column.wrapper;

            head.style.width = col.offsetWidth + 'px';
        }
    }
}
