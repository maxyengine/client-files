const
    Trigger = nrgsoft.sdk.rx.Trigger,
    Type = nrgsoft.sdk.lang.utility.Type;

nrgsoft.widget.table.body.DisplayColumns = class extends Trigger {

    get grid() {
        return this.owner;
    }

    onBeforeCreateItem(event) {
        this.trigger('beforeDisplayColumn', event);

        event.properties = {
            data: this.createColumnsData(event.properties)
        };
    }

    createColumnsData(contents) {
        const data = [];
        let colspan = 1;

        this.grid.fields.forEach(field => {
            if (Type.isSet(contents[field.name])) {
                data.push({
                    content: contents[field.name],
                    colspan: colspan
                });
                colspan = 1;
            } else if (data.length) {
                data[data.length - 1].colspan++;
            } else {
                colspan++;
            }
        });

        return data;
    }
}
