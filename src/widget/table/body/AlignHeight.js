const
    Observer = nrgsoft.sdk.rx.Observer;

nrgsoft.widget.table.body.AlignHeight = class extends Observer {

    static get services() {
        return {
            layouts: 'layouts'
        };
    }

    initialize() {
        super.initialize();

        window.addEventListener('resize', () => {
            this.alignHeight();
        });
    }

    onSetData() {
        this.alignHeight();
    }

    alignHeight() {
        let
            appWrapper = document.body === this.layouts.wrapper.element ? document.documentElement : this.layouts.wrapper.element,
            wrapper = appWrapper;

        if (this.owner.wrapper && this.owner.wrapper.element !== appWrapper) {
            wrapper = this.owner.wrapper.element;
            const wrapperHeight = appWrapper.clientHeight - wrapper.offsetTop;
            wrapper.style.height = wrapperHeight + 'px';
        }

        const
            element = this.owner.element,
            height = wrapper.clientHeight - element.offsetTop;

        element.style.height = height + 'px';
    }
}
