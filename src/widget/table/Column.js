const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.widget.table.Column = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
                
    get colspan() {
                return this._element.getAttribute('colspan');
    }
                
    get wrapper() {
                return this._e1;
    }
                
    get content() {
                return this._content;
    }
    
    set colspan(value) {
                this._element.setAttribute('colspan', value);
    }
    
    set content(value) {
                this._content = Element.insert(this._e1, value);
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('td');
        this._e1 = document.createElement('div');
        this._element.appendChild(this._e1);
            
    this._element.classList.add('nrgsoft-widget-table-Column');
    
        super._constructor(properties);
    
    }
}
