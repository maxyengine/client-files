const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkDataList = nrgsoft.sdk.data.List,
    NrgsoftSdkGuiWidgetAbility = nrgsoft.sdk.gui.WidgetAbility,
    NrgsoftSdkDataObserverVisualList = nrgsoft.sdk.data.observer.VisualList,
    NrgsoftWidgetTableRow = nrgsoft.widget.table.Row;

nrgsoft.widget.table.Body = class extends nrgsoft.sdk.data.List {
        
    get itemClass() {
        return nrgsoft.widget.table.Row;
    }
            
    static get services() {
        return {
            ...nrgsoft.sdk.data.List.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.sdk.gui.WidgetAbility,
			nrgsoft.sdk.data.observer.VisualList
        ];
    }
                
    get visualList() {
                return this._e1;
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = document.createElement('table');
        this._element.appendChild(this._e1);
            
    this._element.classList.add('nrgsoft-widget-table-Body');
    
        super._constructor(properties);
    
    }
}
