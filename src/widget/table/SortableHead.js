const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkDataSortableList = nrgsoft.sdk.data.SortableList,
    NrgsoftSdkGuiWidgetAbility = nrgsoft.sdk.gui.WidgetAbility,
    NrgsoftSdkDataObserverVisualList = nrgsoft.sdk.data.observer.VisualList,
    NrgsoftWidgetTableSortableHeadItemClass = nrgsoft.widget.table.sortableHead.ItemClass;

nrgsoft.widget.table.SortableHead = class extends nrgsoft.sdk.data.SortableList {
        
    static get services() {
        return {
            ...nrgsoft.sdk.data.SortableList.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.sdk.gui.WidgetAbility,
			nrgsoft.sdk.data.observer.VisualList,
			nrgsoft.widget.table.sortableHead.ItemClass
        ];
    }
                
    get fields() {
                return this.data;
    }
                
    get table() {
                return this._e1;
    }
                
    get visualList() {
                return this._e2;
    }
    
    set fields(value) {
                this.data = value;
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = document.createElement('table');
        this._element.appendChild(this._e1);
        this._e2 = document.createElement('tr');
        this._e1.appendChild(this._e2);
            
    this._element.classList.add('nrgsoft-widget-table-SortableHead');
    
        super._constructor(properties);
    
    }
}
