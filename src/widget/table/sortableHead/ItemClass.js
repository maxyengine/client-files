const
    Observer = nrgsoft.sdk.rx.Observer,
    Column = nrgsoft.widget.table.Column,
    SortableColumn = nrgsoft.widget.table.sortableHead.Column;

nrgsoft.widget.table.sortableHead.ItemClass = class extends Observer {

    onBeforeCreateItem(event) {
        event.itemClass = event.properties.sortable ? SortableColumn : Column;
    }
}
