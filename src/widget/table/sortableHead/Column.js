const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftWidgetBaseLink = nrgsoft.widget.base.Link,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.widget.table.sortableHead.Column = class extends nrgsoft.sdk.gui.Widget {
            
    asc() {
        this.direction = 'asc'; 
    }
                    
    desc() {
        this.direction = 'desc'; 
    }
                
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
                
    get sortable() {
                
    return this._element.classList.contains('sortable');

    }
                
    get direction() {
                
    for (let className of ["asc","desc"]) {
        if (this._element.classList.contains(className)) {
            return className;
        }        
    }

    }
                
    get wrapper() {
                return this._e1;
    }
                
    get _e2_() {
                
        if (!this.__e2_) {
            const
                properties = {},
                services = {
                    t: this.it.t
                };
            
            this.__e2_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.Link, properties, services) :
                new nrgsoft.widget.base.Link(properties, services);
        }

        return this.__e2_;
    
    }
                
    get url() {
                return this._e2_.url;
    }
                
    get content() {
                return this._e2_.content;
    }
    
    set sortable(value) {
            if (value) {
            this._element.classList.add('sortable');
        } else {
            this._element.classList.remove('sortable');
        }
        
    }
    
    set direction(value) {
                
    this._element.classList.remove(...["asc","desc"]);
     
    if (["asc","desc"].indexOf(value) > -1) {
        this._element.classList.add(value);
    }

    }
    
    set url(value) {
                this._e2_.url = value;
    }
    
    set content(value) {
                this._e2_.content = value;
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('td');
        this._e1 = document.createElement('div');
        this._element.appendChild(this._e1);
        this._e2 = this._e2_.element;
        this._e1.appendChild(this._e2);
            
    this._element.classList.add('nrgsoft-widget-table-sortableHead-Column');
    
        super._constructor(properties);
        this.sortable = !!this.sortable; 
        this.it.mapEvent(this._e2_, 'beforeGoTo', 'beforeGoTo');
    
    }
}
