const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkDataList2 = nrgsoft.sdk.data.List,
    NrgsoftSdkGuiWidgetAbility = nrgsoft.sdk.gui.WidgetAbility,
    NrgsoftSdkDataObserverVisualList = nrgsoft.sdk.data.observer.VisualList,
    NrgsoftWidgetTableColumn = nrgsoft.widget.table.Column;

nrgsoft.widget.table.Head = class extends nrgsoft.sdk.data.List {
        
    get itemClass() {
        return nrgsoft.widget.table.Column;
    }
                
    get fields() {
        return this.data;
    }
    
    set fields(value) {
        this.data = value;
        
    }
            
    get table() {
        return this._e1;
    }
            
    get visualList() {
        return this._e2;
    }
            
    static get services() {
        return {
            ...nrgsoft.sdk.data.List.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.sdk.gui.WidgetAbility,
			nrgsoft.sdk.data.observer.VisualList
        ];
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = document.createElement('table');
        this._element.appendChild(this._e1);
        this._e2 = document.createElement('tr');
        this._e1.appendChild(this._e2);
    
        super._constructor(properties);
    
    }
}
