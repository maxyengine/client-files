const
  List = nrgsoft.sdk.data.List,
  Link = nrgsoft.widget.base.Link,
  Template = nrgsoft.widget.menu.dropdown.Tpl,
  VisualList = nrgsoft.sdk.data.observer.VisualList

nrgsoft.widget.menu.Dropdown = class extends List {

  get itemClass () {
    return Link
  }

  get traits () {
    return [
      Template,
      VisualList
    ]
  }

  initialize () {
    document.addEventListener('click', event => {
      if (!this.contains(event.target)) {
        this.collapse()
      }
    }, true)
  }

  toggle () {
    this.expanded = this.collapsed
  }

  collapse () {
    this.collapsed = true
  }

  expand () {
    this.expanded = true
  }

  onToggle () {
    this.toggle()
  }

  onClear () {
    this.collapsed = true
  }

  onAddItem({item}) {
    item.element.addEventListener('click', () => {
      this.collapse()
    })
  }
}
