const
    Button = nrgsoft.widget.base.Button,
    Hotkeys = nrgsoft.sdk.gui.trigger.Hotkeys;

nrgsoft.widget.base.HotkeyButton = class extends Button {

    get traits() {
        return [
            Hotkeys
        ];
    }

    set clickHotkey(hotkey) {
        this.setHotkey('click', hotkey);
    }
}
