const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftWidgetBaseTraitLink = nrgsoft.widget.base.trait.Link,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.widget.base.Link = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.widget.base.trait.Link
        ];
    }
                
    get href() {
                return this._element.href;
    }
                
    get title() {
                return this._element.title;
    }
                
    get rel() {
                return this._element.rel;
    }
                
    get target() {
                return this._element.target;
    }
                
    get content() {
                return this._content;
    }
    
    set href(value) {
                this._element.href = value;
    }
    
    set title(value) {
                this._element.title = value;
    }
    
    set rel(value) {
                this._element.rel = value;
    }
    
    set target(value) {
                this._element.target = value;
    }
    
    set content(value) {
                this._content = Element.insert(this._element, value);
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('a');
            
    this._element.classList.add('nrgsoft-widget-base-Link');
    
        super._constructor(properties);
        this.it.mapEvent(this._element, 'click', 'click');
    
    }
}
