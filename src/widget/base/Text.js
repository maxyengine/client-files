const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.widget.base.Text = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
                
    get value() {
        return this._e1.nodeValue;
    }
    
    set value(value) {
        
        this._e1.nodeValue = this.it.t ? this.it.t(value) : value;
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = document.createTextNode('');
        this._element.appendChild(this._e1);
            
    this._element.classList.add('nrgsoft-widget-base-Text');
    
        super._constructor(properties);
    
    }
}
