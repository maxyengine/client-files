const
  Trigger = nrgsoft.sdk.rx.Trigger,
  Url = nrgsoft.sdk.web.Url,
  Type = nrgsoft.sdk.lang.utility.Type

const url = Symbol()

nrgsoft.widget.base.trait.Link = class extends Trigger {

  static get services () {
    return {
      ...Trigger.services || {},
      router: 'router'
    }
  }

  get assignments () {
    return [
      'url'
    ]
  }

  get link () {
    return this.owner
  }

  get url () {
    return this[url]
  }

  set url (value) {
    if (Type.isString(value)) {
      if (Url.isAbsolute(value)) {
        this[url] = new Url(value)
      } else {
        this[url] = this.router.createUrl(value)
      }
    } else {
      this[url] = value
    }

    if (this[url]) {
      this.link.set({
        href: this[url].href,
        ...this[url].options
      })
    }
  }

  onClick (nativeEvent) {
    const event = {
      nativeEvent: nativeEvent,
      link: this.link,
      params: {},
      body: {}
    }

    this.trigger('beforeGoTo', event)

    if ('external' !== this.link.rel) {
      nativeEvent.preventDefault()

      this.router.goTo(this.url, event.params, event.body)
    }
  }
}
