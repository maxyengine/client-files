const
    Link = nrgsoft.widget.base.Link,
    Hotkeys = nrgsoft.sdk.gui.trigger.Hotkeys;

nrgsoft.widget.base.HotkeyLink = class extends Link {

    get traits() {
        return [
            ...super.traits || [],
            Hotkeys
        ];
    }

    set clickHotkey(hotkey) {
        this.setHotkey('click', hotkey);
    }
}
