const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.widget.base.Button = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
                
    get title() {
                return this._element.title;
    }
                
    get type() {
                return this._element.type;
    }
                
    get content() {
                return this._content;
    }
    
    set title(value) {
                this._element.title = value;
    }
    
    set type(value) {
                this._element.type = value;
    }
    
    set content(value) {
                this._content = Element.insert(this._element, value);
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('button');
            
    this._element.classList.add('nrgsoft-widget-base-Button');
    
        super._constructor(properties);
        this.it.mapEvent(this._element, 'click', 'click');
    
    }
}
