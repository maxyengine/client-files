const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftWidgetBaseHotkeyButton = nrgsoft.widget.base.HotkeyButton,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.widget.dialog.Confirm = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
                
    get message() {
        return this._e2.nodeValue;
    }
                
    get _e4_() {
                
        if (!this.__e4_) {
            const
                properties = {"content":"cancel","clickHotkey":"Esc"},
                services = {
                    t: this.it.t
                };
            
            this.__e4_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.HotkeyButton, properties, services) :
                new nrgsoft.widget.base.HotkeyButton(properties, services);
        }

        return this.__e4_;
    
    }
                
    get isActiveHotkeys() {
                return this._e4_.isActiveHotkeys;
    }
                
    get _e5_() {
                
        if (!this.__e5_) {
            const
                properties = {"content":"ok","clickHotkey":"Enter"},
                services = {
                    t: this.it.t
                };
            
            this.__e5_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.HotkeyButton, properties, services) :
                new nrgsoft.widget.base.HotkeyButton(properties, services);
        }

        return this.__e5_;
    
    }
    
    set message(value) {
        
        this._e2.nodeValue = this.it.t ? this.it.t(value) : value;
    }
    
    set isActiveHotkeys(value) {
                this._e4_.isActiveHotkeys = value;
                this._e5_.isActiveHotkeys = value;
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = document.createElement('div');
            
        this._e1.classList.add('nrgsoft-widget-dialog-Confirm__message');
        this._element.appendChild(this._e1);
        this._e2 = document.createTextNode('');
        this._e1.appendChild(this._e2);
        this._e3 = document.createElement('div');
            
        this._e3.classList.add('nrgsoft-widget-dialog-Confirm__buttons');
        this._element.appendChild(this._e3);
        this._e4 = this._e4_.element;
        this._e3.appendChild(this._e4);
            
        for (const modifier of ["cancel"]) {
            this._e4.classList.add('nrgsoft-widget-base-HotkeyButton--' + modifier);
        }
        this._e5 = this._e5_.element;
        this._e3.appendChild(this._e5);
            
    this._element.classList.add('nrgsoft-widget-dialog-Confirm');
    
        super._constructor(properties);
        this.it.mapEvent(this._e4_, 'click', 'cancel');
        this.it.mapEvent(this._e5_, 'click', 'confirm');
    
    }
}
