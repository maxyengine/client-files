const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.widget.Overview = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._element.setAttribute('class', 'overview');
        this._e1 = document.createElement('div');
        this._e1.setAttribute('class', 'toolbar');
        this._element.appendChild(this._e1);
        this._e2 = document.createElement('button');
        this._e2.setAttribute('class', 'close');
        this._e1.appendChild(this._e2);
            
    this._element.classList.add('nrgsoft-widget-Overview');
    
        super._constructor(properties);
        this.it.mapEvent(this._e2, 'click', 'close');
    
    }
}
