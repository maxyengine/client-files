const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.widget.progress.Item = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
                
    get caption() {
        return this._e2.nodeValue;
    }
                
    get bar() {
                return this._e4;
    }
                
    get percent() {
                return this.bar.style.width;
    }
    
    set caption(value) {
        
        this._e2.nodeValue = this.it.t ? this.it.t(value) : value;
    }
    
    set percent(value) {
                this.bar.style.width = value;
        
        this._e5.nodeValue = this.it.t ? this.it.t(value) : value;
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = document.createElement('div');
            
        this._e1.classList.add('nrgsoft-widget-progress-Item__caption');
        this._element.appendChild(this._e1);
        this._e2 = document.createTextNode('');
        this._e1.appendChild(this._e2);
        this._e3 = document.createElement('div');
            
        this._e3.classList.add('nrgsoft-widget-progress-Item__progress');
        this._element.appendChild(this._e3);
        this._e4 = document.createElement('div');
            
        this._e4.classList.add('nrgsoft-widget-progress-Item__bar');
        this._e3.appendChild(this._e4);
        this._e5 = document.createTextNode('');
        this._e4.appendChild(this._e5);
            
    this._element.classList.add('nrgsoft-widget-progress-Item');
    
        super._constructor(properties);
    
    }
}
