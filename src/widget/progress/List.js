const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkDataList = nrgsoft.sdk.data.List,
    NrgsoftSdkGuiWidgetAbility = nrgsoft.sdk.gui.WidgetAbility,
    NrgsoftSdkDataObserverVisualList = nrgsoft.sdk.data.observer.VisualList,
    NrgsoftWidgetProgressItem = nrgsoft.widget.progress.Item;

nrgsoft.widget.progress.List = class extends nrgsoft.sdk.data.List {
        
    get itemClass() {
        return nrgsoft.widget.progress.Item;
    }
            
    static get services() {
        return {
            ...nrgsoft.sdk.data.List.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.sdk.gui.WidgetAbility,
			nrgsoft.sdk.data.observer.VisualList
        ];
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
            
    this._element.classList.add('nrgsoft-widget-progress-List');
    
        super._constructor(properties);
    
    }
}
