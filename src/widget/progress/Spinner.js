const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.widget.progress.Spinner = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = document.createElement('div');
        this._e1.setAttribute('class', 'rect1');
        this._element.appendChild(this._e1);
        this._e2 = document.createElement('div');
        this._e2.setAttribute('class', 'rect2');
        this._element.appendChild(this._e2);
        this._e3 = document.createElement('div');
        this._e3.setAttribute('class', 'rect3');
        this._element.appendChild(this._e3);
        this._e4 = document.createElement('div');
        this._e4.setAttribute('class', 'rect4');
        this._element.appendChild(this._e4);
        this._e5 = document.createElement('div');
        this._e5.setAttribute('class', 'rect5');
        this._element.appendChild(this._e5);
            
    this._element.classList.add('nrgsoft-widget-progress-Spinner');
    
        super._constructor(properties);
    
    }
}
