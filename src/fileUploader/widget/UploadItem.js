const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftWidgetBaseLink = nrgsoft.widget.base.Link,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.fileUploader.widget.UploadItem = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
                
    get uploaded() {
                
    return this._element.classList.contains('uploaded');

    }
                
    get name() {
        return this._e3.nodeValue;
    }
                
    get loaded() {
        return this._e6.nodeValue;
    }
                
    get total() {
        return this._e9.nodeValue;
    }
                
    get showInFolder() {
                
        if (!this._showInFolder) {
            const
                properties = {"nrg:access":"\/file-manager\/directory\/read","content":"show in folder","url":"\/"},
                services = {
                    t: this.it.t
                };
            
            this._showInFolder = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.Link, properties, services) :
                new nrgsoft.widget.base.Link(properties, services);
        }

        return this._showInFolder;
    
    }
                
    get bar() {
                return this._e13;
    }
                
    get percent() {
                return this.bar.style.width;
    }
    
    set uploaded(value) {
            if (value) {
            this._element.classList.add('uploaded');
        } else {
            this._element.classList.remove('uploaded');
        }
        
    }
    
    set name(value) {
        
        this._e3.nodeValue = this.it.t ? this.it.t(value) : value;
    }
    
    set loaded(value) {
        
        this._e6.nodeValue = this.it.t ? this.it.t(value) : value;
    }
    
    set total(value) {
        
        this._e9.nodeValue = this.it.t ? this.it.t(value) : value;
    }
    
    set percent(value) {
                this.bar.style.width = value;
        
        this._e14.nodeValue = this.it.t ? this.it.t(value) : value;
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._element.setAttribute('class', 'item');
        this._e1 = document.createElement('a');
        this._e1.setAttribute('class', 'close');
        this._element.appendChild(this._e1);
        this._e2 = document.createElement('div');
        this._e2.setAttribute('class', 'name');
        this._element.appendChild(this._e2);
        this._e3 = document.createTextNode('');
        this._e2.appendChild(this._e3);
        this._e4 = document.createElement('div');
        this._e4.setAttribute('class', 'part');
        this._element.appendChild(this._e4);
        this._e5 = document.createElement('span');
        this._e4.appendChild(this._e5);
        this._e6 = document.createTextNode('');
        this._e5.appendChild(this._e6);
        this._e7 = document.createTextNode(' / ');
        this._e4.appendChild(this._e7);
        this._e8 = document.createElement('span');
        this._e4.appendChild(this._e8);
        this._e9 = document.createTextNode('');
        this._e8.appendChild(this._e9);
        this._e10 = document.createElement('div');
        this._e10.setAttribute('class', 'show-in-folder');
        this._element.appendChild(this._e10);
        this._e11 = this.showInFolder.element;
        this._e10.appendChild(this._e11);
        this._e12 = document.createElement('div');
        this._e12.setAttribute('class', 'progress');
        this._element.appendChild(this._e12);
        this._e13 = document.createElement('div');
        this._e13.setAttribute('class', 'bar');
        this._e12.appendChild(this._e13);
        this._e14 = document.createTextNode('');
        this._e13.appendChild(this._e14);
            
    this._element.classList.add('nrgsoft-fileUploader-widget-UploadItem');
    
        super._constructor(properties);
        this.uploaded = !!this.uploaded; 
        this.it.mapEvent(this._e1, 'click', 'close');
    
    }
}
