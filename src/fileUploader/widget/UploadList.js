const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkDataList = nrgsoft.sdk.data.List,
    NrgsoftSdkGuiWidgetAbility = nrgsoft.sdk.gui.WidgetAbility,
    NrgsoftSdkDataObserverVisualList = nrgsoft.sdk.data.observer.VisualList,
    NrgsoftFileUploaderWidgetUploadItem = nrgsoft.fileUploader.widget.UploadItem;

nrgsoft.fileUploader.widget.UploadList = class extends nrgsoft.sdk.data.List {
        
    get itemClass() {
        return nrgsoft.fileUploader.widget.UploadItem;
    }
            
    static get services() {
        return {
            ...nrgsoft.sdk.data.List.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.sdk.gui.WidgetAbility,
			nrgsoft.sdk.data.observer.VisualList
        ];
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._element.setAttribute('class', 'list');
            
    this._element.classList.add('nrgsoft-fileUploader-widget-UploadList');
    
        super._constructor(properties);
    
    }
}
