const
    Action = nrgsoft.fileUploader.action.Action,
    View = nrgsoft.fileUploader.view.Main,
    Size = nrgsoft.fileManager.value.Size;

nrgsoft.fileUploader.action.Main = class extends Action {

    get defaults() {
        return {
            defaultParams: {
                path: '/'
            },
            backUrl: '/'
        };
    }

    initialize() {
        this.view = this.createView(View);
    }

    get uploadList() {
        return this.view.uploadList;
    }

    execute(event) {
        this.params = {...this.defaultParams, ...event.url.params};
        this.layout.content = this.view;
    }

    onSelectFiles(event) {
        const files = [...(event.target.files || event.dataTransfer.files)];
        event.target.value = null;

        for (const file of files) {
            this.uploadFile(file);
        }
    }

    uploadFile(file) {
        const uploadItem = this.uploadList.addItem({
                name: file.name,
                total: new Size(file.size),
                onClose: () => {
                    this.uploadList.removeItem(uploadItem);
                },
                onSetPercent(event) {
                    console.log(event.value);
                }
            });

        this.fileModel.uploadFile({file: file, path: this.params.path})
            .onProgress(progress => {
                uploadItem.percent = Math.ceil(100 * progress.loaded / progress.total) + '%';
                uploadItem.loaded = new Size(progress.loaded);
            })
            .onLoad(file => {
                uploadItem.showInFolder.url = this.createUrl(this.backUrl, {
                    path: this.params.path,
                    fileName: file.path.fileName.value
                });
                uploadItem.uploaded = true;
            })
            .start();
    }

    onClose() {
        this.goTo(this.backUrl, {
            path: this.params.path,
        });
    }
}
