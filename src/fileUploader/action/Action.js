const
    WebAction = nrgsoft.sdk.web.Action,
    FileModel = nrgsoft.fileManager.model.File;

nrgsoft.fileUploader.action.Action = class extends WebAction {

    static get services() {
        return {
            ...WebAction.services,
            fileModel: FileModel
        };
    }
}
