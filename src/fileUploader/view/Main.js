const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftWidgetOverview = nrgsoft.widget.Overview,
    NrgsoftFileUploaderWidgetUploadList = nrgsoft.fileUploader.widget.UploadList,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.fileUploader.view.Main = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
                
    get overview() {
                
        if (!this._overview) {
            const
                properties = {},
                services = {
                    t: this.it.t
                };
            
            this._overview = this.injector ?
                this.injector.createObject(nrgsoft.widget.Overview, properties, services) :
                new nrgsoft.widget.Overview(properties, services);
        }

        return this._overview;
    
    }
                
    get uploadList() {
                
        if (!this._uploadList) {
            const
                properties = {},
                services = {
                    t: this.it.t
                };
            
            this._uploadList = this.injector ?
                this.injector.createObject(nrgsoft.fileUploader.widget.UploadList, properties, services) :
                new nrgsoft.fileUploader.widget.UploadList(properties, services);
        }

        return this._uploadList;
    
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._element.setAttribute('class', 'nrgsoft-file-uploader__index-main');
        this._e1 = this.overview.element;
        this._element.appendChild(this._e1);
        this._e2 = document.createElement('input');
        this._e2.setAttribute('type', 'file');
        this._e2.setAttribute('multiple', 'multiple');
        this._element.appendChild(this._e2);
        this._e3 = this.uploadList.element;
        this._element.appendChild(this._e3);
            
    this._element.classList.add('nrgsoft-fileUploader-view-Main');
    
        super._constructor(properties);
        this.it.mapEvent(this.overview, 'close', 'close');
        this.it.mapEvent(this._e2, 'change', 'selectFiles');
    
    }
}
