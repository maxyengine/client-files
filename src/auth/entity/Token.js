const
    Value = nrgsoft.sdk.lang.Value;

nrgsoft.auth.entity.Token = class extends Value {

    get isExpired() {
        return (new Date()).getTime() >= this.expires.getTime();
    }

    toString() {
        return this.id;
    }
}
