nrgsoft.auth.entity.Roles = class {

    constructor(roles) {
        this.roles = roles;
    }

    isAllowed(url) {
        for (const role of this.roles) {
            if (role.isAllowed(url)) {
                return true;
            }
        }

        return false;
    }
}
