const
    Value = nrgsoft.sdk.lang.Value;

nrgsoft.auth.entity.Role = class extends Value {

    isAllowed(url) {
        return this.permissions.isAllowed(url);
    }
}
