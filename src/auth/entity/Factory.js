const
    Value = nrgsoft.sdk.lang.Value,
    Url = nrgsoft.sdk.web.Url,
    Roles = nrgsoft.auth.entity.Roles,
    Permissions = nrgsoft.auth.value.Permissions,
    User = nrgsoft.auth.entity.User,
    Token = nrgsoft.auth.entity.Token,
    Role = nrgsoft.auth.entity.Role;

nrgsoft.auth.entity.Factory = class extends Value {

    static get services() {
        return {
            client: 'client'
        };
    }

    createUser(raw) {
        return new User({
            ...raw,
            createdAt: new Date(raw.createdAt),
            updatedAt: raw.updatedAt && new Date(raw.updatedAt),
            roles: this.createRoles(raw.roles.data),
            permissions: raw.permissions ? this.createPermissions(raw.permissions) : null,
            avatar: raw.avatar ? new Url(raw.avatar) : null
        });
    }

    createUsers(collection) {
        const
            //collection = {...collection}, todo: check!
            data = [];
        collection.data.forEach(raw => {
            data.push(this.createUser(raw));
        });
        collection.data = data;

        return collection;
    }

    createToken(raw) {
        const expires = new Date();
        expires.setSeconds(expires.getSeconds() + raw.expires);

        return new Token({
            ...raw,
            createdAt: new Date(raw.createdAt),
            expires: expires,
            user: this.createUser(raw.user)
        });
    }

    createRole(raw) {
        return new Role({
            ...raw,
            createdAt: new Date(raw.createdAt),
            updatedAt: raw.updatedAt && new Date(raw.updatedAt),
            permissions: this.createPermissions(raw.permissions)
        });
    }

    createRoles(raws) {
        const roles = [];
        for (const raw of raws) {
            roles.push(this.createRole(raw));
        }

        return new Roles(roles);
    }

    createPermissions(hrefs) {
        const urls = [];
        for (const href of hrefs) {
            urls.push(this.client.createUrl(href));
        }

        return new Permissions(urls);
    }
}
