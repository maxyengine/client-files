const
    Value = nrgsoft.sdk.lang.Value;

nrgsoft.auth.entity.User = class extends Value {

    get isSuperUser() {
        return !this.permissions;
    }

    isAllowed(url) {
        return this.isSuperUser ||
            this.permissions.isAllowed(url) ||
            this.roles.isAllowed(url);
    }
}
