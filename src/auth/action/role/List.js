const
    BaseAction = nrgsoft.auth.action.Action,
    View = nrgsoft.auth.view.role.List;

nrgsoft.auth.action.role.List = class extends BaseAction {

    get form() {
        return this.view.form;
    }

    initialize() {
        this.view = this.injector.createObject(View, {}, {t: this.t});
        this.form.on(this);
    }

    execute(queryParams = {}) {
        this.form.reset();

        this.permissionModel.list({}, {
            onSuccess: event => {
                this.form.permissions.data = event.collection.asNestedList;
            }
        });

        this.roleModel.list(queryParams, {
            onSuccess: event => {
                this.layout.content = this.view;
                this.view.collection = event.collection;
            }
        });
    }

    onSubmit(event) {
        event.preventDefault();

        if (!this.form.hasErrors) {
            this.roleModel.create(this.form.serialize(), this);
        }
    }

    onSuccess(event) {
        this.router.refresh({
            orderBy: [
                {
                    field: 'createdAt',
                    direction: 'desc'
                }
            ]
        });
    }

    onStatusCode422(event) {
        this.form.errorMessages = event.errorMessages;
    }
}
