const
    BaseAction = nrgsoft.auth.action.Action,
    View = nrgsoft.auth.view.role.Details;

const self = nrgsoft.auth.action.role.Details = class extends BaseAction {

    initialize() {
        this.view = this.injector.createObject(View, {t: this.t});
    }

    execute(queryParams = {}) {
        this.roleModel.details(queryParams, {
            onSuccess: event => {
                this.view.set(
                    self.createEntityView(event.entity)
                );

                this.layout.content = this.view;
            }
        });
    }

    static createEntityView(entity) {
        return {
            ...entity,
            ...{
                createdAt: entity.createdAt.toLocaleString(),
                updatedAt: entity.updatedAt && entity.updatedAt.toLocaleString(),
                hasUpdated: !!entity.updatedAt,
                permissionNestedList: entity.permissions.asNestedList
            }
        };
    }
}
