const
    BaseAction = nrgsoft.auth.action.Action,
    View = nrgsoft.auth.view.role.Update;

nrgsoft.auth.action.role.Update = class extends BaseAction {

    initialize() {
        this.form = new View({t: this.t});
        this.form.on(this);
        this.subLayout.content = this.form;
    }

    execute(params) {
        this.params = params;
        this.subLayout.params = params;
        this.form.reset();
        this.roleModel.viewDetails(params, {
            onSuccess: event => {
                this.layout.header = this.t('edit role');
                this.layout.content = this.subLayout;
                this.form.populate(event.entity);
            }
        });
    }

    onSubmit(event) {
        event.preventDefault();
        if (!this.form.hasErrors) {
            this.roleModel.updateDetails({...this.params, ...this.form.serialize()}, this);
        }
    }

    onSuccess(event) {
        this.router.goToRelative('/view', {id: event.entity.id});
    }

    onStatusCode422(event) {
        this.form.errorMessages = event.errorMessages;
    }
}
