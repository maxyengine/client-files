const
    BaseAction = nrgsoft.auth.action.Action,
    View = nrgsoft.auth.form.role.Create;

nrgsoft.auth.action.role.Create = class extends BaseAction {

    initialize() {
        this.form = new View({t: this.t});
        this.form.submit.caption = 'save';
        this.form.on(this);
    }

    execute() {
        this.form.reset();
        this.permissionModel.list({}, {
            onSuccess: event => {
                this.form.permissions.data = event.collection;
                this.layout.content = this.form;
            }
        });
    }

    onSubmit(event) {
        event.preventDefault();
        if (!this.form.hasErrors) {
            this.roleModel.create(this.form.serialize(), this);
        }
    }

    onSuccess(event) {
        this.router.goToRelative('/details', {id: event.entity.id});
    }

    onStatusCode422(event) {
        this.form.errorMessages = event.errorMessages;
    }
}
