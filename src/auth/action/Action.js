const
    WebAction = nrgsoft.sdk.web.Action,
    RoleModel = nrgsoft.auth.model.Role,
    UserModel = nrgsoft.auth.model.User,
    PermissionModel = nrgsoft.auth.model.Permission,
    TranslatorAbility = nrgsoft.sdk.i18n.TranslatorAbility;

nrgsoft.auth.action.Action = class extends WebAction {

    static get services() {
        return {
            ...WebAction.services,
            accessControl: 'accessControl',
            roleModel: RoleModel,
            userModel: UserModel,
            permissionModel: PermissionModel
        };
    }

    authorize(token) {
        this.accessControl.authorize(token);
    }

    unauthorized(backUrl) {
        return this.controller.unauthorized(backUrl);
    }
}
