const
    BaseAction = nrgsoft.auth.action.Action,
    View = nrgsoft.auth.view.user.List;

nrgsoft.auth.action.user.List = class extends BaseAction {

    get form() {
        return this.view.form;
    }

    initialize() {
        this.view = this.injector.createObject(View, {}, {t: this.t});
        this.form.on(this);
    }

    execute(queryParams = {}) {
        this.form.reset();

        this.roleModel.list({}, {
            onSuccess: event => {
                this.form.roles.collection = event.collection;
            }
        });

        this.userModel.list(queryParams, {
            onSuccess: event => {
                this.layout.content = this.view;
                this.view.collection = event.collection;
            }
        });
    }

    onSubmit(event) {
        event.preventDefault();

        if (!this.form.hasErrors) {
            this.userModel.create(this.form.serialize(), this);
        }
    }

    onSuccess(event) {
        this.router.refresh({
            orderBy: [
                {
                    field: 'createdAt',
                    direction: 'desc'
                }
            ]
        });
    }

    onStatusCode422(event) {
        this.form.errorMessages = event.errorMessages;
    }
}
