const
    Action = nrgsoft.auth.action.Action,
    View = nrgsoft.auth.view.user.DeniedAccess;

nrgsoft.auth.action.user.ForbiddenAction = class extends Action {

    initialize() {
        this.view = this.createView(View);
    }

    execute() {
        this.layout.content = this.view;
    }
}
