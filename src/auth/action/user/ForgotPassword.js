const
    BaseAction = nrgsoft.auth.action.Action,
    View = nrgsoft.auth.view.user.Login,
    Link = nrgsoft.widget.base.Link;

nrgsoft.auth.action.user.ForgotPassword = class extends BaseAction {

    get form() {
        return this.view.form;
    }

    initialize() {
        this.view = this.injector.createObject(View, {t: this.t});
        this.form.submit.caption = 'login';

        this.form.email.value = 'umax@yahoo.com';
        this.form.password.value = 'test7777';

        this.form.on(this);
    }

    execute(queryParams = {}) {
        this.layout.content = this.injector.createObject(Link, {
            url: '/auth/user/list',
            content: 'Test Link'
        });
        return;

        this.queryParams = queryParams;
        this.layout.content = this.view;
    }

    onSubmit(event) {
        event.preventDefault();
        if (!this.form.hasErrors) {
            this.userModel.login(this.form.serialize(), this);
        }
    }

    onSuccess(event) {
        this.accessControl.token = event.entity;

        this.router.goTo('/');
    }

    onStatusCode422(event) {
        this.form.errorMessages = event.errorMessages;
    }
}
