const
  Action = nrgsoft.auth.action.Action,
  View = nrgsoft.auth.view.user.Login

nrgsoft.auth.action.user.Login = class extends Action {

  get defaults () {
    return {
      ...super.defaults || {},
      backUrl: '/'
    }
  }

  get form () {
    return this.view.form
  }

  initialize () {
    this.view = this.createView(View)
  }

  execute (event) {
    this.backUrl = event.url.getParam('backUrl', this.backUrl)
    this.layout.content = this.view
    this.form.focus()
  }

  onSubmit (event) {
    event.preventDefault()

    if (!this.form.hasErrors) {
      this.userModel.login(this.form.serialize())
        .then(token => {
          this.authorize(token)
          this.goTo(decodeURIComponent(this.backUrl))
        })
        .catch(error => {
          if (error.messages) {
            this.form.errorMessages = error.messages
          } else {
            console.log(error)
          }
        })
    }
  }
}
