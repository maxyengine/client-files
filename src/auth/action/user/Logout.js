const
    BaseAction = nrgsoft.auth.action.Action;

nrgsoft.auth.action.user.Logout = class extends BaseAction {

    execute() {
        this.userModel.logout()
            .then(() => {
                this.unauthorized();
            });
    }
}
