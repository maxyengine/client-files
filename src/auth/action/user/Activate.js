const
    BaseAction = nrgsoft.auth.action.Action,
    View = nrgsoft.auth.view.user.Activate;

nrgsoft.auth.action.user.Activate = class extends BaseAction {

    get form() {
        return this.view.form;
    }

    initialize() {
        this.view = this.injector.createObject(View, {t: this.t});
        this.form.submit.caption = 'activate';
        this.form.on(this);
    }

    execute(queryParams = {}) {
        this.queryParams = queryParams;
        this.layout.content = this.view;
    }

    onSubmit(event) {
        event.preventDefault();
        if (!this.form.hasErrors) {
            this.userModel.activate({
                ...this.form.serialize(),
                id: this.queryParams.id,
                token: this.queryParams.token
            }, this);
        }
    }

    onSuccess(event) {
        this.router.goToRelative('/login');
    }

    onStatusCode422(event) {
        this.form.errorMessages = event.errorMessages;
    }
}
