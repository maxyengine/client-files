const
    BaseAction = nrgsoft.auth.action.Action,
    View = nrgsoft.auth.view.user.Profile;

const self = nrgsoft.auth.action.user.Profile = class extends BaseAction {

    initialize() {
        this.view = this.injector.createObject(View, {t: this.t});
        this.view.on(this);
    }

    execute() {
        this.view.set(
            self.createEntityView(this.accessControl.user)
        );

        this.layout.content = this.view;
    }

    static createEntityView(entity) {
        return {
            ...entity,
            ...{
                hasAvatar: !!entity.avatar,
                createdAt: entity.createdAt.toLocaleString(),
                updatedAt: entity.updatedAt && entity.updatedAt.toLocaleString(),
                hasUpdated: !!entity.updatedAt,
                permissionNestedList: entity.permissions && entity.permissions.asNestedList,
            }
        };
    }

    onSelectAvatarFile() {
        if(!this.view.avatarFile.hasError) {
            const formData = new FormData();
            formData.append('file', this.view.avatarFile.value);

            this.userModel.uploadProfileAvatar(formData, {
                onSuccess: event => {
                    console.log('upload Ok');
                },
                onStatusCode422: event => {
                    console.log(event.errorMessages);
                }
            });
        }
    }
}
