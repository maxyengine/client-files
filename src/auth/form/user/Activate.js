const
    Element = nrgsoft.sdk.gui.utility.Element,
    ClassName = nrgsoft.sdk.gui.utility.ClassName,
    EngineFormForm = nrgsoft.sdk.form.Form,
    EngineGuiWidgetAbility = nrgsoft.sdk.gui.WidgetAbility,
    EngineGuiObserverAppendWidgetToList = nrgsoft.sdk.gui.observer.AppendWidgetToList,
    EngineAuthFormUserElementPassword = nrgsoft.auth.form.user.element.Password,
    EngineAuthFormUserElementPasswordConfirmation = nrgsoft.auth.form.user.element.PasswordConfirmation,
    EngineWidgetFormSubmitButton = nrgsoft.widget.form.SubmitButton;

nrgsoft.auth.form.user.Activate = class extends nrgsoft.sdk.form.Form {
        
    get password() {
        if (!this._password) {
            const properties = {t: this.it.t};
            this._password = this.injector ?
                this.injector.createObject(nrgsoft.auth.form.user.element.Password, properties) :
                new nrgsoft.auth.form.user.element.Password(properties);
                
            Element.append(this._element, this._password);
        }

        return this._password;
    }
            
    get passwordconfirmation() {
        if (!this._passwordconfirmation) {
            const properties = {t: this.it.t};
            this._passwordconfirmation = this.injector ?
                this.injector.createObject(nrgsoft.auth.form.user.element.PasswordConfirmation, properties) :
                new nrgsoft.auth.form.user.element.PasswordConfirmation(properties);
                
            Element.append(this._element, this._passwordconfirmation);
        }

        return this._passwordconfirmation;
    }
            
    get submit() {
        if (!this._submit) {
            const properties = {t: this.it.t};
            this._submit = this.injector ?
                this.injector.createObject(nrgsoft.widget.form.SubmitButton, properties) :
                new nrgsoft.widget.form.SubmitButton(properties);
                
            Element.append(this._element, this._submit);
        }

        return this._submit;
    }
            
    static get services() {
        return {
            ...(nrgsoft.sdk.form.Form.services || {}),
            
        };
    }
            
    get traits() {
        return [
            ...(super.traits || []),
            nrgsoft.sdk.gui.WidgetAbility,
			nrgsoft.sdk.gui.observer.AppendWidgetToList
        ];
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('form');
    
        super._constructor(properties);
        this.it.addEvent(this._element, 'submit', 'submit');
        this.it.trigger('appendWidget', {widget: this.password});
        this.it.trigger('appendWidget', {widget: this.passwordconfirmation});
        this.it.trigger('appendWidget', {widget: this.submit});
    
    }
}
