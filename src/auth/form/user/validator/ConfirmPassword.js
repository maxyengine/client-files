const
    TypeObject = nrgsoft.sdk.lang.Object,
    Type = nrgsoft.sdk.lang.utility.Type,
    Message = nrgsoft.sdk.i18n.Message;

nrgsoft.auth.form.user.validator.ConfirmPassword = class extends TypeObject {

    get defaults() {
        return {
            invalidConfirmation: 'confirmation does not match the original'
        }
    }

    isValid(value) {
        if (Type.isEmpty(value)) {
            return true;
        }

        const password = this.element.form.password.value;

        if (password !== value) {
            this.errorMessage = new Message(this.invalidConfirmation);

            return false;
        }

        return true;
    }
}
