const
    TypeObject = nrgsoft.sdk.lang.Object,
    Type = nrgsoft.sdk.lang.utility.Type,
    Message = nrgsoft.sdk.i18n.Message;

nrgsoft.auth.form.user.validator.AvatarImage = class extends TypeObject {

    get defaults() {
        return {
            minSize: 0,
            maxSize: 5 * 1024,
            allowTypes: ['image/png', 'image/gif', 'image/jpeg'],
            errorLessMinSize: 'file size must be greater than %d byte',
            errorGreaterMaxSize: 'file size must be less than %d byte'
        }
    }

    isValid(value) {
        if (Type.isEmpty(value)) {
            return true;
        }

        return this.checkFile(value);
    }

    checkFile(file) {
        if (file.size < this.minSize) {
            this.errorMessage = new Message(this.errorLessMinSize, this.minSize);

            return false;
        }

        if (file.size > this.maxSize) {
            this.errorMessage = new Message(this.errorGreaterMaxSize, this.maxSize);

            return false;
        }

        return true;
    }
}
