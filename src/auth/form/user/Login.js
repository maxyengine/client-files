const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkFormForm = nrgsoft.sdk.form.Form,
    NrgsoftSdkGuiWidgetAbility = nrgsoft.sdk.gui.WidgetAbility,
    NrgsoftSdkFormObserverOnAppendElement = nrgsoft.sdk.form.observer.OnAppendElement,
    NrgsoftAuthFormUserElementEmail = nrgsoft.auth.form.user.element.Email,
    NrgsoftAuthFormUserElementPassword = nrgsoft.auth.form.user.element.Password;

nrgsoft.auth.form.user.Login = class extends nrgsoft.sdk.form.Form {
        
    static get services() {
        return {
            ...nrgsoft.sdk.form.Form.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.sdk.gui.WidgetAbility,
			nrgsoft.sdk.form.observer.OnAppendElement
        ];
    }
                
    get _e1_() {
                
        if (!this.__e1_) {
            const
                properties = {"label":"email","value":"super@user.com"},
                services = {
                    t: this.it.t
                };
            
            this.__e1_ = this.injector ?
                this.injector.createObject(nrgsoft.auth.form.user.element.Email, properties, services) :
                new nrgsoft.auth.form.user.element.Email(properties, services);
        }

        return this.__e1_;
    
    }
                
    get _e2_() {
                
        if (!this.__e2_) {
            const
                properties = {"label":"password","value":"test7777"},
                services = {
                    t: this.it.t
                };
            
            this.__e2_ = this.injector ?
                this.injector.createObject(nrgsoft.auth.form.user.element.Password, properties, services) :
                new nrgsoft.auth.form.user.element.Password(properties, services);
        }

        return this.__e2_;
    
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = this._e1_.element;
        this._element.appendChild(this._e1);
        this._e2 = this._e2_.element;
        this._element.appendChild(this._e2);
            
    this._element.classList.add('nrgsoft-auth-form-user-Login');
    
        super._constructor(properties);
        this.it.trigger('appendElement', {widget: this._e1_});
        this.it.trigger('appendElement', {widget: this._e2_});
    
    }
}
