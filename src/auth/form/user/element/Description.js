const
    TextareaElement = nrgsoft.widget.form.TextareaElement,
    Trim = nrgsoft.sdk.form.filter.Trim,
    TypeString = nrgsoft.sdk.form.validator.TypeString,
    Length = nrgsoft.sdk.form.validator.Length;

nrgsoft.auth.form.user.element.Description = class extends TextareaElement {

    initialize() {
        this
            .set({
                name: 'description',
                label: 'description',
                value: ''
            })
            .addFilter(new Trim())
            .addValidator(new TypeString())
            .addValidator(new Length({
                min: 3,
                max: 255,
            }))
        ;
    }
}
