const
    FileElement = nrgsoft.widget.form.FileElement,
    AvatarImage = nrgsoft.auth.form.user.validator.AvatarImage;

nrgsoft.auth.form.user.element.Avatar = class extends FileElement {

    initialize() {
        this
            .set({
                name: 'avatar',
                multiple: false,
                placeholder: 'change avatar',
                value: ''
            })
            .addValidator(new AvatarImage())
        ;
    }
}
