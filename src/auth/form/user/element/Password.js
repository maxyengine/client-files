const
    PasswordElement = nrgsoft.widget.form.PasswordElement,
    Trim = nrgsoft.sdk.form.filter.Trim,
    Required = nrgsoft.sdk.form.validator.Required,
    TypeString = nrgsoft.sdk.form.validator.TypeString,
    Length = nrgsoft.sdk.form.validator.Length;

nrgsoft.auth.form.user.element.Password = class extends PasswordElement {

    initialize() {
        this
            .set({
                name: 'password',
                isRequired: true
            })
            .addFilter(new Trim())
            .addValidator(new Required())
            .addValidator(new TypeString())
            .addValidator(new Length({
                min: 8,
                max: 70,
            }))
        ;
    }
}
