const
    TextElement = nrgsoft.widget.form.TextElement,
    Trim = nrgsoft.sdk.form.filter.Trim,
    Required = nrgsoft.sdk.form.validator.Required,
    TypeString = nrgsoft.sdk.form.validator.TypeString,
    TypeEmail = nrgsoft.sdk.form.validator.Email,
    Length = nrgsoft.sdk.form.validator.Length;

nrgsoft.auth.form.user.element.Email = class extends TextElement {

    initialize() {
        this
            .set({
                name: 'email',
                isRequired: true
            })
            .addFilter(new Trim())
            .addValidator(new Required())
            .addValidator(new TypeString())
            .addValidator(new TypeEmail())
            .addValidator(new Length({
                min: 3,
                max: 255,
            }))
        ;
    }
}
