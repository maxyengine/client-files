const
    RolesElement = nrgsoft.auth.widget.RolesElement,
    Required = nrgsoft.sdk.form.validator.Required;

nrgsoft.auth.form.user.element.Roles = class extends RolesElement {

    initialize() {
        this
            .set({
                label: this.t('roles'),
                name: 'roles',
                isRequired: true
            })
            .addValidator(new Required())
        ;
    }
}
