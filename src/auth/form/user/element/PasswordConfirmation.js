const
    PasswordElement = nrgsoft.widget.form.PasswordElement,
    Trim = nrgsoft.sdk.form.filter.Trim,
    Required = nrgsoft.sdk.form.validator.Required,
    TypeString = nrgsoft.sdk.form.validator.TypeString,
    Length = nrgsoft.sdk.form.validator.Length,
    ConfirmPassword = nrgsoft.auth.form.user.validator.ConfirmPassword;

nrgsoft.auth.form.user.element.PasswordConfirmation = class extends PasswordElement {

    initialize() {
        this
            .set({
                name: 'passwordConfirmation',
                label: this.t('password confirmation'),
                value: '',
                isRequired: true
            })
            .addFilter(new Trim())
            .addValidator(new Required())
            .addValidator(new TypeString())
            .addValidator(new Length({
                min: 8,
                max: 70,
            }))
            .addValidator(new ConfirmPassword())
        ;
    }
}
