const
    Element = nrgsoft.sdk.gui.utility.Element,
    ClassName = nrgsoft.sdk.gui.utility.ClassName,
    EngineFormForm = nrgsoft.sdk.form.Form,
    EngineGuiWidgetAbility = nrgsoft.sdk.gui.WidgetAbility,
    EngineGuiObserverAppendWidgetToList = nrgsoft.sdk.gui.observer.AppendWidgetToList,
    EngineAuthFormRoleElementName = nrgsoft.auth.form.role.element.Name,
    EngineAuthFormRoleElementDescription = nrgsoft.auth.form.role.element.Description,
    EngineAuthFormRoleElementPermissions = nrgsoft.auth.form.role.element.Permissions,
    EngineWidgetFormSubmitButton = nrgsoft.widget.form.SubmitButton;

nrgsoft.auth.form.role.Create = class extends nrgsoft.sdk.form.Form {
        
    get name() {
        if (!this._name) {
            const
                properties = {},
                services = {
                    t: this.it.t
                };
            
            this._name = this.injector ?
                this.injector.createObject(nrgsoft.auth.form.role.element.Name, properties, services) :
                new nrgsoft.auth.form.role.element.Name(properties, services);
        }

        return this._name;
    }
            
    get description() {
        if (!this._description) {
            const
                properties = {},
                services = {
                    t: this.it.t
                };
            
            this._description = this.injector ?
                this.injector.createObject(nrgsoft.auth.form.role.element.Description, properties, services) :
                new nrgsoft.auth.form.role.element.Description(properties, services);
        }

        return this._description;
    }
            
    get permissions() {
        if (!this._permissions) {
            const
                properties = {},
                services = {
                    t: this.it.t
                };
            
            this._permissions = this.injector ?
                this.injector.createObject(nrgsoft.auth.form.role.element.Permissions, properties, services) :
                new nrgsoft.auth.form.role.element.Permissions(properties, services);
        }

        return this._permissions;
    }
            
    get submit() {
        if (!this._submit) {
            const
                properties = {"caption":"save"},
                services = {
                    t: this.it.t
                };
            
            this._submit = this.injector ?
                this.injector.createObject(nrgsoft.widget.form.SubmitButton, properties, services) :
                new nrgsoft.widget.form.SubmitButton(properties, services);
        }

        return this._submit;
    }
            
    static get services() {
        return {
            ...(nrgsoft.sdk.form.Form.services || {}),
            
        };
    }
            
    get traits() {
        return [
            ...(super.traits || []),
            nrgsoft.sdk.gui.WidgetAbility,
			nrgsoft.sdk.gui.observer.AppendWidgetToList
        ];
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('form');
        this._e1 = this.name.element;
        this._element.appendChild(this._e1);
        this._e2 = this.description.element;
        this._element.appendChild(this._e2);
        this._e3 = this.permissions.element;
        this._element.appendChild(this._e3);
        this._e4 = this.submit.element;
        this._element.appendChild(this._e4);
    
        super._constructor(properties);
        this.it.addEvent(this._element, 'submit', 'submit');
        this.it.trigger('appendWidget', {widget: this.name});
        this.it.trigger('appendWidget', {widget: this.description});
        this.it.trigger('appendWidget', {widget: this.permissions});
    
    }
}
