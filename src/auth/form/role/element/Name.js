const
    TextElement = nrgsoft.widget.form.TextElement,
    Trim = nrgsoft.sdk.form.filter.Trim,
    Required = nrgsoft.sdk.form.validator.Required,
    TypeString = nrgsoft.sdk.form.validator.TypeString,
    Length = nrgsoft.sdk.form.validator.Length;

nrgsoft.auth.form.role.element.Name = class extends TextElement {

    initialize() {
        this
            .set({
                name: 'name',
                label: this.t('name'),
                value: '',
                isRequired: true
            })
            .addFilter(new Trim())
            .addValidator(new Required())
            .addValidator(new TypeString())
            .addValidator(new Length({
                min: 1,
                max: 50,
            }))
        ;
    }
}
