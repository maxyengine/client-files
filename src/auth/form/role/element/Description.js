const
    TextareaElement = nrgsoft.widget.form.TextareaElement,
    Trim = nrgsoft.sdk.form.filter.Trim,
    Required = nrgsoft.sdk.form.validator.Required,
    TypeString = nrgsoft.sdk.form.validator.TypeString,
    Length = nrgsoft.sdk.form.validator.Length;

nrgsoft.auth.form.role.element.Description = class extends TextareaElement {

    initialize() {
        this
            .set({
                name: 'description',
                label: this.t('description'),
                value: '',
                isRequired: true
            })
            .addFilter(new Trim())
            .addValidator(new Required())
            .addValidator(new TypeString())
            .addValidator(new Length({
                min: 3,
                max: 255,
            }))
        ;
    }
}
