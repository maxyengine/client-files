const
    PermissionsElement = nrgsoft.auth.widget.PermissionsElement,
    Required = nrgsoft.sdk.form.validator.Required;

nrgsoft.auth.form.role.element.Permissions = class extends PermissionsElement {

    initialize() {
        this
            .set({
                label: this.t('permissions'),
                name: 'permissions',
                isRequired: true
            })
            .addValidator(new Required())
        ;
    }
}
