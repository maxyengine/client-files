const
    Model = nrgsoft.auth.model.Model;

nrgsoft.auth.model.User = class extends Model {

    list(body = {}) {
        return this.fetchUsers('/list', body);
    }

    create(body) {
        return this.fetchUser('/create', body);
    }

    details(body) {
        return this.fetchUser('/details', body);
    }

    update(body) {
        return this.fetchUser('/update', body);
    }

    activate(body) {
        return this.fetchUser('/activate', body);
    }

    login(body) {
        return this.fetchToken('/login', body);
    }

    logout(body) {
        return this.fetchToken('/logout', body);
    }

    //todo: remake
    uploadAvatar(body) {
        return this.fetch('/auth/user/upload/avatar', {body: body})
            .then(data => {
                return data;
            });
    }
}
