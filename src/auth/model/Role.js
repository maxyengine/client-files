const
    WebModel = nrgsoft.sdk.web.Model,
    Role = nrgsoft.auth.entity.Role;

nrgsoft.auth.model.Role = class extends WebModel {

    list(params = {}, ...observers) {
        this.post({
            target: '/auth/role/list',
            bodyParams: params
        }, {
            onSuccess: event => {
                event.collection = event.response.parsedBody;

                const data = [];
                event.collection.data.forEach(item => {
                    data.push(new Role(item));
                });

                event.collection.data = data;
            }
        }, ...observers);
    }

    create(params, ...observers) {
        this.post({
            target: '/auth/role/create',
            bodyParams: params
        }, {
            onSuccess: event => {
                event.entity = new Role(event.response.parsedBody);
            },
            onStatusCode422: event => {
                event.errorMessages = event.response.parsedBody;
            }
        }, ...observers);
    }

    details(params, ...observers) {
        this.post({
            target: '/auth/role/details',
            bodyParams: params
        }, {
            onSuccess: event => {
                event.entity = new Role(event.response.parsedBody);
            }
        }, ...observers);
    }

    update(params, ...observers) {
        this.post({
            target: '/auth/role/update',
            bodyParams: params
        }, {
            onSuccess: event => {
                event.entity = new Role(event.response.parsedBody);
            },
            onStatusCode422: event => {
                event.errorMessages = event.response.parsedBody;
            }
        }, ...observers);
    }
}
