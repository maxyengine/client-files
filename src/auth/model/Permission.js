const
    WebModel = nrgsoft.sdk.web.Model,
    Permissions = nrgsoft.auth.value.Permissions;

nrgsoft.auth.model.Permission = class extends WebModel {

    list(params, ...observers) {
        this.post({
            target: '/auth/permission/list',
            bodyParams: params
        }, {
            onSuccess: event => {
                event.collection = new Permissions(...event.response.parsedBody);
            }
        }, ...observers);
    }
}
