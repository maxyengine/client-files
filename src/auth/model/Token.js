const
    Model = nrgsoft.auth.model.Model;

nrgsoft.auth.model.Token = class extends Model {

    renew() {
        return this.fetchToken('/renew');
    }
}
