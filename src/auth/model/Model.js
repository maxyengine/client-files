const
    Model = nrgsoft.sdk.web.Model,
    Factory = nrgsoft.auth.entity.Factory;

nrgsoft.auth.model.Model = class extends Model {

    static get services() {
        return {
            ...Model.services,
            factory: Factory
        };
    }

    get defaults() {
        return {
            userApiUrl: '/auth/user',
            tokenApiUrl: '/auth/token'
        };
    }

    fetchUser(url, body = {}) {
        return this.fetch(this.userApiUrl + url, {body: body})
            .then(json => {
                return this.factory.createUser(json);
            });
    }

    fetchUsers(url, body = {}) {
        return this.fetch(this.userApiUrl + url, {body: body})
            .then(json => {
                return this.factory.createUsers(json);
            });
    }

    fetchToken(url, body = {}) {
        return this.fetch(this.tokenApiUrl + url, {body: body})
            .then(json => {
                return this.factory.createToken(json);
            });
    }
}
