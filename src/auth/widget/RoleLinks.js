const
    ListContainer = nrgsoft.widget.ListContainer,
    VisualList = nrgsoft.sdk.gui.observer.VisualList,
    Link = nrgsoft.widget.base.Link;

nrgsoft.auth.widget.RoleLinks = class extends ListContainer {

    static get services() {
        return {
            ...ListContainer.services,
            router: 'router'
        };
    }

    set roles(roles) {
        const data = [];

        roles.forEach(role => {
            data.push(new Link({
                router: this.router,
                url: this.router.createUrl('/auth/role/details', {id: role.id}),
                content: role.name
            }));
        });

        this.data = data;
    }
}
