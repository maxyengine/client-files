const
    List = nrgsoft.sdk.data.List,
    Arr = nrgsoft.sdk.lang.utility.Array,
    Template = nrgsoft.auth.widget.rolesList.Tpl,
    VisualList = nrgsoft.sdk.gui.observer.VisualList,
    CheckboxElement = nrgsoft.widget.form.CheckboxElement;

nrgsoft.auth.widget.RolesList = class extends List {

    get traits() {
        return [
            Template,
            VisualList
        ];
    }

    get itemClass() {
        return CheckboxElement;
    }

    set collection(collection) {
        const data = [];

        collection.data.forEach(entity => {
            data.push({
                entityId: entity.id,
                caption: entity.name
            });
        });

        this.data = data;
    }

    // noinspection JSAnnotator
    set value(value) {
        this.forEach(item => {
            item.value = Arr.has(value, item.entityId);
        });
    }

    get value() {
        const value = [];
        this.forEach(item => {
            if (item.value) {
                value.push(item.entityId);
            }
        });

        return value;
    }
}
