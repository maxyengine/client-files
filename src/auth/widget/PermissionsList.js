const
    NestedList = nrgsoft.widget.NestedList,
    Template = nrgsoft.auth.widget.permissionsList.Tpl,
    VisualNestedList = nrgsoft.sdk.gui.observer.VisualNestedList,
    OnToggleCheck = nrgsoft.auth.widget.permissionsList.OnToggleCheck,
    Arr = nrgsoft.sdk.lang.utility.Array;

const self = nrgsoft.auth.widget.PermissionsList = class extends NestedList {

    get traits() {
        return [
            Template,
            VisualNestedList,
            OnToggleCheck
        ];
    }

    // noinspection JSAnnotator
    set value(value) {
        this.forEach(item => {
            if (item instanceof this.itemClass) {
                item.checked = Arr.has(value, item.value);
            }
        });
    }

    get value() {
        const value = [];
        this.forEach(item => {
            if (item instanceof this.itemClass && item.checked) {
                value.push(item.value);
            }
        });

        return value;
    }
}
