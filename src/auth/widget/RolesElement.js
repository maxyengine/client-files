const
    Element = nrgsoft.sdk.gui.utility.Element,
    ClassName = nrgsoft.sdk.gui.utility.ClassName,
    EngineFormElement = nrgsoft.sdk.form.Element,
    EngineGuiWidgetAbility = nrgsoft.sdk.gui.WidgetAbility,
    EngineAuthWidgetRolesList = nrgsoft.auth.widget.RolesList;

nrgsoft.auth.widget.RolesElement = class extends nrgsoft.sdk.form.Element {
            
    set label(value) {
        if (this.it.t) {
            value = this.it.t(value);
        }    
        this._e2.nodeValue = value;
        
    }

    get label() {
        return this._e2.nodeValue;
    }
                
    set isRequired(value) {
        value = !value;
        
        if (value) {
            ClassName.add(this._e3, 'd-none');
        } else {
            ClassName.remove(this._e3, 'd-none');
        }
        
    }

    get isRequired() {
        return !ClassName.has(this._e3, 'd-none')
    }
                
    set isValid(value) {
        if (undefined === value) {
            ClassName.remove(this._e5, 'is-valid', 'is-invalid');
        } else if (value) {
            ClassName.replace(this._e5, 'is-invalid', 'is-valid');
        } else {
            ClassName.replace(this._e5, 'is-valid', 'is-invalid');
        }
        
    }
    
    get isValid() {
        return ClassName.has(this._e5, 'is-valid');
    }
            
    set helpText(value) {
        if (this.it.t) {
            value = this.it.t(value);
        }    
        this._e7.nodeValue = value;
        
    }

    get helpText() {
        return this._e7.nodeValue;
    }
                
    set errorMessage(value) {
        if (this.it.t) {
            value = this.it.t(value);
        }    
        this._e9.nodeValue = value;
        this.it.trigger('setErrorMessage', {value: value});
    }

    get errorMessage() {
        return this._e9.nodeValue;
    }
            
    get list() {
        if (!this._list) {
            const properties = {t: this.it.t};
            this._list = this.injector ?
                this.injector.createObject(nrgsoft.auth.widget.RolesList, properties) :
                new nrgsoft.auth.widget.RolesList(properties);
                
            Element.append(this._e10, this._list);
        }

        return this._list;
    }
                
    get value() {
        return this.list.value;
    }
    
    set value(value) {
        this.list.value = value;
        
    }
                
    get collection() {
        return this.list.collection;
    }
    
    set collection(value) {
        this.list.collection = value;
        
    }
            
    static get services() {
        return {
            ...(nrgsoft.sdk.form.Element.services || {}),
            
        };
    }
            
    get traits() {
        return [
            ...(super.traits || []),
            nrgsoft.sdk.gui.WidgetAbility
        ];
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._element.setAttribute('class', 'form-group');
        this._e1 = document.createElement('label');
        this._element.appendChild(this._e1);
        this._e2 = document.createTextNode('');
        this._e1.appendChild(this._e2);
        this._e3 = document.createElement('span');
        this._element.appendChild(this._e3);
        this._e4 = document.createTextNode('*');
        this._e3.appendChild(this._e4);
        this._e5 = document.createElement('input');
        this._e5.setAttribute('type', 'hidden');
        this._e5.setAttribute('class', 'form-control');
        this._element.appendChild(this._e5);
        this._e6 = document.createElement('small');
        this._e6.setAttribute('class', 'form-text text-muted');
        this._element.appendChild(this._e6);
        this._e7 = document.createTextNode('');
        this._e6.appendChild(this._e7);
        this._e8 = document.createElement('div');
        this._e8.setAttribute('class', 'invalid-feedback');
        this._element.appendChild(this._e8);
        this._e9 = document.createTextNode('');
        this._e8.appendChild(this._e9);
        this._e10 = document.createElement('div');
        this._element.appendChild(this._e10);
    
        super._constructor(properties);
        this.isRequired = !!this.isRequired; 
        
    
    }
}
