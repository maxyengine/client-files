const
    Element = nrgsoft.sdk.gui.utility.Element,
    ClassName = nrgsoft.sdk.gui.utility.ClassName,
    EngineWidgetFormCheckboxElement = nrgsoft.widget.form.CheckboxElement,
    EngineGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.auth.widget.permissionsList.PermissionItem = class extends nrgsoft.sdk.gui.Widget {
        
    get checkbox() {
        if (!this._checkbox) {
            const properties = {t: this.it.t};
            this._checkbox = this.injector ?
                this.injector.createObject(nrgsoft.widget.form.CheckboxElement, properties) :
                new nrgsoft.widget.form.CheckboxElement(properties);
                
            Element.append(this._element, this._checkbox);
        }

        return this._checkbox;
    }
                
    get checked() {
        return this.checkbox.value;
    }
    
    set checked(value) {
        this.checkbox.value = value;
        
    }
                
    get caption() {
        return this.checkbox.caption;
    }
    
    set caption(value) {
        this.checkbox.caption = value;
        
    }
            
    static get services() {
        return {
            ...(nrgsoft.sdk.gui.Widget.services || {}),
            
        };
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('li');
        this._element.setAttribute('class', 'nav-item');
    
        super._constructor(properties);
        
        this.it.addEvent(this.checkbox, 'change', 'toggleCheck');
    
    }
}
