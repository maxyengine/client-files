const
    Observer = nrgsoft.sdk.rx.Observer;

nrgsoft.auth.widget.permissionsList.OnToggleCheck = class extends Observer {

    get properties() {
        return {
            list: 'owner.list',
            checked: 'owner.checked'
        };
    }

    onToggleCheck() {
        this.list.forEach(item => {
            item.checked = this.checked;
        });
    }

    onSetList(event) {
        const list = event.list;

        list.forEach(item => {
            item.on('toggleCheck', () => {
                if (!item.checked) {
                    this.checked = false;
                } else {
                    let temp = true;
                    list.forEach(item => {
                        if (!item.checked) {
                            temp = false;
                        }
                    });
                    this.checked = temp;
                }
            });
        });
    }
}
