const
    Element = nrgsoft.sdk.gui.utility.Element,
    ClassName = nrgsoft.sdk.gui.utility.ClassName,
    EngineDataNestedListItem = nrgsoft.sdk.data.nestedList.Item,
    EngineGuiWidgetAbility = nrgsoft.sdk.gui.WidgetAbility,
    EngineAuthWidgetPermissionsListOnToggleCheck = nrgsoft.auth.widget.permissionsList.OnToggleCheck,
    EngineWidgetNestedListOnToggleCollapse = nrgsoft.widget.nestedList.OnToggleCollapse,
    EngineWidgetFormCheckboxElement = nrgsoft.widget.form.CheckboxElement;

nrgsoft.auth.widget.permissionsList.ResourceItem = class extends nrgsoft.sdk.data.nestedList.Item {

    set collapsed(value) {
        if (undefined === value) {
            ClassName.remove(this._e1, 'fa-caret-right', 'fa-caret-down');
        } else if (value) {
            ClassName.replace(this._e1, 'fa-caret-down', 'fa-caret-right');
        } else {
            ClassName.replace(this._e1, 'fa-caret-right', 'fa-caret-down');
        }
        
        
    }
    
    get collapsed() {
        return ClassName.has(this._e1, 'fa-caret-right');
    }
    
    set expanded(value) {
        this.collapsed = !value;
    }
    
    get expanded() {
        return ClassName.has(this._e1, 'fa-caret-down');
    }
            
    get checkbox() {
        if (!this._checkbox) {
            const properties = {t: this.it.t};
            this._checkbox = this.injector ?
                this.injector.createObject(nrgsoft.widget.form.CheckboxElement, properties) :
                new nrgsoft.widget.form.CheckboxElement(properties);
                
            Element.append(this._e2, this._checkbox);
        }

        return this._checkbox;
    }
                
    get checked() {
        return this.checkbox.value;
    }
    
    set checked(value) {
        this.checkbox.value = value;
        
    }
                
    get caption() {
        return this.checkbox.caption;
    }
    
    set caption(value) {
        this.checkbox.caption = value;
        
    }
            
    static get services() {
        return {
            ...(nrgsoft.sdk.data.nestedList.Item.services || {}),
            injector: 'injector'
        };
    }
            
    get traits() {
        return [
            ...(super.traits || []),
            nrgsoft.sdk.gui.WidgetAbility,
			nrgsoft.auth.widget.permissionsList.OnToggleCheck,
			nrgsoft.widget.nestedList.OnToggleCollapse
        ];
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('li');
        this._element.setAttribute('class', 'nav-item');
        this._e1 = document.createElement('i');
        this._e1.setAttribute('class', 'fas fa-lg float-left mt-1');
        this._e1.setAttribute('aria-hidden', 'true');
        this._element.appendChild(this._e1);
        this._e2 = document.createElement('div');
        this._e2.setAttribute('class', 'ml-4');
        this._element.appendChild(this._e2);
    
        super._constructor(properties);
        this.it.addEvent(this._e1, 'click', 'toggleCollapse');
        
        this.it.addEvent(this.checkbox, 'change', 'toggleCheck');
    
    }
}
