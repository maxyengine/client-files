const
    Component = nrgsoft.sdk.rx.Component,
    Template = nrgsoft.auth.widget.accountSummary.Tpl,
    WidgetActivity = nrgsoft.sdk.gui.trigger.WidgetActivity;

nrgsoft.auth.widget.AccountSummary = class extends Component {

    static get services() {
        return {
            injector: 'injector'
        };
    }

    get traits() {
        return [
            Template,
            WidgetActivity
        ];
    }

    set user(user) {
        this.set({
            name: user.name,
            description: user.description,
            hasAvatar: !!user.avatar,
            avatar: user.avatar,
        });
    }

    onAskToToggle(event) {
        event.preventDefault();

        this.isShown = this.isHidden;
    }

    onDeactivateWidget() {
        this.isShown = false;
    }
}
