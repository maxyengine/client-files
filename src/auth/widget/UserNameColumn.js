const
    Element = nrgsoft.sdk.gui.utility.Element,
    ClassName = nrgsoft.sdk.gui.utility.ClassName,
    EngineWebUrl = nrgsoft.sdk.web.Url,
    EngineAuthWidgetRoleLinks = nrgsoft.auth.widget.RoleLinks,
    EngineGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.auth.widget.UserNameColumn = class extends nrgsoft.sdk.gui.Widget {
            
    set hasAvatar(value) {
        value = value;
        
        if (value) {
            ClassName.add(this._e1, 'has-avatar');
        } else {
            ClassName.remove(this._e1, 'has-avatar');
        }
        
    }

    get hasAvatar() {
        return ClassName.has(this._e1, 'has-avatar')
    }
                    
    set avatarUrl(value) {
        this._e2.href = value;
        
    }

    get avatarUrl() {
        return this._e2.href;
    }
                    
    set avatar(value) {
        this._e3.src = value;
        
    }

    get avatar() {
        return this._e3.src;
    }
                    
    set nameUrl(value) {
        this._e7.href = value;
        
    }

    get nameUrl() {
        return this._e7.href;
    }
                
    set name(value) {
        if (this.it.t) {
            value = this.it.t(value);
        }    
        this._e8.nodeValue = value;
        
    }

    get name() {
        return this._e8.nodeValue;
    }
            
    get roleLinks() {
        if (!this._roleLinks) {
            const properties = {t: this.it.t};
            this._roleLinks = this.injector ?
                this.injector.createObject(nrgsoft.auth.widget.RoleLinks, properties) :
                new nrgsoft.auth.widget.RoleLinks(properties);
                
            Element.append(this._e9, this._roleLinks);
        }

        return this._roleLinks;
    }
                
    get roles() {
        return this.roleLinks.roles;
    }
    
    set roles(value) {
        this.roleLinks.roles = value;
        
    }
            
    static get services() {
        return {
            ...(nrgsoft.sdk.gui.Widget.services || {}),
            
        };
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._element.setAttribute('class', 'user-name-column');
        this._e1 = document.createElement('div');
        this._e1.setAttribute('class', 'user-avatar');
        this._element.appendChild(this._e1);
        this._e2 = document.createElement('a');
        this._e2.setAttribute('href', '');
        this._e2.addEventListener('click', event => {
            event.preventDefault();
            this.router.goTo(new nrgsoft.sdk.web.Url(this.avatarUrl));
        });
        this._e1.appendChild(this._e2);
        this._e3 = document.createElement('img');
        this._e3.setAttribute('alt', 'avatar');
        this._e3.setAttribute('src', '');
        this._e2.appendChild(this._e3);
        this._e4 = document.createElement('i');
        this._e4.setAttribute('class', 'fas fa-user-circle');
        this._e2.appendChild(this._e4);
        this._e5 = document.createElement('div');
        this._e5.setAttribute('class', 'user-info');
        this._element.appendChild(this._e5);
        this._e6 = document.createElement('div');
        this._e6.setAttribute('class', 'account-name');
        this._e5.appendChild(this._e6);
        this._e7 = document.createElement('a');
        this._e7.setAttribute('href', '');
        this._e7.addEventListener('click', event => {
            event.preventDefault();
            this.router.goTo(new nrgsoft.sdk.web.Url(this.nameUrl));
        });
        this._e6.appendChild(this._e7);
        this._e8 = document.createTextNode('');
        this._e7.appendChild(this._e8);
        this._e9 = document.createElement('div');
        this._e9.setAttribute('class', 'user-description');
        this._e5.appendChild(this._e9);
        this._e10 = document.createElement('div');
        this._e10.setAttribute('class', 'clearfix');
        this._element.appendChild(this._e10);
    
        super._constructor(properties);
        this.hasAvatar = !!this.hasAvatar; 
        
    
    }
}
