const
    Value = nrgsoft.sdk.lang.Value,
    Url = nrgsoft.sdk.web.Url,
    Link = nrgsoft.widget.base.Link,
    Truncate = nrgsoft.widget.Truncate,
    Badge = nrgsoft.widget.tbody.Badge;

nrgsoft.auth.view.role.list.DisplayEntity = class extends Value {

    static get services() {
        return {
            router: 'router'
        };
    }

    get table() {
        return this.owner;
    }

    initialize() {
        this.table.body.on(this)
    }

    onBeforeAddItem(event) {
        const
            row = event.item,
            entityView = this.createView(row.entity);

        row.forEach(column => {
            column.content.insert(entityView[column.fieldName]);
        });
    }

    createView(entity) {
        return {
            ...entity,
            ...{
                name: new Link({
                    router: this.router,
                    url: this.router.createRelativeUrl('/details', {id: entity.id}),
                    content: entity.name
                }),
                description: new Truncate({
                    content: entity.description,
                    title: entity.description
                }),
                numberOfUsers: new Badge({
                    content: entity.numberOfUsers,
                    type: 'badge-info'
                }),
                createdAt: entity.createdAt.toLocaleString(),
                updatedAt: entity.updatedAt && entity.updatedAt.toLocaleString(),
            }
        };
    }
}
