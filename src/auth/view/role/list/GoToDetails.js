const
    Observer = nrgsoft.sdk.rx.Observer;

nrgsoft.auth.view.role.list.GoToDetails = class extends Observer {

    onAddItem(event) {
        event.item.on('click', (event, tableRow) => {
            this.owner.router.goToRelative('/details/view', {id: tableRow.entity.id});
        });
    }
}
