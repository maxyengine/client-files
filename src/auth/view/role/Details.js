const
    Element = nrgsoft.sdk.gui.utility.Element,
    ClassName = nrgsoft.sdk.gui.utility.ClassName,
    EngineWidgetNestedList = nrgsoft.widget.NestedList,
    EngineGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.auth.view.role.Details = class extends nrgsoft.sdk.gui.Widget {
            
    set name(value) {
        if (this.it.t) {
            value = this.it.t(value);
        }    
        this._e2.nodeValue = value;
        
    }

    get name() {
        return this._e2.nodeValue;
    }
                
    set createdAt(value) {
        if (this.it.t) {
            value = this.it.t(value);
        }    
        this._e6.nodeValue = value;
        
    }

    get createdAt() {
        return this._e6.nodeValue;
    }
                
    set hasUpdated(value) {
        value = !value;
        
        if (value) {
            ClassName.add(this._e7, 'd-none');
        } else {
            ClassName.remove(this._e7, 'd-none');
        }
        
    }

    get hasUpdated() {
        return !ClassName.has(this._e7, 'd-none')
    }
                
    set updatedAt(value) {
        if (this.it.t) {
            value = this.it.t(value);
        }    
        this._e10.nodeValue = value;
        
    }

    get updatedAt() {
        return this._e10.nodeValue;
    }
                
    set description(value) {
        if (this.it.t) {
            value = this.it.t(value);
        }    
        this._e12.nodeValue = value;
        
    }

    get description() {
        return this._e12.nodeValue;
    }
            
    get permissionList() {
        if (!this._permissionList) {
            const properties = {t: this.it.t};
            this._permissionList = this.injector ?
                this.injector.createObject(nrgsoft.widget.NestedList, properties) :
                new nrgsoft.widget.NestedList(properties);
                
            Element.append(this._e13, this._permissionList);
        }

        return this._permissionList;
    }
                
    get permissionNestedList() {
        return this.permissionList.data;
    }
    
    set permissionNestedList(value) {
        this.permissionList.data = value;
        
    }
            
    static get services() {
        return {
            ...(nrgsoft.sdk.gui.Widget.services || {}),
            
        };
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = document.createElement('h2');
        this._element.appendChild(this._e1);
        this._e2 = document.createTextNode('');
        this._e1.appendChild(this._e2);
        this._e3 = document.createElement('small');
        this._e3.setAttribute('class', 'd-block text-muted');
        this._element.appendChild(this._e3);
        this._e4 = document.createTextNode('Created At: ');
        this._e3.appendChild(this._e4);
        this._e5 = document.createElement('span');
        this._e3.appendChild(this._e5);
        this._e6 = document.createTextNode('');
        this._e5.appendChild(this._e6);
        this._e7 = document.createElement('small');
        this._e7.setAttribute('class', 'text-muted');
        this._element.appendChild(this._e7);
        this._e8 = document.createTextNode('Updated At: ');
        this._e7.appendChild(this._e8);
        this._e9 = document.createElement('span');
        this._e7.appendChild(this._e9);
        this._e10 = document.createTextNode('');
        this._e9.appendChild(this._e10);
        this._e11 = document.createElement('p');
        this._element.appendChild(this._e11);
        this._e12 = document.createTextNode('');
        this._e11.appendChild(this._e12);
        this._e13 = document.createElement('div');
        this._element.appendChild(this._e13);
    
        super._constructor(properties);
        this.hasUpdated = !!this.hasUpdated; 
        
    
    }
}
