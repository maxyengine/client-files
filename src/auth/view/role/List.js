const
    Component = nrgsoft.sdk.rx.Component,
    Template = nrgsoft.auth.view.role.list.Tpl,
    Condition = nrgsoft.sdk.data.Condition,
    DisplayEntity = nrgsoft.auth.view.role.list.DisplayEntity,
    Sortable = nrgsoft.widget.table.Sortable,
    Filterable = nrgsoft.widget.table.Filterable;

nrgsoft.auth.view.role.List = class extends Component {

    static get services() {
        return {
            injector: 'injector'
        };
    }

    get traits() {
        return [Template];
    }

    get limits () {
        return [30, 50, 100];
    }

    get fields() {
        return [
            {name: 'name', caption: 'name', sortable: true, conditions: [Condition.equal, Condition.notEqual]},
            {name: 'description', caption: 'description'},
            {name: 'numberOfUsers', caption: 'num users', sortable: true, conditions: [Condition.equal]},
            {name: 'createdAt', caption: 'created', sortable: true, conditions: [Condition.equal, Condition.dateTimeRange]},
            {name: 'updatedAt', caption: 'updated', conditions: [Condition.equal]}
        ]
    }

    get panelList() {
        return [
            {
                widget: this.form,
                icon: 'plus',
                header: 'create new role'
            },
            {
                widget: this.filter,
                icon: 'filter',
                header: 'filter'
            }
        ];
    }

    set collection(collection) {
        this.filter
            .set({fields: this.fields})
            .build();

        this.pagination
            .set({
                limit: collection.limit,
                offset: collection.offset,
                total: collection.total
            })
            .build()
            .activateItem();

        this.limitation
            .set({limits: this.limits})
            .build()
            .activateItem();

        this.table
            .set({
                fields: this.fields,
                collection: collection.data
            })
            .build();
    }

    initialize() {
        this.table.use(
            DisplayEntity,
            Sortable,
            [Filterable, {filter: this.filter}]
        );

        this.panels
            .set({
                panels: this.panelList
            })
            .build();
    }

    onAskToShowForm() {
        this.panels.showPanel(this.form);
    }

    onAskToShowFilter() {
        this.panels.showPanel(this.filter);
    }
}
