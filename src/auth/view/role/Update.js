const
    BaseForm = nrgsoft.widget.VerticalForm,
    TextElement = nrgsoft.widget.form.TextElement,
    TextareaElement = nrgsoft.widget.form.TextareaElement,
    Trim = nrgsoft.sdk.form.filter.Trim,
    Required = nrgsoft.sdk.form.validator.Required,
    TypeString = nrgsoft.sdk.form.validator.TypeString,
    Length = nrgsoft.sdk.form.validator.Length,
    SubmitButton = nrgsoft.widget.form.SubmitButton;

nrgsoft.auth.view.role.Update = class extends BaseForm {

    initialize() {
        const
            name = new TextElement({
                name: 'name',
                label: this.t('name'),
                isRequired: true,
                value: '',
            })
                .addFilter(new Trim())
                .addValidator(new Required())
                .addValidator(new TypeString())
                .addValidator(new Length({
                    min: 1,
                    max: 50,
                })),

            description = new TextareaElement({
                name: 'description',
                label: this.t('description'),
                isRequired: true,
                value: ''
            })
                .addFilter(new Trim())
                .addValidator(new Required()),

            submit = new SubmitButton({caption: this.t('save')});

        this
            .addItem(name)
            .addItem(description)
            .addItem(submit);
    }
}
