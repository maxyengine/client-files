const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.auth.view.user.DeniedAccess = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._element.setAttribute('class', 'nrgsoft-auth__user-denied-access');
        this._e1 = document.createElement('h1');
        this._element.appendChild(this._e1);
        this._e2 = document.createTextNode('permission denied');
        this._e1.appendChild(this._e2);
            
    this._element.classList.add('nrgsoft-auth-view-user-DeniedAccess');
    
        super._constructor(properties);
    
    }
}
