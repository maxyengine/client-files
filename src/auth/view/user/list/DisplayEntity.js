const
    Value = nrgsoft.sdk.lang.Value,
    Url = nrgsoft.sdk.web.Url,
    Truncate = nrgsoft.widget.Truncate,
    Link = nrgsoft.widget.base.Link,
    UserNameColumn = nrgsoft.auth.widget.UserNameColumn,
    RoleLinks = nrgsoft.auth.widget.RoleLinks,
    Status = nrgsoft.auth.view.user.list.Status;

nrgsoft.auth.view.user.list.DisplayEntity = class extends Value {

    static get services() {
        return {
            injector: 'injector',
            router: 'router'
        };
    }

    get table() {
        return this.owner;
    }

    initialize() {
        this.table.body.on(this)
    }

    onBeforeAddItem(event) {
        const
            row = event.item,
            entityView = this.createView(row.entity);

        row.forEach(column => {
            column.content.insert(entityView[column.fieldName]);
        });
    }

    createView(entity) {
        const url = this.router.createRelativeUrl('/details', {id: entity.id});

        return {
            ...entity,
            ...{
                name: new UserNameColumn({
                    injector: this.injector,
                    router: this.router,
                    avatarUrl: url,
                    nameUrl: url,
                    name: entity.name,
                    description: entity.description,
                    hasAvatar: !!entity.avatar,
                    avatar: entity.avatar,
                    roles: entity.roles
                }),
                description: new Truncate({
                    content: entity.description,
                    title: entity.description
                }),
                status: new Status({
                    content: entity.status,
                    value: 'user-status-' + entity.status
                }),
                createdAt: entity.createdAt.toLocaleString(),
                updatedAt: entity.updatedAt && entity.updatedAt.toLocaleString(),
            }
        };
    }
}
