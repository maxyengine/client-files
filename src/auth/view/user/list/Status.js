const
    Element = nrgsoft.sdk.gui.utility.Element,
    ClassName = nrgsoft.sdk.gui.utility.ClassName,
    EngineGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.auth.view.user.list.Status = class extends nrgsoft.sdk.gui.Widget {
        
    set value(value) {
        ClassName.remove(this._element, ...['user-status-active','user-status-pending']);
         
        if (['user-status-active','user-status-pending'].indexOf(value) > -1) {
            ClassName.add(this._element, value);
        }
      
        
    }
    
    get value() {
        for (let i = 0; i < ['user-status-active','user-status-pending'].length; i++) {
            if (ClassName.has(this._element, ['user-status-active','user-status-pending'][i])) {
                return ['user-status-active','user-status-pending'][i];
            }        
        }
    }
            
    set content(value) {
        if (this.it.t) {
            value = this.it.t(value);
        }    
        this._e1.nodeValue = value;
        
    }

    get content() {
        return this._e1.nodeValue;
    }
            
    static get services() {
        return {
            ...(nrgsoft.sdk.gui.Widget.services || {}),
            
        };
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = document.createTextNode('');
        this._element.appendChild(this._e1);
    
        super._constructor(properties);
    
    }
}
