const
    Element = nrgsoft.sdk.gui.utility.Element,
    ClassName = nrgsoft.sdk.gui.utility.ClassName,
    EngineAuthFormUserActivate = nrgsoft.auth.form.user.Activate,
    EngineGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.auth.view.user.Activate = class extends nrgsoft.sdk.gui.Widget {
            
    set header(value) {
        if (this.it.t) {
            value = this.it.t(value);
        }    
        this._e2.nodeValue = value;
        
    }

    get header() {
        return this._e2.nodeValue;
    }
            
    get form() {
        if (!this._form) {
            const properties = {t: this.it.t};
            this._form = this.injector ?
                this.injector.createObject(nrgsoft.auth.form.user.Activate, properties) :
                new nrgsoft.auth.form.user.Activate(properties);
                
            Element.append(this._e3, this._form);
        }

        return this._form;
    }
            
    static get services() {
        return {
            ...(nrgsoft.sdk.gui.Widget.services || {}),
            
        };
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._element.setAttribute('class', 'auth-form');
        this._e1 = document.createElement('h1');
        this._element.appendChild(this._e1);
        this._e2 = document.createTextNode('activate account');
        this._e1.appendChild(this._e2);
        this._e3 = document.createElement('div');
        this._element.appendChild(this._e3);
    
        super._constructor(properties);
        
    
    }
}
