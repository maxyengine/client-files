const
    Element = nrgsoft.sdk.gui.utility.Element,
    ClassName = nrgsoft.sdk.gui.utility.ClassName,
    EngineAuthFormUserElementAvatar = nrgsoft.auth.form.user.element.Avatar,
    EngineWidgetNestedList = nrgsoft.widget.NestedList,
    EngineAuthWidgetRoleLinks = nrgsoft.auth.widget.RoleLinks,
    EngineGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.auth.view.user.Profile = class extends nrgsoft.sdk.gui.Widget {
            
    set header(value) {
        if (this.it.t) {
            value = this.it.t(value);
        }    
        this._e2.nodeValue = value;
        
    }

    get header() {
        return this._e2.nodeValue;
    }
                
    set hasAvatar(value) {
        value = value;
        
        if (value) {
            ClassName.add(this._e3, 'has-avatar');
        } else {
            ClassName.remove(this._e3, 'has-avatar');
        }
        
    }

    get hasAvatar() {
        return ClassName.has(this._e3, 'has-avatar')
    }
                    
    set avatar(value) {
        this._e4.src = value;
        
    }

    get avatar() {
        return this._e4.src;
    }
            
    get avatarFile() {
        if (!this._avatarFile) {
            const properties = {t: this.it.t};
            this._avatarFile = this.injector ?
                this.injector.createObject(nrgsoft.auth.form.user.element.Avatar, properties) :
                new nrgsoft.auth.form.user.element.Avatar(properties);
                
            Element.append(this._e6, this._avatarFile);
        }

        return this._avatarFile;
    }
                
    set name(value) {
        if (this.it.t) {
            value = this.it.t(value);
        }    
        this._e8.nodeValue = value;
        
    }

    get name() {
        return this._e8.nodeValue;
    }
                
    set createdAt(value) {
        if (this.it.t) {
            value = this.it.t(value);
        }    
        this._e12.nodeValue = value;
        
    }

    get createdAt() {
        return this._e12.nodeValue;
    }
                
    set hasUpdated(value) {
        value = !value;
        
        if (value) {
            ClassName.add(this._e13, 'd-none');
        } else {
            ClassName.remove(this._e13, 'd-none');
        }
        
    }

    get hasUpdated() {
        return !ClassName.has(this._e13, 'd-none')
    }
                
    set updatedAt(value) {
        if (this.it.t) {
            value = this.it.t(value);
        }    
        this._e16.nodeValue = value;
        
    }

    get updatedAt() {
        return this._e16.nodeValue;
    }
                
    set description(value) {
        if (this.it.t) {
            value = this.it.t(value);
        }    
        this._e18.nodeValue = value;
        
    }

    get description() {
        return this._e18.nodeValue;
    }
            
    get permissionList() {
        if (!this._permissionList) {
            const properties = {t: this.it.t};
            this._permissionList = this.injector ?
                this.injector.createObject(nrgsoft.widget.NestedList, properties) :
                new nrgsoft.widget.NestedList(properties);
                
            Element.append(this._e19, this._permissionList);
        }

        return this._permissionList;
    }
                
    get permissionNestedList() {
        return this.permissionList.data;
    }
    
    set permissionNestedList(value) {
        this.permissionList.data = value;
        
    }
            
    get roleLinks() {
        if (!this._roleLinks) {
            const properties = {t: this.it.t};
            this._roleLinks = this.injector ?
                this.injector.createObject(nrgsoft.auth.widget.RoleLinks, properties) :
                new nrgsoft.auth.widget.RoleLinks(properties);
                
            Element.append(this._e20, this._roleLinks);
        }

        return this._roleLinks;
    }
                
    get roles() {
        return this.roleLinks.roles;
    }
    
    set roles(value) {
        this.roleLinks.roles = value;
        
    }
            
    static get services() {
        return {
            ...(nrgsoft.sdk.gui.Widget.services || {}),
            injector: 'injector'
        };
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = document.createElement('h1');
        this._e1.setAttribute('class', 'header');
        this._element.appendChild(this._e1);
        this._e2 = document.createTextNode('profile');
        this._e1.appendChild(this._e2);
        this._e3 = document.createElement('div');
        this._e3.setAttribute('class', 'profile-avatar');
        this._element.appendChild(this._e3);
        this._e4 = document.createElement('img');
        this._e4.setAttribute('alt', 'avatar');
        this._e4.setAttribute('src', '');
        this._e3.appendChild(this._e4);
        this._e5 = document.createElement('i');
        this._e5.setAttribute('class', 'fas fa-user-circle');
        this._e3.appendChild(this._e5);
        this._e6 = document.createElement('div');
        this._element.appendChild(this._e6);
        this._e7 = document.createElement('h2');
        this._element.appendChild(this._e7);
        this._e8 = document.createTextNode('');
        this._e7.appendChild(this._e8);
        this._e9 = document.createElement('small');
        this._e9.setAttribute('class', 'd-block text-muted');
        this._element.appendChild(this._e9);
        this._e10 = document.createTextNode('Created At: ');
        this._e9.appendChild(this._e10);
        this._e11 = document.createElement('span');
        this._e9.appendChild(this._e11);
        this._e12 = document.createTextNode('');
        this._e11.appendChild(this._e12);
        this._e13 = document.createElement('small');
        this._e13.setAttribute('class', 'text-muted');
        this._element.appendChild(this._e13);
        this._e14 = document.createTextNode('Updated At: ');
        this._e13.appendChild(this._e14);
        this._e15 = document.createElement('span');
        this._e13.appendChild(this._e15);
        this._e16 = document.createTextNode('');
        this._e15.appendChild(this._e16);
        this._e17 = document.createElement('p');
        this._element.appendChild(this._e17);
        this._e18 = document.createTextNode('');
        this._e17.appendChild(this._e18);
        this._e19 = document.createElement('div');
        this._element.appendChild(this._e19);
        this._e20 = document.createElement('div');
        this._element.appendChild(this._e20);
    
        super._constructor(properties);
        this.hasAvatar = !!this.hasAvatar; 
        this.it.trigger('appendWidget', {widget: this.avatarFile});
        this.it.addEvent(this.avatarFile, 'selectFiles', 'selectAvatarFile');
        this.hasUpdated = !!this.hasUpdated; 
        
        
    
    }
}
