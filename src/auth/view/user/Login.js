const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftAuthFormUserLogin = nrgsoft.auth.form.user.Login,
    NrgsoftWidgetBaseButton = nrgsoft.widget.base.Button,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.auth.view.user.Login = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
                
    get form() {
                
        if (!this._form) {
            const
                properties = {},
                services = {
                    t: this.it.t
                };
            
            this._form = this.injector ?
                this.injector.createObject(nrgsoft.auth.form.user.Login, properties, services) :
                new nrgsoft.auth.form.user.Login(properties, services);
        }

        return this._form;
    
    }
                
    get _e6_() {
                
        if (!this.__e6_) {
            const
                properties = {"content":"login","type":"submit"},
                services = {
                    t: this.it.t
                };
            
            this.__e6_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.Button, properties, services) :
                new nrgsoft.widget.base.Button(properties, services);
        }

        return this.__e6_;
    
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('form');
        this._e1 = document.createElement('div');
            
        this._e1.classList.add('nrgsoft-auth-view-user-Login__content');
        this._element.appendChild(this._e1);
        this._e2 = document.createElement('h1');
        this._e1.appendChild(this._e2);
        this._e3 = document.createTextNode('login');
        this._e2.appendChild(this._e3);
        this._e4 = this.form.element;
        this._e1.appendChild(this._e4);
        this._e5 = document.createElement('div');
            
        this._e5.classList.add('nrgsoft-auth-view-user-Login__footer');
        this._e1.appendChild(this._e5);
        this._e6 = this._e6_.element;
        this._e5.appendChild(this._e6);
            
    this._element.classList.add('nrgsoft-auth-view-user-Login');
    
        super._constructor(properties);
        this.it.mapEvent(this._element, 'submit', 'submit');
    
    }
}
