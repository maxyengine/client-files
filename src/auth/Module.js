const
    AppModule = nrgsoft.sdk.app.Module,
    RoleList = nrgsoft.auth.action.role.List,
    RoleDetails = nrgsoft.auth.action.role.Details,
    RoleUpdate = nrgsoft.auth.action.role.Update,
    UserList = nrgsoft.auth.action.user.List,
    UserProfile = nrgsoft.auth.action.user.Profile,
    UserActivate = nrgsoft.auth.action.user.Activate,
    UserLogin = nrgsoft.auth.action.user.Login,
    UserLogout = nrgsoft.auth.action.user.Logout,
    UserForgotPassword = nrgsoft.auth.action.user.ForgotPassword;

nrgsoft.auth.Module = class extends AppModule {

    get routes() {
        return {
            '/auth/role/list': RoleList,
            '/auth/role/details': RoleDetails,
            '/auth/role/update': RoleUpdate,
            '/auth/user/list': UserList,
            '/auth/user/profile': UserProfile,
            '/auth/user/activate': UserActivate,
            '/auth/user/login': UserLogin,
            '/auth/user/logout': UserLogout,
            '/auth/user/forgot-password': UserForgotPassword
        };
    }

    get permissions() {
        return {
            '/auth/role/list': ['/auth/role/list'],
            '/auth/role/details': ['/auth/role/details'],
            '/auth/role/create': ['/auth/role/create', '/auth/permission/list'], // additional permission
            '/auth/user/list': ['/auth/user/list'],
            '/auth/user/details': ['/auth/user/details'],
            '/auth/user/create': ['/auth/user/create', '/auth/role/list'], // additional permission
            '/auth/user/logout': ['/auth/user/logout']
        };
    }
}
