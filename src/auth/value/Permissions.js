nrgsoft.auth.value.Permissions = class {

    constructor(apiUrls) {
        this.apiUrls = apiUrls
    }

    get asNestedList() {
        let nestedList = {};
        this.apiUrls.forEach(apiUrl => {
            const exploded = apiUrl.path.substr(1).split('/');
            let temp = nestedList;
            exploded.forEach((key, index) => {
                if (index < exploded.length - 1) {
                    temp[key] = temp[key] || {};
                    temp = temp[key];
                } else {
                    temp[key] = apiUrl.path;
                }
            });
        });

        return nestedList;
    }

    isAllowed(url) {
        for (const apiUrl of this.apiUrls) {
            if (url.contains(apiUrl)) {
                return true;
            }
        }

        return false;
    }
}
