const
  WebController = nrgsoft.sdk.web.Controller,
  ForbiddenAction = nrgsoft.auth.action.user.ForbiddenAction,
  Type = nrgsoft.sdk.lang.utility.Type

nrgsoft.auth.service.Controller = class extends WebController {

  static get services () {
    return {
      ...WebController.services || {},
      accessControl: 'accessControl'
    }
  }

  get defaults () {
    return {
      ...super.defaults || {},
      forbiddenAction: ForbiddenAction,
      loginUrl: '/auth/user/login'
    }
  }

  executeAction (event) {
    return this.accessControl.loadToken()
      .then(token => {
        if (this.accessControl.isAllowed(event.url)) {
          return super.executeAction(event)
        }

        return token ? this.forbidden(event) : this.unauthorized(event.url)
      })
  }

  forbidden (event) {
    if (!Type.isObject(this.forbiddenAction)) {
      this.forbiddenAction = this.injector.createObject(this.forbiddenAction)
    }

    return this.forbiddenAction.execute(event)
  }

  unauthorized (backUrl) {
    this.accessControl.unauthorized()
    this.goTo(this.loginUrl, backUrl ? {backUrl: encodeURIComponent(backUrl.href)} : {})
  }
}
