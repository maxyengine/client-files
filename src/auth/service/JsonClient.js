const
    WebJsonClient = nrgsoft.sdk.web.JsonClient;

nrgsoft.auth.service.JsonClient = class extends WebJsonClient {

    static get services() {
        return {
            ...WebJsonClient.services || {},
            session: 'session'
        };
    }

    fetch(url, options = {}) {
        this.addAuthorizationHeader(options);

        return super.fetch(url, options);
    }

    upload(url, options = {}) {
        this.addAuthorizationHeader(options);

        return super.upload(url, options);
    }

    addAuthorizationHeader(options = {}) {
        if (this.session.isActive) {
            options.headers = options.headers || new Headers();
            options.headers.set('Authorization', this.session.id);
        }
    }

    createAuthorizedUrl(url) {
        return this.createUrl(url).setParam('Authorization', this.session.id);
    }
}
