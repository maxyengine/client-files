const
    Value = nrgsoft.sdk.lang.Value,
    TokenModel = nrgsoft.auth.model.Token;

const timerId = Symbol();

nrgsoft.auth.service.accessControl.RenewToken = class extends Value {

    static get services() {
        return {
            tokenModel: TokenModel
        };
    }

    get assignments() {
        return [
            'startRenewTimer',
            'stopRenewTimer'
        ];
    }

    get properties() {
        return {
            ttl: 'owner.ttl',
            authorize: 'owner.authorize'
        };
    }

    startRenewTimer(token) {
        this.stopRenewTimer();

        if (!token.expires) {
            return;
        }

        const
            reduce = token.ttl - token.ttl / (100/this.ttl),
            timeout = token.expires.getTime() - Date.now() - reduce * 1000;

        //this.tmpDebug(token);

        this[timerId] = setTimeout(() => {
            this.tokenModel.renew()
                .then(token => {
                    console.clear();
                    this.authorize(token);
                });

        }, timeout);
    }

    stopRenewTimer() {
        if (this[timerId]) {
            clearTimeout(this[timerId]);
        }
    }

    tmpDebug(token) {
        console.log(Math.round((token.expires.getTime() - Date.now())/1000));

        this.intervalId && clearInterval(this.intervalId);

        this.intervalId = setInterval(() => {
            token.expires && console.log(Math.round(token.expires.getTime()/1000) - Math.round(Date.now()/1000));
        }, 1000);
    }
}
