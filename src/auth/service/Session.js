const
    Component = nrgsoft.sdk.rx.Component,
    Cookie = nrgsoft.sdk.web.utility.Cookie,
    Type = nrgsoft.sdk.lang.utility.Type;

const
    id = Symbol(),
    expires = Symbol(),
    data = Symbol(),
    options = Symbol(),
    store = Symbol(),
    restore = Symbol();

nrgsoft.auth.service.Session = class extends Component {

    get defaults() {
        return {
            name: 'app',
            path: '/'
        };
    }

    get id() {
        return this[id];
    }

    get isActive() {
        return !!this.id;
    }

    get data() {
        return this[data];
    }

    initialize() {
        this[restore]();
    }

    start(params = {}) {
        this[id] = params.id || null;
        this[expires] = params.expires || null;

        this[store]();
    }

    remove() {
        Cookie.remove(this.name, this[options]());
        this[restore]();
    }

    read(key, byDefault = null) {
        return this[data][key] || byDefault;
    }

    write(key, value) {
        this[data][key] = value;
        this[store]();

        return this;
    }

    [store]() {
        Cookie.set(this.name, JSON.stringify({
            id: this[id],
            expires: this[expires],
            data: this[data]
        }), this[options]());
    }

    [restore]() {
        const json = JSON.parse(
            Cookie.get(this.name, '{"id":null,"expires":null,"data":{}}')
        );

        this[id] = json.id;
        this[expires] = json.expires && new Date(json.expires);
        this[data] = json.data;
    }


    [options]() {
        return this[expires] ?
            {path: this.path, expires: this[expires]} :
            {path: this.path};
    }
}
