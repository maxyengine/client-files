const
    Component = nrgsoft.sdk.rx.Component,
    Obj = nrgsoft.sdk.lang.utility.Object,
    UserModel = nrgsoft.auth.model.User,
    RenewToken = nrgsoft.auth.service.accessControl.RenewToken;

const
    accessMap = Symbol(),
    token = Symbol();

nrgsoft.auth.service.AccessControl = class extends Component {

    static get services() {
        return {
            client: 'client',
            router: 'router',
            session: 'session',
            userModel: UserModel
        };
    }

    get defaults() {
        return {
            ttl: 50
        };
    }

    get traits() {
        return [
            RenewToken
        ];
    }

    get token() {
        return this[token];
    }

    set accessMap(accessMap) {
        for (const [appHref, apiHref] of Obj.toMap(accessMap)) {
            this.setItem(appHref, apiHref);
        }
    }

    _constructor(...args) {
        this[accessMap] = new Map();

        super._constructor(...args);
    }

    setItem(appHref, apiHref) {
        const
            appUrl = this.router.createUrl(appHref),
            apiUrl = this.client.createUrl(apiHref);

        this[accessMap].set(appUrl, apiUrl);
    }

    authorize(accessToken) {
        this[token] = accessToken;
        this.session.start(accessToken);
        this.startRenewTimer(accessToken);
    }

    unauthorized() {
        this[token] = null;
        this.session.remove();
        this.stopRenewTimer();
    }

    isAllowed(url) {
        for (const [appUrl, apiUrl] of this[accessMap]) {
            if (url.contains(appUrl)) {
                return this[token] ? this[token].user.isAllowed(apiUrl) : false;
            }
        }

        return true;
    }

    loadToken() {
        if (this[token] || !this.session.id) {
            return new Promise(resolve => {
                resolve(this[token]);
            });
        }

        return this.userModel.login()
            .then(token => {
                this.authorize(token);

                return token;
            })
            .catch(e => {
                return null;
            });
    }
}
