const
    Action = nrgsoft.textEditor.action.Action,
    Type = nrgsoft.sdk.lang.utility.Type;

nrgsoft.textEditor.action.Print = class extends Action {

    execute(event) {
        if (!Type.isSet(event.text)) {
            return this.goTo('/text-editor');
        }

        const
            text = event.text,
            iframe = document.createElement('iframe');

        iframe.style.display = 'none';
        iframe.setAttribute('src', 'about:blank');
        document.body.appendChild(iframe);

        const pre = iframe.contentWindow.document.body.appendChild(
            iframe.contentWindow.document.createElement('pre')
        );
        pre.innerText = text;

        iframe.contentWindow.print();

        iframe.contentWindow.document.close();
        iframe.contentWindow.close();

        document.body.removeChild(iframe);
    }
}
