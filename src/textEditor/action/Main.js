const
    Action = nrgsoft.textEditor.action.Action,
    View = nrgsoft.textEditor.view.Main,
    Obj = nrgsoft.sdk.lang.utility.Object;

nrgsoft.textEditor.action.Main = class extends Action {

    get defaults() {
        return {
            defaultParams: {
                path: '/'
            }
        };
    }

    initialize() {
        this.view = this.createView(View);
    }

    execute(event) {
        this.params = {...this.defaultParams, ...event.url.params};
        this.layout.content = this.view;
        this.view.text = '';

        if (this.params.fileName) {
            return this.fileModel.read({path: this.params.path + '/' + this.params.fileName})
                .then(file => {
                    this.view.text = file.contents;
                });
        }
    }

    onSave() {
        if (this.params.fileName) {
            this.fileModel.update({
                path: this.params.path + '/' + this.params.fileName,
                contents: this.view.text
            });
        } else {
            this.executeAction({
                url: this.createUrl('/text-editor/save', this.params),
                contents: this.view.text
            });
        }
    }

    onPrint() {
        this.executeAction({
            url: this.createUrl('/text-editor/print', this.params),
            text: this.view.text
        });
    }

    onClose() {
        this.goTo('/', Obj.filterUndefined({
            path: this.params.path,
            fileName: this.params.fileName
        }));
    }
}
