const
    Action = nrgsoft.textEditor.action.Action,
    View = nrgsoft.textEditor.view.Save,
    ModalLayout = nrgsoft.widget.layout.Modal,
    Type = nrgsoft.sdk.lang.utility.Type;

nrgsoft.textEditor.action.Save = class extends Action {

    initialize() {
        this.view = this.createView(View);
        this.form = this.view.form;
    }

    get layout() {
        return this.layouts.getLayout(ModalLayout);
    }

    execute(event) {
        if (!Type.isSet(event.contents)) {
            return this.goTo('/text-editor');
        }

        this.contents = event.contents;
        this.params = event.url.params;

        this.layout.content = this.view;
        this.form.reset().focus();
    }

    onClose(event) {
        event.preventDefault();

        this.goTo('/text-editor', this.params);
    }

    onSubmit(event) {
        event.preventDefault();

        if (!this.form.hasErrors) {
            const
                input = this.form.serialize(),
                path = this.params.path + '/' + input.path;

            this.fileModel.create({path: path, contents: this.contents})
                .then(entity => {
                    this.goTo('/text-editor', {
                        path: entity.path.parent.value,
                        fileName: entity.path.fileName.value
                    });
                })
                .catch(error => {

                    console.log(error);

                    this.form.errorMessages = error.messages;
                });
        }
    }
}
