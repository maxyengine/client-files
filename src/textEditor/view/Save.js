const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkGuiTriggerWidgetActivity = nrgsoft.sdk.gui.trigger.WidgetActivity,
    NrgsoftSdkGuiTriggerHotkeys = nrgsoft.sdk.gui.trigger.Hotkeys,
    NrgsoftTextEditorFormSave = nrgsoft.textEditor.form.Save,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.textEditor.view.Save = class extends nrgsoft.sdk.gui.Widget {
        
    get hotkeys() {
        return {
        close: `Esc`
     };
    }
            
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.sdk.gui.trigger.WidgetActivity,
			nrgsoft.sdk.gui.trigger.Hotkeys
        ];
    }
                
    get form() {
                
        if (!this._form) {
            const
                properties = {"className":"body"},
                services = {
                    t: this.it.t
                };
            
            this._form = this.injector ?
                this.injector.createObject(nrgsoft.textEditor.form.Save, properties, services) :
                new nrgsoft.textEditor.form.Save(properties, services);
        }

        return this._form;
    
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('form');
        this._e1 = document.createElement('div');
        this._e1.setAttribute('class', 'header');
        this._element.appendChild(this._e1);
        this._e2 = document.createElement('div');
        this._e2.setAttribute('class', 'title');
        this._e1.appendChild(this._e2);
        this._e3 = document.createTextNode('save file');
        this._e2.appendChild(this._e3);
        this._e4 = document.createElement('div');
        this._e1.appendChild(this._e4);
        this._e5 = document.createElement('a');
        this._e5.setAttribute('href', '#');
        this._e5.setAttribute('class', 'control close');
        this._e4.appendChild(this._e5);
        this._e6 = this.form.element;
        this._element.appendChild(this._e6);
        this._e7 = document.createElement('div');
        this._e7.setAttribute('class', 'footer');
        this._element.appendChild(this._e7);
        this._e8 = document.createElement('button');
        this._e8.setAttribute('class', 'button');
        this._e8.setAttribute('type', 'submit');
        this._e7.appendChild(this._e8);
        this._e9 = document.createTextNode('save');
        this._e8.appendChild(this._e9);
            
    this._element.classList.add('nrgsoft-textEditor-view-Save');
    
        super._constructor(properties);
        this.it.mapEvent(this._element, 'submit', 'submit');
        this.it.mapEvent(this._e5, 'click', 'close');
    
    }
}
