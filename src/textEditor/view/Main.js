const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkGuiTriggerHotkeys = nrgsoft.sdk.gui.trigger.Hotkeys,
    NrgsoftWidgetOverview = nrgsoft.widget.Overview,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.textEditor.view.Main = class extends nrgsoft.sdk.gui.Widget {
        
    get hotkeys() {
        return {
        save: `Alt+s`,
        print: `Alt+p`
     };
    }
            
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.sdk.gui.trigger.Hotkeys
        ];
    }
                
    get overview() {
                
        if (!this._overview) {
            const
                properties = {},
                services = {
                    t: this.it.t
                };
            
            this._overview = this.injector ?
                this.injector.createObject(nrgsoft.widget.Overview, properties, services) :
                new nrgsoft.widget.Overview(properties, services);
        }

        return this._overview;
    
    }
                
    get toolbar() {
                return this._e2;
    }
                
    get textArea() {
                return this._e7;
    }
                
    get text() {
                return this._e7.value;
    }
    
    set text(value) {
                this._e7.value = value;
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._element.setAttribute('class', 'nrgsoft-text-editor__index-main');
        this._e1 = this.overview.element;
        this._element.appendChild(this._e1);
        this._e2 = document.createElement('div');
        this._e2.setAttribute('class', 'toolbar');
        this._element.appendChild(this._e2);
        this._e3 = document.createElement('button');
        this._e3.setAttribute('class', 'text-editor-save');
        this._e2.appendChild(this._e3);
        this._e4 = document.createTextNode('save');
        this._e3.appendChild(this._e4);
        this._e5 = document.createElement('button');
        this._e5.setAttribute('class', 'text-editor-print');
        this._e2.appendChild(this._e5);
        this._e6 = document.createTextNode('print');
        this._e5.appendChild(this._e6);
        this._e7 = document.createElement('textarea');
        this._e7.setAttribute('placeholder', 'Enter text here');
        this._element.appendChild(this._e7);
            
    this._element.classList.add('nrgsoft-textEditor-view-Main');
    
        super._constructor(properties);
        this.it.mapEvent(this.overview, 'close', 'close');
        this.it.mapEvent(this._e3, 'click', 'save');
        this.it.mapEvent(this._e5, 'click', 'print');
    
    }
}
