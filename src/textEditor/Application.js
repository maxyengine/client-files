const
    TypeObject = nrgsoft.sdk.lang.Object,
    Controller = nrgsoft.sdk.web.Controller,
    Router = nrgsoft.sdk.web.Router,
    Client = nrgsoft.auth.service.JsonClient,
    Session = nrgsoft.auth.service.Session,
    Url = nrgsoft.sdk.web.Url,
    Translator = nrgsoft.sdk.i18n.Translator,
    Layouts = nrgsoft.sdk.gui.Layouts,
    Injector = nrgsoft.sdk.di.Injector,
    Clipboard = nrgsoft.fileManager.service.Clipboard,
    MainAction = nrgsoft.textEditor.action.Main,
    SaveAction = nrgsoft.textEditor.action.Save,
    PrintAction = nrgsoft.textEditor.action.Print;

nrgsoft.textEditor.Application = class extends TypeObject {

    get defaults() {
        return {
            wrapper: document.body,
            rootUrl: '/',
            i18n: {},
            locale: 'ru'
        };
    }

    get services() {
        return {
            controller: [Controller, {actions: this.actions}],
            router: [Router, {rootUrl: new Url(this.rootUrl)}],
            client: [Client, {apiUrl: new Url(this.apiUrl)}],
            session: [Session, {name: 'nrgsoft.fileManager.Application'}],
            translator: [Translator, {i18n: this.i18n, locale: this.locale}],
            layouts: [Layouts, {wrapper: this.wrapper}],
            clipboard: Clipboard
        };
    }

    get actions() {
        return {
            '/text-editor': MainAction,
            '/text-editor/save': SaveAction,
            '/text-editor/print': PrintAction
        };
    }

    run() {
        new Injector()
            .loadServices(this.services)
            .getService('controller')
            .goTo(window.location.href);
    }
}
