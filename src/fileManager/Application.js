const
    TypeObject = nrgsoft.sdk.lang.Object,
    Controller = nrgsoft.auth.service.Controller,
    AccessControl = nrgsoft.auth.service.AccessControl,
    Router = nrgsoft.sdk.web.Router,
    Url = nrgsoft.sdk.web.Url,
    Translator = nrgsoft.sdk.i18n.Translator,
    Client = nrgsoft.auth.service.JsonClient,
    Layouts = nrgsoft.sdk.gui.Layouts,
    Injector = nrgsoft.sdk.di.Injector,
    Session = nrgsoft.auth.service.Session,
    Clipboard = nrgsoft.fileManager.service.Clipboard,
    DirectoryDoubleRead = nrgsoft.fileManager.action.directory.DoubleRead,
    DirectoryCreate = nrgsoft.fileManager.action.directory.Create,
    HyperlinkCreate = nrgsoft.fileManager.action.hyperlink.Create,
    FileCopy = nrgsoft.fileManager.action.file.Copy,
    FileDelete = nrgsoft.fileManager.action.file.Delete,
    FileRename = nrgsoft.fileManager.action.file.Rename,
    FileZip = nrgsoft.fileManager.action.file.Zip,
    FileUnzip = nrgsoft.fileManager.action.file.Unzip,
    Download = nrgsoft.fileManager.action.file.Download,
    StorageList = nrgsoft.fileManager.action.storage.List,
    DeleteStorage = nrgsoft.fileManager.action.storage.Delete,
    SaveLocalStorage = nrgsoft.fileManager.action.storage.local.Save,
    SaveFtpStorage = nrgsoft.fileManager.action.storage.ftp.Save,
    SaveSftpStorage = nrgsoft.fileManager.action.storage.sftp.Save,
    TextEditorAction = nrgsoft.textEditor.action.Main,
    FileUploaderAction = nrgsoft.fileUploader.action.Main,
    SaveAction = nrgsoft.textEditor.action.Save,
    PrintAction = nrgsoft.textEditor.action.Print,
    UserLogin = nrgsoft.auth.action.user.Login,
    UserLogout = nrgsoft.auth.action.user.Logout;

nrgsoft.fileManager.Application = class extends TypeObject {

    get defaults() {
        return {
            wrapper: document.body,
            minHeight: 500,
            rootUrl: '/',
            apiUrl: null,
            i18n: {},
            locale: 'ru'
        };
    }

    get services() {
        return {
            accessControl: [AccessControl, {accessMap: this.accessMap}],
            controller: [Controller, {actions: this.actions}],
            router: [Router, {rootUrl: new Url(this.rootUrl)}],
            client: [Client, {apiUrl: new Url(this.apiUrl)}],
            translator: [Translator, {i18n: this.i18n, locale: this.locale}],
            session: [Session, {name: 'nrgsoft.fileManager.Application'}],
            layouts: [Layouts, {wrapper: this.wrapper, minHeight: this.minHeight}],
            clipboard: Clipboard
        };
    }

    get actions() {
        return {
            '/': DirectoryDoubleRead,

            '/file-manager': DirectoryDoubleRead,

            '/file-manager/directory/create': DirectoryCreate,

            '/file-manager/file/copy': FileCopy,
            '/file-manager/file/delete': FileDelete,
            '/file-manager/file/rename': FileRename,
            '/file-manager/file/zip': FileZip,
            '/file-manager/file/unzip': FileUnzip,
            '/file-manager/file/download': Download,

            '/file-manager/hyperlink/create': HyperlinkCreate,

            '/file-manager/storage/list': StorageList,
            '/file-manager/storage/delete': DeleteStorage,
            '/file-manager/storage/local/save': SaveLocalStorage,
            '/file-manager/storage/ftp/save': SaveFtpStorage,
            '/file-manager/storage/sftp/save': SaveSftpStorage,

            '/text-editor': TextEditorAction,
            '/text-editor/save': SaveAction,
            '/text-editor/print': PrintAction,

            '/file-uploader': FileUploaderAction,

            '/auth/user/login': UserLogin,
            '/auth/user/logout': UserLogout
        };
    }

    get accessMap() {
        return {
            '/': '/file-manager/directory/read',

            '/file-manager': '/file-manager/directory/read',
            '/file-manager/directory/create': '/file-manager/directory/create',
            '/file-manager/file/read': '/file-manager/file/read',
            '/file-manager/file/create': '/file-manager/file/create',
            '/file-manager/file/update': '/file-manager/file/update',

            '/auth/role/details': '/auth/role/details',
            '/auth/role/create': '/auth/role/create',
            '/auth/user/list': '/auth/user/list',
            '/auth/user/details': '/auth/user/details',
            '/auth/user/create': '/auth/user/create'
        };
    }

    run() {
        new Injector()
            .loadServices(this.services)
            .getService('controller')
            .goTo(window.location.href);
    }
}
