const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkFormForm = nrgsoft.sdk.form.Form,
    NrgsoftSdkGuiWidgetAbility = nrgsoft.sdk.gui.WidgetAbility,
    NrgsoftSdkFormObserverOnAppendElement = nrgsoft.sdk.form.observer.OnAppendElement,
    NrgsoftFileManagerFormFileElementPath = nrgsoft.fileManager.form.file.element.Path,
    NrgsoftFileManagerFormHyperlinkElementUrl = nrgsoft.fileManager.form.hyperlink.element.Url;

nrgsoft.fileManager.form.hyperlink.Create = class extends nrgsoft.sdk.form.Form {
        
    static get services() {
        return {
            ...nrgsoft.sdk.form.Form.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.sdk.gui.WidgetAbility,
			nrgsoft.sdk.form.observer.OnAppendElement
        ];
    }
                
    get _e1_() {
                
        if (!this.__e1_) {
            const
                properties = {"label":"name","placeholder":"NewHyperlink"},
                services = {
                    t: this.it.t
                };
            
            this.__e1_ = this.injector ?
                this.injector.createObject(nrgsoft.fileManager.form.file.element.Path, properties, services) :
                new nrgsoft.fileManager.form.file.element.Path(properties, services);
        }

        return this.__e1_;
    
    }
                
    get _e2_() {
                
        if (!this.__e2_) {
            const
                properties = {"label":"url","placeholder":"https:\/\/somedomain.com\/music\/my-favorite"},
                services = {
                    t: this.it.t
                };
            
            this.__e2_ = this.injector ?
                this.injector.createObject(nrgsoft.fileManager.form.hyperlink.element.Url, properties, services) :
                new nrgsoft.fileManager.form.hyperlink.element.Url(properties, services);
        }

        return this.__e2_;
    
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = this._e1_.element;
        this._element.appendChild(this._e1);
        this._e2 = this._e2_.element;
        this._element.appendChild(this._e2);
            
    this._element.classList.add('nrgsoft-fileManager-form-hyperlink-Create');
    
        super._constructor(properties);
        this.it.trigger('appendElement', {widget: this._e1_});
        this.it.trigger('appendElement', {widget: this._e2_});
    
    }
}
