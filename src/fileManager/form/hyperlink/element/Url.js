const
    TextElement = nrgsoft.widget.form.TextElement,
    Trim = nrgsoft.sdk.form.filter.Trim,
    Required = nrgsoft.sdk.form.validator.Required,
    TypeString = nrgsoft.sdk.form.validator.TypeString,
    Length = nrgsoft.sdk.form.validator.Length,
    LinkUrl = nrgsoft.sdk.form.validator.Url;

nrgsoft.fileManager.form.hyperlink.element.Url = class extends TextElement {

    initialize() {
        this
            .set({
                name: this.name || 'url',
                isRequired: true
            })
            .addFilter(new Trim())
            .addValidator(new Required())
            .addValidator(new TypeString())
            .addValidator(new Length({
                min: 1,
                max: 5000
            }))
            .addValidator(new LinkUrl())
        ;
    }
}
