const
    Form = nrgsoft.sdk.form.Form,
    Template = nrgsoft.fileManager.form.file.UnzipTpl,
    OnAppendElement = nrgsoft.sdk.form.observer.OnAppendElement,
    Element = nrgsoft.sdk.gui.utility.Element;

nrgsoft.fileManager.form.file.Unzip = class extends Form {

    get traits() {
        return [
            ...super.traits || [],
            OnAppendElement,
            Template
        ];
    }

    onToggleTarget(event, form, checkbox) {
        if (checkbox.value) {
            this.deleteElement(this.remove(this.pathElement));
        } else {
            this.addElement(this.appendBefore(this.pathElement, checkbox));
        }
    }
}
