const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkFormForm = nrgsoft.sdk.form.Form,
    NrgsoftSdkGuiWidgetAbility = nrgsoft.sdk.gui.WidgetAbility,
    NrgsoftSdkFormObserverOnAppendElement = nrgsoft.sdk.form.observer.OnAppendElement,
    NrgsoftFileManagerFormFileElementPath = nrgsoft.fileManager.form.file.element.Path;

nrgsoft.fileManager.form.file.Rename = class extends nrgsoft.sdk.form.Form {
        
    static get services() {
        return {
            ...nrgsoft.sdk.form.Form.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.sdk.gui.WidgetAbility,
			nrgsoft.sdk.form.observer.OnAppendElement
        ];
    }
                
    get _e1_() {
                
        if (!this.__e1_) {
            const
                properties = {"label":"file name","placeholder":"","name":"newPath"},
                services = {
                    t: this.it.t
                };
            
            this.__e1_ = this.injector ?
                this.injector.createObject(nrgsoft.fileManager.form.file.element.Path, properties, services) :
                new nrgsoft.fileManager.form.file.element.Path(properties, services);
        }

        return this.__e1_;
    
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = this._e1_.element;
        this._element.appendChild(this._e1);
            
    this._element.classList.add('nrgsoft-fileManager-form-file-Rename');
    
        super._constructor(properties);
        this.it.trigger('appendElement', {widget: this._e1_});
    
    }
}
