const
    TextElement = nrgsoft.widget.form.TextElement,
    Integer = nrgsoft.sdk.form.filter.Integer,
    TypeInteger = nrgsoft.sdk.form.validator.TypeInteger;

nrgsoft.fileManager.form.storage.ftp.element.Port = class extends TextElement {

    initialize() {
        this
            .set({
                name: 'port'
            })
            .addFilter(new Integer())
            .addValidator(new TypeInteger())
        ;
    }
}
