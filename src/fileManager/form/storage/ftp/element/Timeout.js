const
    TextElement = nrgsoft.widget.form.TextElement,
    Trim = nrgsoft.sdk.form.filter.Trim,
    Integer = nrgsoft.sdk.form.filter.Integer,
    TypeInteger = nrgsoft.sdk.form.validator.TypeInteger;

nrgsoft.fileManager.form.storage.ftp.element.Timeout = class extends TextElement {

    initialize() {
        this
            .set({
                name: 'timeout'
            })
            .addFilter(new Trim())
            .addFilter(new Integer())
            .addValidator(new TypeInteger())
        ;
    }
}
