const
    TextElement = nrgsoft.widget.form.TextElement,
    Trim = nrgsoft.sdk.form.filter.Trim,
    TypeString = nrgsoft.sdk.form.validator.TypeString,
    Length = nrgsoft.sdk.form.validator.Length;

nrgsoft.fileManager.form.storage.ftp.element.Root = class extends TextElement {

    initialize() {
        this
            .set({
                name: 'root'
            })
            .addFilter(new Trim())
            .addValidator(new TypeString())
            .addValidator(new Length({
                min: 1,
                max: 255
            }))
        ;
    }
}
