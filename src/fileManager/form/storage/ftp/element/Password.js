const
    TextElement = nrgsoft.widget.form.TextElement,
    Trim = nrgsoft.sdk.form.filter.Trim,
    Required = nrgsoft.sdk.form.validator.Required,
    TypeString = nrgsoft.sdk.form.validator.TypeString,
    Length = nrgsoft.sdk.form.validator.Length;

nrgsoft.fileManager.form.storage.ftp.element.Password = class extends TextElement {

    initialize() {
        this
            .set({
                name: 'password'
            })
            .addFilter(new Trim())
            .addValidator(new Required())
            .addValidator(new TypeString())
            .addValidator(new Length({
                max: 50
            }))
        ;
    }
}
