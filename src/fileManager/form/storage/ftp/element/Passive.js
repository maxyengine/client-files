const
    CheckboxElement = nrgsoft.widget.form.CheckboxElement;

nrgsoft.fileManager.form.storage.ftp.element.Passive = class extends CheckboxElement {

    initialize() {
        this.set({
            name: 'passive'
        });
    }
}
