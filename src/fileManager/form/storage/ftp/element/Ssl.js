const
    CheckboxElement = nrgsoft.widget.form.CheckboxElement;

nrgsoft.fileManager.form.storage.ftp.element.Ssl = class extends CheckboxElement {

    initialize() {
        this.set({
            name: 'ssl'
        });
    }
}
