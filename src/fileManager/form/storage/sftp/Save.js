const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkFormForm = nrgsoft.sdk.form.Form,
    NrgsoftSdkGuiWidgetAbility = nrgsoft.sdk.gui.WidgetAbility,
    NrgsoftSdkFormObserverOnAppendElement = nrgsoft.sdk.form.observer.OnAppendElement,
    NrgsoftFileManagerFormStorageElementName = nrgsoft.fileManager.form.storage.element.Name,
    NrgsoftFileManagerFormStorageElementDescription = nrgsoft.fileManager.form.storage.element.Description,
    NrgsoftFileManagerFormStorageFtpElementHost = nrgsoft.fileManager.form.storage.ftp.element.Host,
    NrgsoftFileManagerFormStorageFtpElementUsername = nrgsoft.fileManager.form.storage.ftp.element.Username,
    NrgsoftFileManagerFormStorageFtpElementPassword = nrgsoft.fileManager.form.storage.ftp.element.Password,
    NrgsoftFileManagerFormStorageSftpElementPrivateKey = nrgsoft.fileManager.form.storage.sftp.element.PrivateKey,
    NrgsoftFileManagerFormStorageFtpElementPort = nrgsoft.fileManager.form.storage.ftp.element.Port,
    NrgsoftFileManagerFormStorageFtpElementRoot = nrgsoft.fileManager.form.storage.ftp.element.Root,
    NrgsoftFileManagerFormStorageFtpElementTimeout = nrgsoft.fileManager.form.storage.ftp.element.Timeout;

nrgsoft.fileManager.form.storage.sftp.Save = class extends nrgsoft.sdk.form.Form {
        
    static get services() {
        return {
            ...nrgsoft.sdk.form.Form.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.sdk.gui.WidgetAbility,
			nrgsoft.sdk.form.observer.OnAppendElement
        ];
    }
                
    get _e1_() {
                
        if (!this.__e1_) {
            const
                properties = {"label":"name","placeholder":"new storage"},
                services = {
                    t: this.it.t
                };
            
            this.__e1_ = this.injector ?
                this.injector.createObject(nrgsoft.fileManager.form.storage.element.Name, properties, services) :
                new nrgsoft.fileManager.form.storage.element.Name(properties, services);
        }

        return this.__e1_;
    
    }
                
    get _e2_() {
                
        if (!this.__e2_) {
            const
                properties = {"label":"description","placeholder":"short text about storage"},
                services = {
                    t: this.it.t
                };
            
            this.__e2_ = this.injector ?
                this.injector.createObject(nrgsoft.fileManager.form.storage.element.Description, properties, services) :
                new nrgsoft.fileManager.form.storage.element.Description(properties, services);
        }

        return this.__e2_;
    
    }
                
    get _e5_() {
                
        if (!this.__e5_) {
            const
                properties = {"label":"host","placeholder":"ftp.server.com"},
                services = {
                    t: this.it.t
                };
            
            this.__e5_ = this.injector ?
                this.injector.createObject(nrgsoft.fileManager.form.storage.ftp.element.Host, properties, services) :
                new nrgsoft.fileManager.form.storage.ftp.element.Host(properties, services);
        }

        return this.__e5_;
    
    }
                
    get _e6_() {
                
        if (!this.__e6_) {
            const
                properties = {"label":"username","placeholder":"username"},
                services = {
                    t: this.it.t
                };
            
            this.__e6_ = this.injector ?
                this.injector.createObject(nrgsoft.fileManager.form.storage.ftp.element.Username, properties, services) :
                new nrgsoft.fileManager.form.storage.ftp.element.Username(properties, services);
        }

        return this.__e6_;
    
    }
                
    get _e7_() {
                
        if (!this.__e7_) {
            const
                properties = {"label":"password","placeholder":"password"},
                services = {
                    t: this.it.t
                };
            
            this.__e7_ = this.injector ?
                this.injector.createObject(nrgsoft.fileManager.form.storage.ftp.element.Password, properties, services) :
                new nrgsoft.fileManager.form.storage.ftp.element.Password(properties, services);
        }

        return this.__e7_;
    
    }
                
    get _e8_() {
                
        if (!this.__e8_) {
            const
                properties = {"label":"private key","placeholder":"private key"},
                services = {
                    t: this.it.t
                };
            
            this.__e8_ = this.injector ?
                this.injector.createObject(nrgsoft.fileManager.form.storage.sftp.element.PrivateKey, properties, services) :
                new nrgsoft.fileManager.form.storage.sftp.element.PrivateKey(properties, services);
        }

        return this.__e8_;
    
    }
                
    get _e10_() {
                
        if (!this.__e10_) {
            const
                properties = {"label":"port","placeholder":"port number"},
                services = {
                    t: this.it.t
                };
            
            this.__e10_ = this.injector ?
                this.injector.createObject(nrgsoft.fileManager.form.storage.ftp.element.Port, properties, services) :
                new nrgsoft.fileManager.form.storage.ftp.element.Port(properties, services);
        }

        return this.__e10_;
    
    }
                
    get _e11_() {
                
        if (!this.__e11_) {
            const
                properties = {"label":"root","placeholder":"root path"},
                services = {
                    t: this.it.t
                };
            
            this.__e11_ = this.injector ?
                this.injector.createObject(nrgsoft.fileManager.form.storage.ftp.element.Root, properties, services) :
                new nrgsoft.fileManager.form.storage.ftp.element.Root(properties, services);
        }

        return this.__e11_;
    
    }
                
    get _e12_() {
                
        if (!this.__e12_) {
            const
                properties = {"label":"timeout","placeholder":"timeout"},
                services = {
                    t: this.it.t
                };
            
            this.__e12_ = this.injector ?
                this.injector.createObject(nrgsoft.fileManager.form.storage.ftp.element.Timeout, properties, services) :
                new nrgsoft.fileManager.form.storage.ftp.element.Timeout(properties, services);
        }

        return this.__e12_;
    
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = this._e1_.element;
        this._element.appendChild(this._e1);
        this._e2 = this._e2_.element;
        this._element.appendChild(this._e2);
        this._e3 = document.createElement('div');
        this._e3.setAttribute('style', 'display: flex');
        this._element.appendChild(this._e3);
        this._e4 = document.createElement('div');
        this._e3.appendChild(this._e4);
        this._e5 = this._e5_.element;
        this._e4.appendChild(this._e5);
        this._e6 = this._e6_.element;
        this._e4.appendChild(this._e6);
        this._e7 = this._e7_.element;
        this._e4.appendChild(this._e7);
        this._e8 = this._e8_.element;
        this._e4.appendChild(this._e8);
        this._e9 = document.createElement('div');
        this._e3.appendChild(this._e9);
        this._e10 = this._e10_.element;
        this._e9.appendChild(this._e10);
        this._e11 = this._e11_.element;
        this._e9.appendChild(this._e11);
        this._e12 = this._e12_.element;
        this._e9.appendChild(this._e12);
            
    this._element.classList.add('nrgsoft-fileManager-form-storage-sftp-Save');
    
        super._constructor(properties);
        this.it.trigger('appendElement', {widget: this._e1_});
        this.it.trigger('appendElement', {widget: this._e2_});
        this.it.trigger('appendElement', {widget: this._e5_});
        this.it.trigger('appendElement', {widget: this._e6_});
        this.it.trigger('appendElement', {widget: this._e7_});
        this.it.trigger('appendElement', {widget: this._e8_});
        this.it.trigger('appendElement', {widget: this._e10_});
        this.it.trigger('appendElement', {widget: this._e11_});
        this.it.trigger('appendElement', {widget: this._e12_});
    
    }
}
