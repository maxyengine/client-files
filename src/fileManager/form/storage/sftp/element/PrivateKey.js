const
    TextElement = nrgsoft.widget.form.TextElement,
    Trim = nrgsoft.sdk.form.filter.Trim,
    Required = nrgsoft.sdk.form.validator.Required,
    TypeString = nrgsoft.sdk.form.validator.TypeString,
    Length = nrgsoft.sdk.form.validator.Length;

nrgsoft.fileManager.form.storage.sftp.element.PrivateKey = class extends TextElement {

    initialize() {
        this
            .set({
                name: 'privateKey'
            })
            .addFilter(new Trim())
            .addValidator(new TypeString())
            .addValidator(new Length({
                max: 4096
            }))
        ;
    }
}
