const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkFormForm = nrgsoft.sdk.form.Form,
    NrgsoftSdkGuiWidgetAbility = nrgsoft.sdk.gui.WidgetAbility,
    NrgsoftSdkFormObserverOnAppendElement = nrgsoft.sdk.form.observer.OnAppendElement,
    NrgsoftFileManagerFormStorageElementName = nrgsoft.fileManager.form.storage.element.Name,
    NrgsoftFileManagerFormStorageElementDescription = nrgsoft.fileManager.form.storage.element.Description,
    NrgsoftFileManagerFormStorageLocalElementRoot = nrgsoft.fileManager.form.storage.local.element.Root;

nrgsoft.fileManager.form.storage.local.Save = class extends nrgsoft.sdk.form.Form {
        
    static get services() {
        return {
            ...nrgsoft.sdk.form.Form.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.sdk.gui.WidgetAbility,
			nrgsoft.sdk.form.observer.OnAppendElement
        ];
    }
                
    get _e1_() {
                
        if (!this.__e1_) {
            const
                properties = {"label":"name","placeholder":"new storage"},
                services = {
                    t: this.it.t
                };
            
            this.__e1_ = this.injector ?
                this.injector.createObject(nrgsoft.fileManager.form.storage.element.Name, properties, services) :
                new nrgsoft.fileManager.form.storage.element.Name(properties, services);
        }

        return this.__e1_;
    
    }
                
    get _e2_() {
                
        if (!this.__e2_) {
            const
                properties = {"label":"description","placeholder":"short text about storage"},
                services = {
                    t: this.it.t
                };
            
            this.__e2_ = this.injector ?
                this.injector.createObject(nrgsoft.fileManager.form.storage.element.Description, properties, services) :
                new nrgsoft.fileManager.form.storage.element.Description(properties, services);
        }

        return this.__e2_;
    
    }
                
    get _e3_() {
                
        if (!this.__e3_) {
            const
                properties = {"label":"root","placeholder":"path to root folder"},
                services = {
                    t: this.it.t
                };
            
            this.__e3_ = this.injector ?
                this.injector.createObject(nrgsoft.fileManager.form.storage.local.element.Root, properties, services) :
                new nrgsoft.fileManager.form.storage.local.element.Root(properties, services);
        }

        return this.__e3_;
    
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = this._e1_.element;
        this._element.appendChild(this._e1);
        this._e2 = this._e2_.element;
        this._element.appendChild(this._e2);
        this._e3 = this._e3_.element;
        this._element.appendChild(this._e3);
            
    this._element.classList.add('nrgsoft-fileManager-form-storage-local-Save');
    
        super._constructor(properties);
        this.it.trigger('appendElement', {widget: this._e1_});
        this.it.trigger('appendElement', {widget: this._e2_});
        this.it.trigger('appendElement', {widget: this._e3_});
    
    }
}
