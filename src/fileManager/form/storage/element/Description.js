const
    TextElement = nrgsoft.widget.form.TextElement,
    Trim = nrgsoft.sdk.form.filter.Trim,
    Required = nrgsoft.sdk.form.validator.Required,
    TypeString = nrgsoft.sdk.form.validator.TypeString,
    Length = nrgsoft.sdk.form.validator.Length;

nrgsoft.fileManager.form.storage.element.Description = class extends TextElement {

    initialize() {
        this
            .set({
                name: 'description'
            })
            .addFilter(new Trim())
            .addValidator(new TypeString())
            .addValidator(new Length({
                min: 1,
                max: 255
            }))
        ;
    }
}
