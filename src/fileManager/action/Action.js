const
  WebAction = nrgsoft.sdk.web.Action,
  Layout = nrgsoft.fileManager.view.layout.Main,
  FileModel = nrgsoft.fileManager.model.File,
  HyperlinkModel = nrgsoft.fileManager.model.Hyperlink,
  DirectoryModel = nrgsoft.fileManager.model.Directory,
  StorageModel = nrgsoft.fileManager.model.Storage

nrgsoft.fileManager.action.Action = class extends WebAction {

  static get services () {
    return {
      ...WebAction.services,
      session: 'session',
      clipboard: 'clipboard',
      fileModel: FileModel,
      directoryModel: DirectoryModel,
      hyperlinkModel: HyperlinkModel,
      storageModel: StorageModel
    }
  }

  get layout () {
    return this.layouts.getLayout(Layout)
  }
}
