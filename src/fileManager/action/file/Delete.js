const
  Action = nrgsoft.fileManager.action.common.Bunch,
  DeleteFiles = nrgsoft.fileManager.useCase.file.Delete

nrgsoft.fileManager.action.file.Delete = class extends Action {

  static get services () {
    return {
      ...Action.services,
      deleteFiles: DeleteFiles
    }
  }

  get defaults () {
    return {
      title: 'delete',
      emptyMessage: 'there are no selected files',
      confirmMessage: 'delete selected files ?'
    }
  }

  async task () {
    await this.deleteFiles.execute(this.view.progressList, [...this.clipboard])

    this.close()
  }
}
