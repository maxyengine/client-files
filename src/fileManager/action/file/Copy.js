const
  Action = nrgsoft.fileManager.action.common.Bunch,
  View = nrgsoft.fileManager.view.file.Copy,
  CopyFiles = nrgsoft.fileManager.useCase.file.Copy

nrgsoft.fileManager.action.file.Copy = class extends Action {

  static get services () {
    return {
      ...Action.services,
      copyFiles: CopyFiles
    }
  }

  get viewClass () {
    return View
  }

  get defaults () {
    return {
      title: 'copy',
      emptyMessage: 'there are no selected files',
      confirmMessage: 'copy/move selected files ?'
    }
  }

  execute (event) {
    this.copyTo = event.url.getParam('copyTo')

    super.execute(event)
  }

  async task (move = false) {
    await this.copyFiles.execute(this.view.progressList, [...this.clipboard], this.copyTo, move, this.view.overwrite)

    this.clipboard.clear()
    this.close({}, {noNeedRefresh: !move})
  }

  onMove () {
    this.task(true)
  }
}
