const
  Action = nrgsoft.fileManager.action.common.Task,
  View = nrgsoft.fileManager.view.file.Unzip,
  Path = nrgsoft.fileManager.value.Path,
  UnzipFiles = nrgsoft.fileManager.useCase.file.Unzip,
  Form = nrgsoft.fileManager.form.file.Unzip

nrgsoft.fileManager.action.file.Unzip = class extends Action {

  static get services () {
    return {
      ...Action.services,
      unzipFiles: UnzipFiles
    }
  }

  get defaults () {
    return {
      title: 'Unzip archive',
      formClass: Form
    }
  }

  get viewClass () {
    return View
  }

  async execute (event) {
    this.path = new Path(event.url.getParam('path'), true)

    return super.execute(event)
  }

  show () {
    this.view.showContent(false)
    super.show()
  }

  async task ({path, toCurrentDirectory, overwrite}) {
    const
      parent = this.path.parent,
      toPath = toCurrentDirectory ? parent : parent.join(path),
      fileName = toCurrentDirectory ? this.path.fileName.value : toPath.fileName.value

    await this.unzipFiles.execute(this.path, toPath, this.view.progressList, overwrite)

    this.close({fileName})
  }
}
