const
  Action = nrgsoft.fileManager.action.common.Task,
  View = nrgsoft.fileManager.view.file.Zip,
  Form = nrgsoft.fileManager.form.file.Zip,
  ZipFiles = nrgsoft.fileManager.useCase.file.Zip,
  Path = nrgsoft.fileManager.value.Path

nrgsoft.fileManager.action.file.Zip = class extends Action {

  static get services () {
    return {
      ...Action.services,
      zipFiles: ZipFiles
    }
  }

  get defaults () {
    return {
      title: 'create zip archive',
      emptyMessage: 'there are no selected files',
      formClass: Form
    }
  }

  get viewClass () {
    return View
  }

  initialize () {
    super.initialize()
    this.view.emptyMessage = this.emptyMessage
  }

  async execute (event) {
    this.currentPath = new Path(event.url.getParam('currentPath'), true)

    return super.execute(event)
  }

  show () {
    this.view.showContent(this.clipboard.isEmpty)
    super.show()
  }

  async task ({path}) {
    const
      fileName = `${path}.zip`,
      zipPath = this.currentPath.join(fileName)

    await this.zipFiles.execute(zipPath, [...this.clipboard], this.view.progressList)

    this.close({fileName})
  }
}
