const
  Action = nrgsoft.fileManager.action.common.Task,
  Form = nrgsoft.fileManager.form.file.Rename,
  Path = nrgsoft.fileManager.value.Path

nrgsoft.fileManager.action.file.Rename = class extends Action {

  get defaults () {
    return {
      title: 'rename file',
      formClass: Form
    }
  }

  async execute (event) {
    this.path = new Path(event.url.getParam('path'), true)
    this.parent = this.path.parent

    return super.execute(event)
  }

  show () {
    super.show()
    this.form.populate({newPath: this.path.fileName.value})
  }

  async task ({newPath: fileName}) {
    const newPath = this.parent.join(fileName)

    await this.fileModel.move({
      path: this.path.value,
      newPath: newPath.value
    })

    this.close({fileName})
  }
}
