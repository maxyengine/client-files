const
  Action = nrgsoft.fileManager.action.common.Task,
  View = nrgsoft.fileManager.view.file.Zip,
  Form = nrgsoft.fileManager.form.file.Zip,
  CreateTempZip = nrgsoft.fileManager.useCase.file.CreateTempZip,
  File = nrgsoft.fileManager.entity.File

nrgsoft.fileManager.action.file.Download = class extends Action {

  static get services () {
    return {
      ...Action.services,
      createTempZip: CreateTempZip
    }
  }

  get defaults () {
    return {
      title: 'download as',
      emptyMessage: 'there are no selected files',
      backHref: '/file-manager',
      formClass: Form
    }
  }

  get viewClass () {
    return View
  }

  initialize () {
    super.initialize()
    this.view.emptyMessage = this.emptyMessage
  }

  show () {
    this.view.showContent(this.clipboard.isEmpty)
    super.show()
    this.form.populate({
      path: 1 === this.clipboard.size ?
        this.clipboard.asArray[0].path.fileName.value :
        new Date().toJSON().slice(0, 19).replace(/[T:\s]/g, '_')
    })
  }

  async task ({path}) {
    if (1 === this.clipboard.size && this.clipboard.asArray[0] instanceof File) {
      this.downloadFile(this.clipboard.asArray[0].path.value, path)
    } else {
      this.downloadZip(path)
    }
  }

  downloadFile (filePath, fileName, deleteTempFile = false) {
    const apiDownloadUrl = this.client.createAuthorizedUrl('/file-manager/file/download')
      .mergeParams({
        path: filePath,
        fileName: fileName,
        deleteTempFile: deleteTempFile
      })

    window.open(apiDownloadUrl)
    this.close({}, {noNeedRefresh: true})
  }

  async downloadZip (fileName) {
    const
      zipName = `${fileName}.zip`,
      tempZipPath = await this.createTempZip.execute(zipName, [...this.clipboard], this.view.progressList)

    this.downloadFile(tempZipPath, zipName, true)
  }
}
