const
  Action = nrgsoft.fileManager.action.common.Modal,
  View = nrgsoft.fileManager.view.common.Task

nrgsoft.fileManager.action.common.Task = class extends Action {

  get form () {
    return this.view.form
  }

  get viewClass () {
    return View
  }

  initialize () {
    super.initialize()

    this.view = this.createView(this.viewClass)
    this.view.form = this.createWidget(this.formClass)
  }

  show () {
    this.form.reset().focus()
  }

  async onConfirm (event) {
    event.preventDefault()

    if (!this.form.hasErrors) {
      try {
        await this.task(this.form.serialize())
      } catch (error) {
        this.form.errorMessages = error.messages
      }
    }
  }
}
