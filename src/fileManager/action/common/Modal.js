const
  Action = nrgsoft.fileManager.action.Action,
  Layout = nrgsoft.widget.layout.Modal

nrgsoft.fileManager.action.common.Modal = class extends Action {

  get layout () {
    return this.layouts.getLayout(Layout)
  }

  initialize () {
    this.layout.on(this)
  }

  async execute ({url, pageWasReloaded, body}) {
    this.backUrl = this.createUrl(decodeURIComponent(url.getParam('backHref', '/')))
    this.reload = body.reload

    if (pageWasReloaded) {
      await this.executeAction({
        url: this.backUrl,
        pageWasReloaded: true
      })
    }

    this.layout.set({
      title: this.title,
      content: this.view
    })

    this.show()
  }

  close (params = {}, body = {}) {
    if (!body.reload) {
      body.reload = this.reload && !body.actionWasCanceled
    }

    this.goTo(this.backUrl, params, body)
  }

  onClose () {
    this.close({}, {actionWasCanceled: true})
  }

  onCancel () {
    this.close({}, {actionWasCanceled: true})
  }
}
