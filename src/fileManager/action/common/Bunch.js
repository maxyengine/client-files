const
  Action = nrgsoft.fileManager.action.common.Modal,
  View = nrgsoft.fileManager.view.common.Bunch

nrgsoft.fileManager.action.common.Bunch = class extends Action {

  get viewClass () {
    return View
  }

  initialize () {
    super.initialize()

    this.view = this.createView(this.viewClass, {
      emptyMessage: this.emptyMessage,
      confirmMessage: this.confirmMessage
    })
  }

  show () {
    this.view.showContent(this.clipboard.isEmpty)
  }

  async onConfirm (event) {
    event.preventDefault()

    await this.task()
  }
}
