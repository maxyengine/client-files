const
  Action = nrgsoft.fileManager.action.common.Task

nrgsoft.fileManager.action.common.Save = class extends Action {

  async execute (event) {
    this.id = event.url.params.id

    return super.execute(event)
  }

  async show () {
    super.show()

    if (this.id) {
      this.form.populate(await this.fetch(this.id))
    }
  }

  async task (input) {
    const {id} = this.id ?
      await this.update({id: this.id, ...input}) :
      await this.create(input)

    this.close({id})
  }
}
