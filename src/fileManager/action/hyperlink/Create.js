const
  Action = nrgsoft.fileManager.action.common.Task,
  Form = nrgsoft.fileManager.form.hyperlink.Create,
  Path = nrgsoft.fileManager.value.Path,
  Url = nrgsoft.sdk.web.Url

nrgsoft.fileManager.action.hyperlink.Create = class extends Action {

  get defaults () {
    return {
      title: 'create hyperlink',
      formClass: Form
    }
  }

  execute (event) {
    this.parent = new Path(event.url.getParam('parent'), true)

    return super.execute(event)
  }

  async task ({url, path: fileName}) {
    fileName = `${fileName}.${new Url(url).protocol}`
    const path = this.parent.join(fileName).value

    await this.hyperlinkModel.create({url, path})

    this.close({fileName})
  }
}
