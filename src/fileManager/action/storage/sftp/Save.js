const
    SaveAction = nrgsoft.fileManager.action.common.Save,
    Form = nrgsoft.fileManager.form.storage.sftp.Save;

nrgsoft.fileManager.action.storage.sftp.Save = class extends SaveAction {

    get defaults() {
        return {
            backHref: '/file-manager/storage/list',
            formClass: Form
        };
    }

    get title() {
        return this.id ? 'update sftp storage' : 'create sftp storage'
    }

    fetch(id) {
        return this.storageModel.details({id});
    }

    create(data) {
        return this.storageModel.createSftp(data);
    }

    update(data) {
        return this.storageModel.updateSftp(data);
    }
}
