const
  Action = nrgsoft.fileManager.action.Action,
  View = nrgsoft.fileManager.view.storage.List

nrgsoft.fileManager.action.storage.List = class extends Action {

  get properties () {
    return {
      hasSelectedItems: {view: 'hasSelectedItems'},
      selectedItems: {view: 'selectedItems'},
      hasActiveItem: {view: 'hasActiveItem'},
      activeItem: {view: 'activeItem'}
    }
  }

  _constructor (...args) {
    this.view = this.createView(View)

    super._constructor(...args)
  }

  execute (event) {
    this.params = event.url.params

    return this.storageModel.list(this.params)
      .then(storages => {
        this.layout.content = this.view
        this.view.storages = storages
      })
  }

  fillInClipboard () {
    if (this.hasSelectedItems) {
      this.clipboard.copy(...this.selectedItems)
    } else if (this.hasActiveItem) {
      this.clipboard.copy(this.activeItem)
    }
  }

  onDeleteItem () {
    this.fillInClipboard()
  }
}
