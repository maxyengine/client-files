const
  Action = nrgsoft.fileManager.action.common.Bunch,
  DeleteStorages = nrgsoft.fileManager.useCase.storage.Delete

nrgsoft.fileManager.action.storage.Delete = class extends Action {

  static get services () {
    return {
      ...Action.services,
      deleteStorages: DeleteStorages
    }
  }

  get defaults () {
    return {
      backHref: '/file-manager/storage/list',
      title: 'delete',
      emptyMessage: 'there are no selected storages',
      confirmMessage: 'delete selected storages ?'
    }
  }

  async task () {
    await this.deleteStorages.execute(this.view.progressList, [...this.clipboard])

    this.close()
  }
}
