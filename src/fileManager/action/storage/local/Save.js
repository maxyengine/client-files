const
    SaveAction = nrgsoft.fileManager.action.common.Save,
    Form = nrgsoft.fileManager.form.storage.local.Save;

nrgsoft.fileManager.action.storage.local.Save = class extends SaveAction {

    get defaults() {
        return {
            backHref: '/file-manager/storage/list',
            formClass: Form
        };
    }

    get title() {
        return this.id ? 'update local storage' : 'create local storage'
    }

    fetch(id) {
        return this.storageModel.details({id});
    }

    create(data) {
        return this.storageModel.createLocal(data);
    }

    update(data) {
        return this.storageModel.updateLocal(data);
    }
}
