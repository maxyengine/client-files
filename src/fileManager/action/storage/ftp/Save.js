const
    SaveAction = nrgsoft.fileManager.action.common.Save,
    Form = nrgsoft.fileManager.form.storage.ftp.Save;

nrgsoft.fileManager.action.storage.ftp.Save = class extends SaveAction {

    get defaults() {
        return {
            backHref: '/file-manager/storage/list',
            formClass: Form
        };
    }

    get title() {
        return this.id ? 'update ftp storage' : 'create ftp storage'
    }

    fetch(id) {
        return this.storageModel.details({id});
    }

    create(data) {
        return this.storageModel.createFtp(data);
    }

    update(data) {
        return this.storageModel.updateFtp(data);
    }
}
