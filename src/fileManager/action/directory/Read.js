const
  Action = nrgsoft.fileManager.action.Action,
  Layout = nrgsoft.widget.layout.Default,
  View = nrgsoft.fileManager.view.directory.Read,
  Parent = nrgsoft.fileManager.entity.Parent,
  Path = nrgsoft.fileManager.value.Path,
  File = nrgsoft.fileManager.entity.File

nrgsoft.fileManager.action.directory.Read = class extends Action {

  get defaults () {
    return {
      defaultParams: {
        orderBy: [
          {field: 'name', direction: 'asc'}
        ]
      }
    }
  }

  get properties () {
    return {
      activateView: {view: 'activateWidget'},
      itemSelector: {view: 'itemSelector'},
      hasSelectedItems: {view: 'hasSelectedItems'},
      selectedItems: {view: 'selectedItems'},
      hasActiveItem: {view: 'hasActiveItem'},
      activeItem: {view: 'activeItem'}
    }
  }

  get events () {
    return {
      activateView: {view: 'activateWidget'}
    }
  }

  get layout () {
    return this.layouts.getLayout(Layout)
  }

  get backHref () {
    return encodeURIComponent(this.createUrl('/', this.params).href)
  }

  _constructor (...args) {
    this.view = this.createView(View, {
      wrapper: this.layouts.wrapper
    })

    super._constructor(...args)
  }

  execute (event) {
    this.clipboard.clear()

    const
      {url, body} = event,
      {actionWasCanceled, noNeedRefresh} = body

    if (noNeedRefresh) {
      this.itemSelector.deselectAll()
      return
    }

    if (actionWasCanceled) {
      return
    }

    this.params = {...this.defaultParams, ...url.params}
    this.layout.content = this.view

    return this.storageModel.list()
      .then(storages => {
        if (!this.params.path) {
          this.params.path = storages.data[0].id + '://'
        }

        this.view.path = new Path(this.params.path)
        this.view.storages = storages
        this.view.showProgress()

        return this.directoryModel.read(this.params)
      })
      .then(directory => {
        this.view.hideProgress()
        this.view.urlParams = this.params
        this.view.fileName = this.params.fileName
        this.view.directory = directory
      })
      .catch(error => { //todo: make sure that is 404

        console.log(error)

        this.execute({
          ...event,
          url: this.createUrl('/file-manager')
        })
      })
  }

  onCreateDirectory ({params}) {
    params.parent = this.params.path
    params.backHref = this.backHref
  }

  onCreateFile ({params}) {
    params.parent = this.params.path
    params.backHref = this.backHref
  }

  onCreateHyperlink ({params}) {
    params.parent = this.params.path
    params.backHref = this.backHref
  }

  onCopyFile ({params}) {
    params.backHref = this.backHref
    this.fillInClipboard()
  }

  onUploadFile ({params}) {
    params.path = this.params.path
    params.backHref = this.backHref
  }

  onDownloadFile () {
    this.fillInClipboard()
  }

  onZipFile ({params}) {
    params.currentPath = this.params.path
    params.backHref = this.backHref
    this.fillInClipboard()
  }

  onRenameFile ({params}) {
    if (!this.hasActiveItem || this.activeItem instanceof Parent) {
      throw new Error('No active item')
    }

    params.path = this.activeItem.path.value
    params.backHref = this.backHref
  }

  onDeleteFile ({params}) {
    params.backHref = this.backHref
    this.fillInClipboard()
  }

  onBeforeOpen ({entity, params}) {
    if (entity instanceof File && 'zip' === entity.extension) {
      params.backHref = this.backHref
    }
  }

  fillInClipboard () {
    if (this.hasSelectedItems) {
      this.clipboard.copy(...this.selectedItems)
    } else if (this.hasActiveItem && !(this.activeItem instanceof Parent)) {
      this.clipboard.copy(this.activeItem)
    }
  }
}
