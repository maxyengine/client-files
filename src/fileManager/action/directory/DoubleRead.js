const
  Action = nrgsoft.fileManager.action.Action,
  View = nrgsoft.fileManager.view.directory.DoubleRead,
  Read = nrgsoft.fileManager.action.directory.Read,
  Layouts = nrgsoft.sdk.gui.Layouts,
  Path = nrgsoft.fileManager.value.Path,
  File = nrgsoft.fileManager.entity.File

const
  inactiveParams = Symbol(),
  activePanel = Symbol(),
  firstTime = Symbol()

nrgsoft.fileManager.action.directory.DoubleRead = class extends Action {

  get events () {
    return {
      activateView: {left: 'activateView', right: 'activateView'},
      createDirectory: {'left.view': 'createDirectory', 'right.view': 'createDirectory'},
      createFile: {'left.view': 'createFile', 'right.view': 'createFile'},
      createHyperlink: {'left.view': 'createHyperlink', 'right.view': 'createHyperlink'},
      copyFile: {'left.view': 'copyFile', 'right.view': 'copyFile'},
      uploadFile: {'left.view': 'uploadFile', 'right.view': 'uploadFile'},
      zipFile: {'left.view': 'zipFile', 'right.view': 'zipFile'},
      renameFile: {'left.view': 'renameFile', 'right.view': 'renameFile'},
      deleteFile: {'left.view': 'deleteFile', 'right.view': 'deleteFile'},
      beforeOpen: {'left.view': 'beforeOpen', 'right.view': 'beforeOpen'}
    }
  }

  get activeAction () {
    return 'left' === this.activePanel ? this.left : this.right
  }

  get inactiveAction () {
    return 'left' === this.activePanel ? this.right : this.left
  }

  get activeParams () {
    return this.activeAction.params
  }

  get inactiveParams () {
    return this.inactiveAction.params || this[inactiveParams]
  }

  initialize () {
    this[firstTime] = true

    this.restoreParams()

    window.addEventListener('beforeunload', () => {
      this.storeParams()
    })

    this.view = this.createView(View, {
      isActiveHotkeys: true
    })

    this.left = this.createReadAction(this.view.left)
    this.right = this.createReadAction(this.view.right)
  }

  async execute (event) {
    const {url, body} = event

    this.layout.mainMenu.alignLinks(url.params)
    this.layout.content = this.view

    this[activePanel] = this.activePanel
    this[inactiveParams] = this.inactiveParams

    this.activeAction.execute(event)
    this.activeAction.activateView(true)

    if (this[firstTime] || body.reload) {
      delete body.noNeedRefresh
      this.inactiveAction.execute({...event, url: url.clone(this.inactiveParams)})
    }

    this[firstTime] = false
  }

  onSwitchToLeft () {
    if ('right' === this.activePanel) {
      this.left.activateView()
    }
  }

  onSwitchToRight () {
    if ('left' === this.activePanel) {
      this.right.activateView()
    }
  }

  onActivateView (event, self, action) {
    if (action === this.left) {
      this.activePanel = 'left'

      this.view.activeLeft = true
      this.view.inactiveRight = true
    } else {
      this.activePanel = 'right'

      this.view.activeRight = true
      this.view.inactiveLeft = true
    }
  }

  onCreateDirectory ({body}) {
    this.doReloadInactivePanel(body)
  }

  onCreateFile ({body}) {
    this.doReloadInactivePanel(body)
  }

  onCreateHyperlink ({body}) {
    this.doReloadInactivePanel(body)
  }

  onCopyFile ({params, body}) {
    params.copyTo = this.inactiveParams.path
    body.reload = true
  }

  onUploadFile ({body}) {
    this.doReloadInactivePanel(body)
  }

  onZipFile ({body}) {
    this.doReloadInactivePanel(body)
  }

  onRenameFile ({body}) {
    this.doReloadInactivePanel(body)
  }

  onDeleteFile ({body}) {
    const inactivePath = new Path(this.inactiveParams.path, true)

    for (const {path} of this.clipboard) {
      if (path.isDirectory && path.contains(inactivePath)) {
        body.reload = true
        break
      }
    }

    this.doReloadInactivePanel(body)
  }

  onBeforeOpen ({entity, body}) {
    if (entity instanceof File && 'zip' === entity.extension) {
      this.doReloadInactivePanel(body)
    }
  }

  doReloadInactivePanel (body) {
    if (this.activeParams.path === this.inactiveParams.path) {
      body.reload = true
    }
  }

  createReadAction (wrapper) {
    return this.injector.createObject(Read, {}, {
      layouts: this.injector.createObject(Layouts, {
        wrapper: wrapper
      })
    })
  }

  restoreParams () {
    this.activePanel = this.session.read('activePanel', 'left')
    this[inactiveParams] = JSON.parse(this.session.read('inactiveParams', '{}'))
  }

  storeParams () {
    this.session.write('activePanel', this[activePanel])
    this.session.write('inactiveParams', JSON.stringify(this[inactiveParams] || {}))
  }
}
