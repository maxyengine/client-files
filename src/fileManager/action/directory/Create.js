const
  Action = nrgsoft.fileManager.action.common.Task,
  Form = nrgsoft.fileManager.form.directory.Create,
  Path = nrgsoft.fileManager.value.Path

nrgsoft.fileManager.action.directory.Create = class extends Action {

  get defaults () {
    return {
      title: 'create directory',
      formClass: Form
    }
  }

  execute (event) {
    this.parent = new Path(event.url.getParam('parent'), true)

    return super.execute(event)
  }

  async task ({path: fileName}) {
    await this.directoryModel.create({
      path: this.parent.join(fileName).value
    })

    this.close({fileName})
  }
}
