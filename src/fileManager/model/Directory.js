const
    Model = nrgsoft.fileManager.model.Model,
    Type = nrgsoft.sdk.lang.utility.Type;

const self = nrgsoft.fileManager.model.Directory = class extends Model {

    read(body) {
        return this.fetchDirectory('/read', body)
            .then(entity => {
                if (body.orderBy) {
                    entity.children = self.orderBy(entity.children, body.orderBy);
                }

                return entity;
            });
    }

    create(body) {
        return this.fetchDirectory('/create', body);
    }

    delete(body) {
        return this.fetchDirectory('/delete', body);
    }

    static orderBy(collection, orderBy) {
        const items = Type.isObject(orderBy) ? Type.objectToArray(orderBy) : orderBy;

        items.forEach(item => {
            collection = self.sort(collection, item.field, item.direction);
        });

        return collection;
    }

    static sort(collection, field, direction = 'asc') {
        const sortArray = collection.map(function (data, index) {
            return {
                index: index,
                data: data
            };
        });

        sortArray.sort((a, b) => {
            const indexOrder = a.index - b.index;

            a = a.data;
            b = b.data;

            if (a.type === 'directory' && b.type !== 'directory') {
                return -1;
            } else if (b.type === 'directory' && a.type !== 'directory') {
                return 1;
            }

            let
                valueA = self.getFieldValue(a, field),
                valueB = self.getFieldValue(b, field),
                multiplier = 'asc' === direction ? 1 : -1;

            if (Type.isSet(valueA) && !Type.isSet(valueB)) {
                return 1;
            } else if (!Type.isSet(valueA) && Type.isSet(valueB)) {
                return -1;
            } else if (!Type.isSet(valueA) && !Type.isSet(valueB)) {
                return indexOrder;
            }

            if (Type.isString(valueA)) {
                return multiplier * valueA.toLocaleLowerCase().localeCompare(valueB.toLocaleLowerCase()) || indexOrder;
            } else {
                return multiplier * (valueA > valueB ? 1 : (valueB > valueA ? -1 : 0)) || indexOrder;
            }
        });

        return sortArray.map(function (value) {
            return value.data;
        });
    }

    static getFieldValue(entity, field) {
        switch (field) {
            case 'name':
                return entity.path.fileName.value;
            case 'extension':
                return entity.path.fileName.extension;
            case 'size':
                return entity.size && entity.size.value;
            case 'lastModified':
                return entity.lastModified.getTime();
            default:
                return entity[field];
        }
    }
}
