const
    Model = nrgsoft.fileManager.model.Model;

nrgsoft.fileManager.model.Storage = class extends Model {

    list(body) {
        return this.fetchStorages('/list', body);
    }

    details(body) {
        return this.fetchStorage('/details', body);
    }

    delete(body) {
        return this.fetchStorage('/delete', body);
    }

    createLocal(body) {
        return this.fetchStorage('/local/create', body);
    }

    updateLocal(body) {
        return this.fetchStorage('/local/update', body);
    }

    createFtp(body) {
        if (!body.timeout) {
            delete body.timeout;
        }

        if (!body.port) {
            delete body.port;
        }

        return this.fetchStorage('/ftp/create', body);
    }

    updateFtp(body) {
        if (!body.timeout) {
            delete body.timeout;
        }

        if (!body.port) {
            delete body.port;
        }

        return this.fetchStorage('/ftp/update', body);
    }

    createSftp(body) {
        if (!body.timeout) {
            delete body.timeout;
        }

        if (!body.port) {
            delete body.port;
        }

        return this.fetchStorage('/sftp/create', body);
    }

    updateSftp(body) {
        if (!body.timeout) {
            delete body.timeout;
        }

        if (!body.port) {
            delete body.port;
        }

        return this.fetchStorage('/sftp/update', body);
    }
}
