const
    WebModel = nrgsoft.sdk.web.Model,
    FileFactory = nrgsoft.fileManager.entity.FileFactory,
    StorageFactory = nrgsoft.fileManager.entity.StorageFactory;

nrgsoft.fileManager.model.Model = class extends WebModel {

    static get services() {
        return {
            ...WebModel.services,
            fileFactory: FileFactory,
            storageFactory: StorageFactory
        };
    }

    get defaults() {
        return {
            fileApiUrl: '/file-manager/file',
            hyperlinkApiUrl: '/file-manager/hyperlink',
            directoryApiUrl: '/file-manager/directory',
            storageApiUrl: '/file-manager/storage'
        };
    }

    fetchFile(url, body = {}) {
        return this.fetch(this.fileApiUrl + url, {body: body})
            .then(json => {
                return json && this.fileFactory.createFile(json);
            });
    }

    fetchHyperlink(url, body = {}) {
        return this.fetch(this.hyperlinkApiUrl + url, {body: body})
            .then(json => {
                return json && this.fileFactory.createHyperlink(json);
            });
    }

    fetchDirectory(url, body = {}) {
        return this.fetch(this.directoryApiUrl + url, {body: body})
            .then(json => {
                return json && this.fileFactory.createDirectory(json);
            });
    }

    fetchStorage(url, body = {}) {
        return this.fetch(this.storageApiUrl + url, {body: body})
            .then(json => {
                return json && this.storageFactory.createStorage(json);
            });
    }

    fetchStorages(url, body = {}) {
        return this.fetch(this.storageApiUrl + url, {body: body})
            .then(json => {
                return json && this.storageFactory.createStorages(json);
            });
    }

    fetchUploadedFile(url, data) {
        const body = new FormData();
        body.append('file', data.file);
        body.append('path', data.path);

        return this.upload(this.fileApiUrl + url, {body: body})
            .onLoad(json => {
                return this.fileFactory.createFile(json);
            });
    }
}
