const
    Model = nrgsoft.fileManager.model.Model;

nrgsoft.fileManager.model.File = class extends Model {

    read(body) {
        return this.fetchFile('/read', body);
    }

    create(body) {
        return this.fetchFile('/create', body);
    }

    update(body) {
        return this.fetchFile('/update', body);
    }

    copy(body) {
        return this.fetchFile('/copy', body);
    }

    move(body) {
        return this.fetchFile('/move', body);
    }

    uploadFile(body) {
        return this.fetchUploadedFile('/upload', body);
    }

    delete(body) {
        return this.fetchFile('/delete', body);
    }

    exists(body) {
        return this.fetch(this.fileApiUrl + '/exists', {body: body});
    }
}
