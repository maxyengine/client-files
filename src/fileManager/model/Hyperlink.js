const
    Model = nrgsoft.fileManager.model.Model;

nrgsoft.fileManager.model.Hyperlink = class extends Model {

    read(body) {
        return this.fetchHyperlink('/read', body);
    }

    create(body) {
        return this.fetchHyperlink('/create', body);
    }

    update(body) {
        return this.fetchHyperlink('/update', body);
    }
}
