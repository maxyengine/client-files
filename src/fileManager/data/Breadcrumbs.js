const
    List = nrgsoft.sdk.data.List,
    FileFactory = nrgsoft.fileManager.entity.FileFactory;

nrgsoft.fileManager.data.Breadcrumbs = class extends List {

    static get services() {
        return {
            fileFactory: FileFactory
        };
    }

    set path(path) {
        let
            data = [],
            item = path,
            next = null;

        while (item) {
            data.push({
                content: item.fileName.value,
                url: this.fileFactory.createOpenUrl(item, 'directory', next && next.fileName)
            });
            next = item;
            item = item.parent;
        }

        this.data = data.reverse();
    }
}
