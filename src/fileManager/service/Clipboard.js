
const items = Symbol();

nrgsoft.fileManager.service.Clipboard = class {

    constructor() {
        this.clear();
    }

    get isEmpty() {
        return !this.size;
    }

    get size() {
        return this[items].size;
    }

    get asArray() {
        return [...this[items]];
    }

    copy(...data) {
        this[items] = new Set(data);
    }

    clear() {
        this[items] = new Set();
    }

    [Symbol.iterator]() {
        return this[items].values();
    }
}
