const
    Value = nrgsoft.sdk.lang.Value;

nrgsoft.fileManager.entity.File = class extends Value {

  get extension() {
    return this.path.fileName.extension
  }
}
