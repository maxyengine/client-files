const
    Value = nrgsoft.sdk.lang.Value;

nrgsoft.fileManager.entity.Directory = class extends Value {

    static get extension() {
        return 'dir';
    }

    get isEmpty() {
        return !this.children || !this.children.length;
    }
}
