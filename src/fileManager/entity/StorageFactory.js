const
    Value = nrgsoft.sdk.lang.Value,
    Storage = nrgsoft.fileManager.entity.Storage;

nrgsoft.fileManager.entity.StorageFactory = class extends Value {

    static get services() {
        return {
            router: 'router'
        };
    }

    createStorage(raw) {
        const data = {...raw};

        data.createdAt = new Date(data.createdAt);
        data.updatedAt = data.updatedAt && new Date(data.updatedAt);
        data.openUrl = this.createOpenUrl(data);

        return new Storage(data);
    }

    createStorages(collection) {
        collection = {...collection};

        const data = [];

        collection.data.forEach(raw => {
            data.push(this.createStorage(raw));
        });

        collection.data = data;

        return collection;
    }

    createOpenUrl({id, type}) {
        switch (type) {
            case 'local':
                return this.router.createUrl('/file-manager/storage/local/save', {id});
            case 'ftp':
                return this.router.createUrl('/file-manager/storage/ftp/save', {id});
            case 'sftp':
                return this.router.createUrl('/file-manager/storage/sftp/save', {id});
        }
    }
}
