const
  Value = nrgsoft.sdk.lang.Value,
  File = nrgsoft.fileManager.entity.File,
  Hyperlink = nrgsoft.fileManager.entity.Hyperlink,
  Directory = nrgsoft.fileManager.entity.Directory,
  Parent = nrgsoft.fileManager.entity.Parent,
  Path = nrgsoft.fileManager.value.Path,
  Size = nrgsoft.fileManager.value.Size,
  Obj = nrgsoft.sdk.lang.utility.Object

nrgsoft.fileManager.entity.FileFactory = class extends Value {

  static get services () {
    return {
      router: 'router',
      client: 'client'
    }
  }

  createFile (raw) {
    const data = {...raw}

    data.lastModified = data.lastModified && new Date(data.lastModified)
    data.path = new Path(data.path)
    data.size = data.size && new Size(data.size)
    data.openUrl = this.createOpenUrl(data.path, data.type)

    return new File(data)
  }

  createHyperlink (raw) {
    const data = {...raw}

    data.lastModified = data.lastModified && new Date(data.lastModified)
    data.path = new Path(data.path)
    data.size = data.size && new Size(data.size)
    data.openUrl = this.createOpenUrl(data.path, data.type)

    return new Hyperlink(data)
  }

  createDirectory (raw) {
    const data = {...raw}

    data.lastModified = data.lastModified && new Date(data.lastModified)
    data.path = new Path(data.path, true)
    data.size = data.size && new Size(data.size)
    data.openUrl = this.createOpenUrl(data.path, data.type)

    if (raw.children) {
      data.children = []
      raw.children.forEach((child, index) => {
        data.children[index] = this.createEntity(child)
      })
    }

    return new Directory(data)
  }

  createEntity (raw) {
    switch (raw.type) {
      case 'file':
        return this.createFile(raw)
      case 'hyperlink':
        return this.createHyperlink(raw)
      case 'directory':
        return this.createDirectory(raw)
      default:
        throw new Error('Unknown file type')
    }
  }

  createParent (directory) {
    return new Parent({
      path: directory.path.parent,
      openUrl: this.createOpenUrl(directory.path.parent, 'directory', directory.path.fileName)
    })
  }

  createOpenUrl (path, type, fileName) {
    switch (type) {
      case 'file':
      case 'hyperlink':
        switch (path.fileName.extension) {
          case 'txt':
            return this.router.createUrl('/text-editor', {
              path: path.value
            })
          case 'zip':
            return this.router.createUrl('/file-manager/file/unzip', {
              path: path.value
            })
          default:
            const url = this.client.createAuthorizedUrl('/file-manager/file/open')
            url.setParam('path', path.value)
            url.options = {
              rel: 'external',
              target: '_blank'
            }

            return url
        }
      default:
        return this.router.createUrl('/file-manager', Obj.filterUndefined({
          path: path.value,
          fileName: fileName && fileName.value
        }))
    }
  }
}
