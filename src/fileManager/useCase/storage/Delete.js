const
  Value = nrgsoft.sdk.lang.Value,
  Model = nrgsoft.fileManager.model.Storage

const
  deleteItems = Symbol(),
  deleteItem = Symbol(),
  complete = Symbol()

nrgsoft.fileManager.useCase.storage.Delete = class extends Value {

  static get services () {
    return {
      model: Model
    }
  }

  execute (progressList, items) {
    this.progressList = progressList
    this[deleteItems](items)

    return this[complete](progressList)
  }

  [complete] (progressList) {
    return new Promise(resolve => {
      progressList.on(new class {
        onClear () {
          progressList.off(this)
          resolve()
        }
      }())
    })
  }

  [deleteItems] (items, progressItem = null, length = null) {
    progressItem = progressItem || this.progressList.addItem()
    length = length || items.length

    let item = items.shift()

    if (!item) {
      return this.progressList.removeItem(progressItem)
    }

    const percent = 100 - Math.ceil(100 * items.length / length)

    progressItem.set({
      caption: item.name,
      percent: percent + '%'
    })

    this[deleteItem]({id: item.id})
      .then(() => {
        this[deleteItems](items, progressItem, length)
      })
  }

  [deleteItem] (data) {
    return this.model.delete(data)
  }
}
