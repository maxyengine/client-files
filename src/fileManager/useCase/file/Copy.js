const
    Value = nrgsoft.sdk.lang.Value,
    FileModel = nrgsoft.fileManager.model.File,
    DirectoryModel = nrgsoft.fileManager.model.Directory,
    Directory = nrgsoft.fileManager.entity.Directory,
    Path = nrgsoft.fileManager.value.Path;

const
    copyFiles = Symbol(),
    expandedFileList = Symbol(),
    copyProcess = Symbol(),
    copyFile = Symbol(),
    moveFile = Symbol(),
    readDirectory = Symbol(),
    createDirectory = Symbol(),
    deleteDirectory = Symbol(),
    complete = Symbol();

nrgsoft.fileManager.useCase.file.Copy = class extends Value {

    static get services() {
        return {
            fileModel: FileModel,
            directoryModel: DirectoryModel
        };
    }

    execute(progressList, files, toPath, move, overwrite) {
        this.progressList = progressList;
        this[copyFiles](files, new Path(toPath, true), move, overwrite);

        return this[complete](progressList);
    }

    [complete](progressList) {
        return new Promise(resolve => {
            progressList.on(new class {
                onClear() {
                    progressList.off(this);
                    resolve();
                }
            }());
        });
    }

    async [copyFiles](files, toPath, move, overwrite) {
        this[copyProcess](
            await this[expandedFileList](files, toPath, move),
            move,
            overwrite
        );
    }

    async [expandedFileList](files, toPath, move, isRoot = true) {
        let list = [];

        for (const file of files) {
            let newPath = toPath.join(file.path.fileName, file instanceof Directory);

            if (newPath.isDirectory) {
                let directory = await this[readDirectory](file);

                if (directory.isEmpty) {
                    list.push(newPath);
                } else {
                    list = list.concat(await this[expandedFileList](directory.children, newPath, move, false));
                }

                if (move && isRoot) {
                    list.push(file);
                }
            } else {
                file.newPath = newPath;
                list.push(file);
            }
        }

        return list;
    }

    [copyProcess](files, move, overwrite, item = null, length = null) {
        item = item || this.progressList.addItem();
        length = length || files.length;

        let file = files.shift();

        if (!file) {
            return this.progressList.removeItem(item);
        }

        const percent = 100 - Math.ceil(100 * files.length / length);

        item.set({
            caption: file instanceof Path ? file.value : file.path.value,
            percent: percent + '%'
        });

        let promise = null;

        if (file instanceof Path) {
            promise = this[createDirectory](file);
        } else if (file instanceof Directory) {
            promise = this[deleteDirectory](file);
        } else if (move) {
            promise = this[moveFile](file, overwrite)
        } else {
            promise = this[copyFile](file, overwrite)
        }

        promise.then(() => {
            this[copyProcess](files, move, overwrite, item, length);
        });
    }

    [copyFile](file, overwrite) {
        return this.fileModel.copy({
            path: file.path.value,
            newPath: file.newPath.value,
            overwrite: overwrite
        });
    }

    [moveFile](file, overwrite) {
        return this.fileModel.move({
            path: file.path.value,
            newPath: file.newPath.value,
            overwrite: overwrite
        });
    }

    [createDirectory](path) {
        return this.fileModel
            .exists({
                path: path.value
            })
            .then(exists => {
                if (!exists) {
                    return this.directoryModel.create({
                        path: path.value
                    });
                }
            });
    }

    [deleteDirectory](directory) {
        return this.directoryModel.delete({
            path: directory.path.value
        });
    }

    [readDirectory](directory) {
        return this.directoryModel.read({path: directory.path.value});
    }
}
