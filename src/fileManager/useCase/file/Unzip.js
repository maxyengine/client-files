const
    Value = nrgsoft.sdk.lang.Value,
    FileModel = nrgsoft.fileManager.model.File,
    DirectoryModel = nrgsoft.fileManager.model.Directory,
    CopyFiles = nrgsoft.fileManager.useCase.file.Copy;

const deleteTmpZip = Symbol();

nrgsoft.fileManager.useCase.file.Unzip = class extends Value {

    static get services() {
        return {
            fileModel: FileModel,
            directoryModel: DirectoryModel,
            copyFiles: CopyFiles
        };
    }

    execute(zipPath, toPath, progressList, overwrite) {
        const
            tempZipName = `${Date.now()}-${zipPath.fileName.value}`,
            tempZipPath = `temp://${tempZipName}`,
            fromPath = `temp-zip:${tempZipName}://`,
            completePromise = this[deleteTmpZip](progressList, tempZipPath);

        this.fileModel
            .copy({
                path: zipPath.value,
                newPath: tempZipPath,
                overwrite: true
            })
            .then(() => {
                return this.fileModel.exists({
                    path: toPath.value
                });
            })
            .then(exists => {
                if (!exists) {
                    return this.directoryModel.create({
                        path: toPath.value
                    });
                }
            })
            .then(() => {
                return this.directoryModel.read({
                    path: fromPath
                });
            })
            .then(directory => {
                this.copyFiles.execute(progressList, directory.children, toPath.value, false, overwrite);
            });

        return completePromise;
    }

    [deleteTmpZip](progressList, tempZipPath) {
        const {fileModel} = this;

        return new Promise(resolve => {
            progressList.on(new class {
                onClear() {
                    fileModel
                        .delete({
                            path: tempZipPath
                        })
                        .then(() => {
                            progressList.off(this);
                            resolve();
                        });
                }
            }());
        });
    }
}
