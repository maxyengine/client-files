const
    Value = nrgsoft.sdk.lang.Value,
    FileModel = nrgsoft.fileManager.model.File,
    CopyFiles = nrgsoft.fileManager.useCase.file.Copy;

const moveTmpZip = Symbol();

nrgsoft.fileManager.useCase.file.Zip = class extends Value {

    static get services() {
        return {
            fileModel: FileModel,
            copyFiles: CopyFiles
        };
    }

    execute(zipPath, files, progressList) {
        const
            tempZipName = `${Date.now()}-${zipPath.fileName.value}`,
            toPath = `temp-zip:${tempZipName}://`,
            completePromise = this[moveTmpZip](progressList, `temp://${tempZipName}`, zipPath.value);

        this.copyFiles.execute(progressList, files, toPath, false, true);

        return completePromise;
    }

    [moveTmpZip](progressList, tempZipPath, zipPath) {
        const {fileModel} = this;

        return new Promise(resolve => {
            progressList.on(new class {
                onClear() {
                    fileModel
                        .move({
                            path: tempZipPath,
                            newPath: zipPath,
                            overwrite: true
                        })
                        .then(() => {
                            progressList.off(this);
                            resolve();
                        });
                }
            }());
        });
    }
}
