const
    Value = nrgsoft.sdk.lang.Value,
    CopyFiles = nrgsoft.fileManager.useCase.file.Copy;

nrgsoft.fileManager.useCase.file.CreateTempZip = class extends Value {

    static get services() {
        return {
            copyFiles: CopyFiles
        };
    }

    execute(zipName, files, progressList) {
        const
            tempZipName = `${Date.now()}-${zipName}`,
            toPath = `temp-zip:${tempZipName}://`,
            tempZipPath = `temp://${tempZipName}`;
        

        return this.copyFiles
            .execute(progressList, files, toPath, false, true)
            .then(() => tempZipPath);
    }
}
