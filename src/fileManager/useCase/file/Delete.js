const
    Value = nrgsoft.sdk.lang.Value,
    FileModel = nrgsoft.fileManager.model.File,
    DirectoryModel = nrgsoft.fileManager.model.Directory,
    Directory = nrgsoft.fileManager.entity.Directory;

const
    deleteFiles = Symbol(),
    deleteFile = Symbol(),
    deleteDirectory = Symbol(),
    complete = Symbol();

nrgsoft.fileManager.useCase.file.Delete = class extends Value {

    static get services() {
        return {
            fileModel: FileModel,
            directoryModel: DirectoryModel
        };
    }

    execute(progressList, files) {
        this.progressList = progressList;
        this[deleteFiles](files);

        return this[complete](progressList);
    }

    [complete](progressList) {
        return new Promise(resolve => {
            progressList.on(new class {
                onClear() {
                    progressList.off(this);
                    resolve();
                }
            }());
        });
    }

    [deleteFiles](files, item = null, length = null) {
        item = item || this.progressList.addItem();
        length = length || files.length;

        let file = files.shift();

        if (!file) {
            return this.progressList.removeItem(item);
        }

        const percent = 100 - Math.ceil(100 * files.length / length);

        item.set({
            caption: file.path.value,
            percent: percent + '%'
        });


        if (file instanceof Directory) {
            this[deleteDirectory]({path: file.path.value})
                .then(() => {
                    this[deleteFiles](files, item, length);
                });
        } else {
            this[deleteFile]({path: file.path.value})
                .then(() => {
                    this[deleteFiles](files, item, length);
                });
        }
    }

    [deleteFile](data) {
        return this.fileModel.delete(data);
    }

    [deleteDirectory](data) {
        return this.directoryModel.delete(data);
    }
}
