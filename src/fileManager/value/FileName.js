const
    Directory = nrgsoft.fileManager.entity.Directory;

const
    value = Symbol(),
    isDirectory = Symbol(),
    baseName = Symbol(),
    extension = Symbol();

const self = nrgsoft.fileManager.value.FileName = class {

    constructor(val, isDir = false) {
        this[value] = val;
        this[isDirectory] = isDir;
        this[extension] = isDir ? Directory.extension : val.slice((val.lastIndexOf('.') - 1 >>> 0) + 2);
        this[baseName] = isDir || !this[extension] ? val : val.slice(0, -this[extension].length - 1);
    }

    get length() {
        return this.value.length;
    }

    get value() {
        return this[value];
    }

    get isDirectory() {
        return this[isDirectory];
    }

    get baseName() {
        return this[baseName];
    }

    get extension() {
        return this[extension];
    }

    toString() {
        return this.value;
    }

    isEqual(fileName) {
        return fileName instanceof self ? fileName.value === this.value : fileName === this.value;
    }
}
