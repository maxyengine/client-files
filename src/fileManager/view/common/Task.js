const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftWidgetDialogConfirm = nrgsoft.widget.dialog.Confirm,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.fileManager.view.common.Task = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
                
    get form() {
                return this._form;
    }
                
    get _e2_() {
                
        if (!this.__e2_) {
            const
                properties = {},
                services = {
                    t: this.it.t
                };
            
            this.__e2_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.dialog.Confirm, properties, services) :
                new nrgsoft.widget.dialog.Confirm(properties, services);
        }

        return this.__e2_;
    
    }
                
    get isActiveWidget() {
                return this._e2_.isActiveHotkeys;
    }
    
    set form(value) {
                this._form = Element.insert(this._e1, value);
    }
    
    set isActiveWidget(value) {
                this._e2_.isActiveHotkeys = value;
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = document.createElement('div');
        this._element.appendChild(this._e1);
        this._e2 = this._e2_.element;
        this._element.appendChild(this._e2);
            
    this._element.classList.add('nrgsoft-fileManager-view-common-Task');
    
        super._constructor(properties);
        this.it.mapEvent(this._e2_, 'confirm', 'confirm');
        this.it.mapEvent(this._e2_, 'cancel', 'cancel');
    
    }
}
