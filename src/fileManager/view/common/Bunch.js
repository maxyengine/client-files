const
  Component = nrgsoft.sdk.rx.Component,
  Template = nrgsoft.fileManager.view.common.bunch.Tpl

nrgsoft.fileManager.view.common.Bunch = class extends Component {

  get traits () {
    return [
      this.template || Template
    ]
  }

  showContent (isEmptyList) {
    if (!isEmptyList) {
      this.showConfirmDialog()
    } else if (this.progressList.isEmpty) {
      this.showEmptyMessage()
    } else {
      this.showProgressList()
    }
  }

  onConfirm () {
    this.showProgressList()
  }
}
