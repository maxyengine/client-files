const
  View = nrgsoft.fileManager.view.file.Unzip,
  Template = nrgsoft.fileManager.view.file.zip.Tpl

nrgsoft.fileManager.view.file.Zip = class extends View {

  get template () {
    return Template
  }
}
