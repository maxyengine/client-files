const
  View = nrgsoft.fileManager.view.common.Bunch,
  Template = nrgsoft.fileManager.view.file.copy.Tpl

nrgsoft.fileManager.view.file.Copy = class extends View {

  get template () {
    return Template
  }

  onMove () {
    this.showProgressList()
  }
}
