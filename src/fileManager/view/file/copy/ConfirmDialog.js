const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftWidgetFormCheckboxElement = nrgsoft.widget.form.CheckboxElement,
    NrgsoftWidgetBaseHotkeyButton = nrgsoft.widget.base.HotkeyButton,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.fileManager.view.file.copy.ConfirmDialog = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
                
    get message() {
        return this._e2.nodeValue;
    }
                
    get _e3_() {
                
        if (!this.__e3_) {
            const
                properties = {"label":"overwrite files"},
                services = {
                    t: this.it.t
                };
            
            this.__e3_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.form.CheckboxElement, properties, services) :
                new nrgsoft.widget.form.CheckboxElement(properties, services);
        }

        return this.__e3_;
    
    }
                
    get overwrite() {
                return this._e3_.value;
    }
                
    get _e5_() {
                
        if (!this.__e5_) {
            const
                properties = {"content":"cancel","clickHotkey":"Esc"},
                services = {
                    t: this.it.t
                };
            
            this.__e5_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.HotkeyButton, properties, services) :
                new nrgsoft.widget.base.HotkeyButton(properties, services);
        }

        return this.__e5_;
    
    }
                
    get isActiveHotkeys() {
                return this._e5_.isActiveHotkeys;
    }
                
    get _e6_() {
                
        if (!this.__e6_) {
            const
                properties = {"content":"copy","clickHotkey":"Enter-Shift"},
                services = {
                    t: this.it.t
                };
            
            this.__e6_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.HotkeyButton, properties, services) :
                new nrgsoft.widget.base.HotkeyButton(properties, services);
        }

        return this.__e6_;
    
    }
                
    get _e7_() {
                
        if (!this.__e7_) {
            const
                properties = {"content":"move","clickHotkey":"Shift+Enter"},
                services = {
                    t: this.it.t
                };
            
            this.__e7_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.HotkeyButton, properties, services) :
                new nrgsoft.widget.base.HotkeyButton(properties, services);
        }

        return this.__e7_;
    
    }
    
    set message(value) {
        
        this._e2.nodeValue = this.it.t ? this.it.t(value) : value;
    }
    
    set overwrite(value) {
                this._e3_.value = value;
    }
    
    set isActiveHotkeys(value) {
                this._e5_.isActiveHotkeys = value;
                this._e6_.isActiveHotkeys = value;
                this._e7_.isActiveHotkeys = value;
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = document.createElement('div');
            
        this._e1.classList.add('nrgsoft-fileManager-view-file-copy-ConfirmDialog__message');
        this._element.appendChild(this._e1);
        this._e2 = document.createTextNode('');
        this._e1.appendChild(this._e2);
        this._e3 = this._e3_.element;
        this._element.appendChild(this._e3);
            
        this._e3.classList.add('nrgsoft-fileManager-view-file-copy-ConfirmDialog__overwrite');
        this._e4 = document.createElement('div');
            
        this._e4.classList.add('nrgsoft-fileManager-view-file-copy-ConfirmDialog__buttons');
        this._element.appendChild(this._e4);
        this._e5 = this._e5_.element;
        this._e4.appendChild(this._e5);
            
        for (const modifier of ["cancel"]) {
            this._e5.classList.add('nrgsoft-widget-base-HotkeyButton--' + modifier);
        }
        this._e6 = this._e6_.element;
        this._e4.appendChild(this._e6);
        this._e7 = this._e7_.element;
        this._e4.appendChild(this._e7);
            
    this._element.classList.add('nrgsoft-fileManager-view-file-copy-ConfirmDialog');
    
        super._constructor(properties);
        this.it.mapEvent(this._e5_, 'click', 'cancel');
        this.it.mapEvent(this._e6_, 'click', 'confirm');
        this.it.mapEvent(this._e7_, 'click', 'move');
    
    }
}
