const
  View = nrgsoft.fileManager.view.common.Bunch,
  Template = nrgsoft.fileManager.view.file.unzip.Tpl

nrgsoft.fileManager.view.file.Unzip = class extends View {

  get template () {
    return Template
  }

  onConfirm () {
    if (!this.form.hasErrors) {
      this.showProgressList()
    }
  }
}
