const
    Table = nrgsoft.widget.grid.Table,
    DisplayEntity = nrgsoft.fileManager.view.storage.list.grid.DisplayEntity;

nrgsoft.fileManager.view.storage.list.Grid = class extends Table {

    get traits() {
        return super.traits.concat([
            DisplayEntity
        ]);
    }

    set collection(collection) {
        this.data = collection.data;
    }
}
