const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftWidgetMenuDropdown = nrgsoft.widget.menu.Dropdown,
    NrgsoftWidgetBaseLink = nrgsoft.widget.base.Link,
    NrgsoftWidgetBaseHotkeyLink = nrgsoft.widget.base.HotkeyLink,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.fileManager.view.storage.list.Toolbar = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
                
    get _e1_() {
                
        if (!this.__e1_) {
            const
                properties = {},
                services = {
                    t: this.it.t
                };
            
            this.__e1_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.menu.Dropdown, properties, services) :
                new nrgsoft.widget.menu.Dropdown(properties, services);
        }

        return this.__e1_;
    
    }
                
    get _e2_() {
                
        if (!this.__e2_) {
            const
                properties = {"url":"\/file-manager\/storage\/local\/save","content":"local"},
                services = {
                    t: this.it.t
                };
            
            this.__e2_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.Link, properties, services) :
                new nrgsoft.widget.base.Link(properties, services);
        }

        return this.__e2_;
    
    }
                
    get _e3_() {
                
        if (!this.__e3_) {
            const
                properties = {"url":"\/file-manager\/storage\/ftp\/save","content":"ftp"},
                services = {
                    t: this.it.t
                };
            
            this.__e3_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.Link, properties, services) :
                new nrgsoft.widget.base.Link(properties, services);
        }

        return this.__e3_;
    
    }
                
    get _e4_() {
                
        if (!this.__e4_) {
            const
                properties = {"url":"\/file-manager\/storage\/sftp\/save","content":"sftp"},
                services = {
                    t: this.it.t
                };
            
            this.__e4_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.Link, properties, services) :
                new nrgsoft.widget.base.Link(properties, services);
        }

        return this.__e4_;
    
    }
                
    get _e5_() {
                
        if (!this.__e5_) {
            const
                properties = {"url":"\/file-manager\/storage\/delete","clickHotkey":"Delete","title":"delete"},
                services = {
                    t: this.it.t
                };
            
            this.__e5_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.HotkeyLink, properties, services) :
                new nrgsoft.widget.base.HotkeyLink(properties, services);
        }

        return this.__e5_;
    
    }
                
    get isActiveHotkeys() {
                return this._e5_.isActiveHotkeys;
    }
    
    set isActiveHotkeys(value) {
                this._e5_.isActiveHotkeys = value;
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = this._e1_.element;
        this._element.appendChild(this._e1);
            
        for (const modifier of ["plus"]) {
            this._e1.classList.add('nrgsoft-widget-menu-Dropdown--' + modifier);
        }
        this._e2 = this._e2_.element;
        this._e1_.trigger('appendChild', {child: this._e2_});
            
        this._e2.classList.add('nrgsoft-fileManager-view-storage-list-Toolbar__create-storage');
        this._e3 = this._e3_.element;
        this._e1_.trigger('appendChild', {child: this._e3_});
            
        this._e3.classList.add('nrgsoft-fileManager-view-storage-list-Toolbar__create-storage');
        this._e4 = this._e4_.element;
        this._e1_.trigger('appendChild', {child: this._e4_});
            
        this._e4.classList.add('nrgsoft-fileManager-view-storage-list-Toolbar__create-storage');
        this._e5 = this._e5_.element;
        this._element.appendChild(this._e5);
            
        this._e5.classList.add('nrgsoft-fileManager-view-storage-list-Toolbar__delete-item');
            
    this._element.classList.add('nrgsoft-fileManager-view-storage-list-Toolbar');
    
        super._constructor(properties);
        this.it.mapEvent(this._e5_, 'beforeGoTo', 'deleteItem');
    
    }
}
