const
  Observer = nrgsoft.sdk.rx.Observer,
  Link = nrgsoft.widget.base.Link,
  Selector = nrgsoft.widget.grid.table.RowSelector

nrgsoft.fileManager.view.storage.list.grid.DisplayEntity = class extends Observer {

  get itemSelector () {
    return this.owner.itemSelector
  }

  onBeforeDisplayColumn (event) {
    event.properties = this.createContents(event.properties)
  }

  createContents (entity) {
    return {
      selector: new Selector({
        className: 'storage-icon',
        itemSelector: this.itemSelector,
        entity: entity
      }),
      ...entity,
      createdAt: entity.createdAt.toLocaleString(),
      updatedAt: entity.updatedAt ? entity.updatedAt.toLocaleString() : ''
    }
  }
}
