const
  Component = nrgsoft.sdk.rx.Component,
  CheckboxElement = nrgsoft.widget.form.CheckboxElement,
  Template = nrgsoft.fileManager.view.storage.list.Tpl,
  SelectAll = nrgsoft.widget.grid.trait.SelectAll

nrgsoft.fileManager.view.storage.List = class extends Component {

  get traits () {
    return [
      Template,
      SelectAll
    ]
  }

  get itemSelector () {
    return this.grid.itemSelector
  }

  get properties () {
    return {
      isActiveWidget: {grid: 'isActiveHotkeys', toolbar: 'isActiveHotkeys'},
      fields: {grid: 'fields', head: 'fields'},
      collection: {grid: 'collection'},
      hasSelectedItems: {grid: 'itemSelector.hasSelected'},
      selectedItems: {grid: 'itemSelector.items'},
      hasActiveItem: {grid: 'hasActiveItem'},
      activeItem: {grid: 'activeItem'}
    }
  }

  get events () {
    return {
      deleteItem: {toolbar: 'deleteItem'}
    }
  }

  get fieldList () {
    return [
      {name: 'selector', content: this.selectorOfAll, sortable: false},
      {name: 'name', content: 'name', sortable: true},
      {name: 'type', content: 'type', sortable: true},
      {name: 'description', content: 'description', sortable: true},
      {name: 'createdAt', content: 'created', sortable: true},
      {name: 'updatedAt', content: 'updated', sortable: true}
    ]
  }

  set storages (storages) {
    this.fields = this.fieldList
    this.collection = storages
    this.selectableItems = storages.data

    this.activateWidget()
  }

  _constructor (...args) {
    this.selectorOfAll = new CheckboxElement()
    super._constructor(...args)
    this.grid.header = this.head
  }
}
