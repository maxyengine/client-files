const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftFileManagerViewLayoutMainMenuAlignLinks = nrgsoft.fileManager.view.layout.mainMenu.AlignLinks,
    NrgsoftWidgetBaseLink = nrgsoft.widget.base.Link,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.fileManager.view.layout.MainMenu = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.fileManager.view.layout.mainMenu.AlignLinks
        ];
    }
                
    get fileManagerLink() {
                
        if (!this._fileManagerLink) {
            const
                properties = {"url":"\/"},
                services = {
                    t: this.it.t
                };
            
            this._fileManagerLink = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.Link, properties, services) :
                new nrgsoft.widget.base.Link(properties, services);
        }

        return this._fileManagerLink;
    
    }
                
    get _e2_() {
                
        if (!this.__e2_) {
            const
                properties = {"url":"\/file-manager\/storage\/list"},
                services = {
                    t: this.it.t
                };
            
            this.__e2_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.Link, properties, services) :
                new nrgsoft.widget.base.Link(properties, services);
        }

        return this.__e2_;
    
    }
                
    get _e3_() {
                
        if (!this.__e3_) {
            const
                properties = {"url":"\/auth\/user\/logout"},
                services = {
                    t: this.it.t
                };
            
            this.__e3_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.Link, properties, services) :
                new nrgsoft.widget.base.Link(properties, services);
        }

        return this.__e3_;
    
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = this.fileManagerLink.element;
        this._element.appendChild(this._e1);
            
        this._e1.classList.add('nrgsoft-fileManager-view-layout-MainMenu__home');
        this._e2 = this._e2_.element;
        this._element.appendChild(this._e2);
            
        this._e2.classList.add('nrgsoft-fileManager-view-layout-MainMenu__storage-list');
        this._e3 = this._e3_.element;
        this._element.appendChild(this._e3);
            
        this._e3.classList.add('nrgsoft-fileManager-view-layout-MainMenu__logout');
            
    this._element.classList.add('nrgsoft-fileManager-view-layout-MainMenu');
    
        super._constructor(properties);
    
    }
}
