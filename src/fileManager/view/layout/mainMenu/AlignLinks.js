const
    Value = nrgsoft.sdk.lang.Value;

nrgsoft.fileManager.view.layout.mainMenu.AlignLinks = class extends Value {

    get assignments() {
        return [
            'alignLinks'
        ];
    }

    get mainMenu() {
        return this.owner;
    }

    alignLinks(params) {
        this.mainMenu.fileManagerLink.url = this.mainMenu.fileManagerLink.url.clone(params);
    }
}