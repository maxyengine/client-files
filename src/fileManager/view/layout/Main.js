const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftWidgetLayoutBehaviourInsert = nrgsoft.widget.layout.behaviour.Insert,
    NrgsoftFileManagerViewLayoutMainMenu = nrgsoft.fileManager.view.layout.MainMenu,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.fileManager.view.layout.Main = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.widget.layout.behaviour.Insert
        ];
    }
                
    get mainMenu() {
                
        if (!this._mainMenu) {
            const
                properties = {},
                services = {
                    t: this.it.t
                };
            
            this._mainMenu = this.injector ?
                this.injector.createObject(nrgsoft.fileManager.view.layout.MainMenu, properties, services) :
                new nrgsoft.fileManager.view.layout.MainMenu(properties, services);
        }

        return this._mainMenu;
    
    }
                
    get content() {
                return this._content;
    }
    
    set content(value) {
                this._content = Element.insert(this._e2, value);
                
        this.it.trigger('setContent', {value: value});
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = this.mainMenu.element;
        this._element.appendChild(this._e1);
        this._e2 = document.createElement('div');
        this._e2.setAttribute('class', 'content');
        this._element.appendChild(this._e2);
            
    this._element.classList.add('nrgsoft-fileManager-view-layout-Main');
    
        super._constructor(properties);
    
    }
}
