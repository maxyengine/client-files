const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftWidgetMenuDropdown = nrgsoft.widget.menu.Dropdown,
    NrgsoftWidgetBaseHotkeyLink = nrgsoft.widget.base.HotkeyLink,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.fileManager.view.directory.read.Toolbar = class extends nrgsoft.sdk.gui.Widget {
        
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
                
    get _e1_() {
                
        if (!this.__e1_) {
            const
                properties = {},
                services = {
                    t: this.it.t
                };
            
            this.__e1_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.menu.Dropdown, properties, services) :
                new nrgsoft.widget.menu.Dropdown(properties, services);
        }

        return this.__e1_;
    
    }
                
    get _e2_() {
                
        if (!this.__e2_) {
            const
                properties = {"clickHotkey":"Ctrl+d","url":"\/file-manager\/directory\/create","title":"Ctrl+d","content":"directory"},
                services = {
                    t: this.it.t
                };
            
            this.__e2_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.HotkeyLink, properties, services) :
                new nrgsoft.widget.base.HotkeyLink(properties, services);
        }

        return this.__e2_;
    
    }
                
    get isActiveHotkeys() {
                return this._e2_.isActiveHotkeys;
    }
                
    get newFile() {
                
        if (!this._newFile) {
            const
                properties = {"clickHotkey":"Ctrl+f","url":"\/text-editor","title":"Ctrl+f","content":"file"},
                services = {
                    t: this.it.t
                };
            
            this._newFile = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.HotkeyLink, properties, services) :
                new nrgsoft.widget.base.HotkeyLink(properties, services);
        }

        return this._newFile;
    
    }
                
    get _e4_() {
                
        if (!this.__e4_) {
            const
                properties = {"clickHotkey":"Ctrl+h","url":"\/file-manager\/hyperlink\/create","title":"Ctrl+h","content":"hyperlink"},
                services = {
                    t: this.it.t
                };
            
            this.__e4_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.HotkeyLink, properties, services) :
                new nrgsoft.widget.base.HotkeyLink(properties, services);
        }

        return this.__e4_;
    
    }
                
    get _e5_() {
                
        if (!this.__e5_) {
            const
                properties = {"clickHotkey":"Ctrl+c","url":"\/file-manager\/file\/copy","title":"copy (move)"},
                services = {
                    t: this.it.t
                };
            
            this.__e5_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.HotkeyLink, properties, services) :
                new nrgsoft.widget.base.HotkeyLink(properties, services);
        }

        return this.__e5_;
    
    }
                
    get uploadFile() {
                
        if (!this._uploadFile) {
            const
                properties = {"clickHotkey":"Ctrl+u","url":"\/file-uploader","title":"upload"},
                services = {
                    t: this.it.t
                };
            
            this._uploadFile = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.HotkeyLink, properties, services) :
                new nrgsoft.widget.base.HotkeyLink(properties, services);
        }

        return this._uploadFile;
    
    }
                
    get downloadFile() {
                
        if (!this._downloadFile) {
            const
                properties = {"clickHotkey":"Alt+d","url":"\/file-manager\/file\/download","title":"download"},
                services = {
                    t: this.it.t
                };
            
            this._downloadFile = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.HotkeyLink, properties, services) :
                new nrgsoft.widget.base.HotkeyLink(properties, services);
        }

        return this._downloadFile;
    
    }
                
    get zipFile() {
                
        if (!this._zipFile) {
            const
                properties = {"clickHotkey":"Ctrl+a","url":"\/file-manager\/file\/zip","title":"zip"},
                services = {
                    t: this.it.t
                };
            
            this._zipFile = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.HotkeyLink, properties, services) :
                new nrgsoft.widget.base.HotkeyLink(properties, services);
        }

        return this._zipFile;
    
    }
                
    get _e9_() {
                
        if (!this.__e9_) {
            const
                properties = {"clickHotkey":"Ctrl+r","url":"\/file-manager\/file\/rename","title":"rename"},
                services = {
                    t: this.it.t
                };
            
            this.__e9_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.HotkeyLink, properties, services) :
                new nrgsoft.widget.base.HotkeyLink(properties, services);
        }

        return this.__e9_;
    
    }
                
    get _e10_() {
                
        if (!this.__e10_) {
            const
                properties = {"clickHotkey":"Delete","url":"\/file-manager\/file\/delete","title":"delete"},
                services = {
                    t: this.it.t
                };
            
            this.__e10_ = this.injector ?
                this.injector.createObject(nrgsoft.widget.base.HotkeyLink, properties, services) :
                new nrgsoft.widget.base.HotkeyLink(properties, services);
        }

        return this.__e10_;
    
    }
                
    get storageList() {
                
        if (!this._storageList) {
            const
                properties = {},
                services = {
                    t: this.it.t
                };
            
            this._storageList = this.injector ?
                this.injector.createObject(nrgsoft.widget.menu.Dropdown, properties, services) :
                new nrgsoft.widget.menu.Dropdown(properties, services);
        }

        return this._storageList;
    
    }
    
    set isActiveHotkeys(value) {
                this._e2_.isActiveHotkeys = value;
                this.newFile.isActiveHotkeys = value;
                this._e4_.isActiveHotkeys = value;
                this._e5_.isActiveHotkeys = value;
                this.uploadFile.isActiveHotkeys = value;
                this.downloadFile.isActiveHotkeys = value;
                this.zipFile.isActiveHotkeys = value;
                this._e9_.isActiveHotkeys = value;
                this._e10_.isActiveHotkeys = value;
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = this._e1_.element;
        this._element.appendChild(this._e1);
            
        for (const modifier of ["plus"]) {
            this._e1.classList.add('nrgsoft-widget-menu-Dropdown--' + modifier);
        }
        this._e2 = this._e2_.element;
        this._e1_.trigger('appendChild', {child: this._e2_});
            
        this._e2.classList.add('nrgsoft-fileManager-view-directory-read-Toolbar__new-folder');
        this._e3 = this.newFile.element;
        this._e1_.trigger('appendChild', {child: this.newFile});
            
        this._e3.classList.add('nrgsoft-fileManager-view-directory-read-Toolbar__new-file');
        this._e4 = this._e4_.element;
        this._e1_.trigger('appendChild', {child: this._e4_});
            
        this._e4.classList.add('nrgsoft-fileManager-view-directory-read-Toolbar__new-hyperlink');
        this._e5 = this._e5_.element;
        this._element.appendChild(this._e5);
            
        this._e5.classList.add('nrgsoft-fileManager-view-directory-read-Toolbar__copy-file');
        this._e6 = this.uploadFile.element;
        this._element.appendChild(this._e6);
            
        this._e6.classList.add('nrgsoft-fileManager-view-directory-read-Toolbar__upload-file');
        this._e7 = this.downloadFile.element;
        this._element.appendChild(this._e7);
            
        this._e7.classList.add('nrgsoft-fileManager-view-directory-read-Toolbar__download-file');
        this._e8 = this.zipFile.element;
        this._element.appendChild(this._e8);
            
        this._e8.classList.add('nrgsoft-fileManager-view-directory-read-Toolbar__zip-file');
        this._e9 = this._e9_.element;
        this._element.appendChild(this._e9);
            
        this._e9.classList.add('nrgsoft-fileManager-view-directory-read-Toolbar__rename-file');
        this._e10 = this._e10_.element;
        this._element.appendChild(this._e10);
            
        this._e10.classList.add('nrgsoft-fileManager-view-directory-read-Toolbar__delete-file');
        this._e11 = this.storageList.element;
        this._element.appendChild(this._e11);
            
    this._element.classList.add('nrgsoft-fileManager-view-directory-read-Toolbar');
    
        super._constructor(properties);
        this.it.trigger('appendLink', {widget: this._e2_});
        this.it.mapEvent(this._e2_, 'beforeGoTo', 'createDirectory');
        this.it.trigger('appendLink', {widget: this.newFile});
        this.it.mapEvent(this.newFile, 'beforeGoTo', 'createFile');
        this.it.trigger('appendLink', {widget: this._e4_});
        this.it.mapEvent(this._e4_, 'beforeGoTo', 'createHyperlink');
        this.it.trigger('appendLink', {widget: this._e5_});
        this.it.mapEvent(this._e5_, 'beforeGoTo', 'copyFile');
        this.it.trigger('appendLink', {widget: this.uploadFile});
        this.it.mapEvent(this.uploadFile, 'beforeGoTo', 'uploadFile');
        this.it.trigger('appendLink', {widget: this.downloadFile});
        this.it.mapEvent(this.downloadFile, 'beforeGoTo', 'downloadFile');
        this.it.trigger('appendLink', {widget: this.zipFile});
        this.it.mapEvent(this.zipFile, 'beforeGoTo', 'zipFile');
        this.it.trigger('appendLink', {widget: this._e9_});
        this.it.mapEvent(this._e9_, 'beforeGoTo', 'renameFile');
        this.it.trigger('appendLink', {widget: this._e10_});
        this.it.mapEvent(this._e10_, 'beforeGoTo', 'deleteFile');
    
    }
}
