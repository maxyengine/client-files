const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftFileManagerDataBreadcrumbs = nrgsoft.fileManager.data.Breadcrumbs,
    NrgsoftSdkGuiWidgetAbility = nrgsoft.sdk.gui.WidgetAbility,
    NrgsoftSdkDataObserverVisualList = nrgsoft.sdk.data.observer.VisualList,
    NrgsoftWidgetBaseLink = nrgsoft.widget.base.Link;

nrgsoft.fileManager.view.directory.read.Breadcrumbs = class extends nrgsoft.fileManager.data.Breadcrumbs {
        
    get itemClass() {
        return nrgsoft.widget.base.Link;
    }
            
    static get services() {
        return {
            ...nrgsoft.fileManager.data.Breadcrumbs.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.sdk.gui.WidgetAbility,
			nrgsoft.sdk.data.observer.VisualList
        ];
    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
            
    this._element.classList.add('nrgsoft-fileManager-view-directory-read-Breadcrumbs');
    
        super._constructor(properties);
    
    }
}
