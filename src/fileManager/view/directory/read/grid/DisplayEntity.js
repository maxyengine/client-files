const
  Trigger = nrgsoft.sdk.rx.Trigger,
  Link = nrgsoft.widget.base.Link,
  Parent = nrgsoft.fileManager.entity.Parent,
  Selector = nrgsoft.widget.grid.table.RowSelector

nrgsoft.fileManager.view.directory.read.grid.DisplayEntity = class extends Trigger {

  get itemSelector () {
    return this.owner.itemSelector
  }

  onBeforeDisplayColumn (event) {
    event.properties = this.createContents(event.properties)
  }

  createContents (entity) {
    if (entity instanceof Parent) {
      return {
        name: this.createLink({
          url: entity.openUrl,
          content: '..',
          className: 'file-icon parent'
        })
      }
    }

    return {
      selector: new Selector({
        className: 'file-icon ' + entity.path.fileName.extension.toLowerCase(),
        itemSelector: this.itemSelector,
        entity: entity
      }),
      name: this.createLink({
        url: entity.openUrl,
        content: entity.path.fileName.baseName,
        onBeforeGoTo: ({params, body}) => {
          this.trigger('beforeOpen', {entity, params, body})
        }
      }),
      extension: entity.path.fileName.extension,
      size: entity.size || '',
      permissions: '',
      lastModified: entity.lastModified ? entity.lastModified.toLocaleString() : ''
    }
  }

  createLink (properties) {
    return this.injector.createObject(Link, properties)
  }
}
