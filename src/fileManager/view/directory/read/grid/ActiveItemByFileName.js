const
    Observer = nrgsoft.sdk.rx.Observer;

nrgsoft.fileManager.view.directory.read.grid.ActiveItemByFileName = class extends Observer {

    onAddItem(event) {
        if (!this.owner.fileName) {
            return;
        }

        const entity = this.owner.getRaw(event.index);

        if (entity.path.fileName.isEqual(this.owner.fileName)) {
            this.owner.activateItem(event.index);
        }
    }
}
