const
    Table = nrgsoft.widget.grid.Table,
    DisplayEntity = nrgsoft.fileManager.view.directory.read.grid.DisplayEntity,
    ActiveItemByFileName = nrgsoft.fileManager.view.directory.read.grid.ActiveItemByFileName;

nrgsoft.fileManager.view.directory.read.Grid = class extends Table {

    get traits() {
        return super.traits.concat([
            DisplayEntity,
            ActiveItemByFileName
        ]);
    }
}
