const
    Element = nrgsoft.sdk.gui.utility.Element,
    NrgsoftSdkGuiTriggerHotkeys = nrgsoft.sdk.gui.trigger.Hotkeys,
    NrgsoftSdkGuiWidget = nrgsoft.sdk.gui.Widget;

nrgsoft.fileManager.view.directory.DoubleRead = class extends nrgsoft.sdk.gui.Widget {
        
    get hotkeys() {
        return {
        switchToLeft: `Ctrl+37`,
        switchToRight: `Ctrl+39`
     };
    }
            
    static get services() {
        return {
            ...nrgsoft.sdk.gui.Widget.services || {},
            
        };
    }
            
    get traits() {
        return [
            ...super.traits || [],
            nrgsoft.sdk.gui.trigger.Hotkeys
        ];
    }
                
    get left() {
                return this._e2;
    }
                
    get inactiveLeft() {
                
    return this._e2.classList.contains('nrgsoft-fileManager-view-directory-DoubleRead__panel--inactive');

    }
                
    get activeLeft() {
                
    return !this.inactiveLeft;

    }
                
    get right() {
                return this._e3;
    }
                
    get inactiveRight() {
                
    return this._e3.classList.contains('nrgsoft-fileManager-view-directory-DoubleRead__panel--inactive');

    }
                
    get activeRight() {
                
    return !this.inactiveRight;

    }
    
    set inactiveLeft(value) {
                
        if (value) {
            this._e2.classList.add('nrgsoft-fileManager-view-directory-DoubleRead__panel--inactive');
        } else {
            this._e2.classList.remove('nrgsoft-fileManager-view-directory-DoubleRead__panel--inactive');
        }

    }
    
    set activeLeft(value) {
                
    this.inactiveLeft = !value;

    }
    
    set inactiveRight(value) {
                
        if (value) {
            this._e3.classList.add('nrgsoft-fileManager-view-directory-DoubleRead__panel--inactive');
        } else {
            this._e3.classList.remove('nrgsoft-fileManager-view-directory-DoubleRead__panel--inactive');
        }

    }
    
    set activeRight(value) {
                
    this.inactiveRight = !value;

    }
    
    get it() {
        return this.owner || this;
    }
    
    _constructor(properties) {
        this._element = document.createElement('div');
        this._e1 = document.createElement('div');
            
        this._e1.classList.add('nrgsoft-fileManager-view-directory-DoubleRead__panels');
        this._element.appendChild(this._e1);
        this._e2 = document.createElement('div');
            
        this._e2.classList.add('nrgsoft-fileManager-view-directory-DoubleRead__panel');
        this._e1.appendChild(this._e2);
        this._e3 = document.createElement('div');
            
        this._e3.classList.add('nrgsoft-fileManager-view-directory-DoubleRead__panel');
        this._e1.appendChild(this._e3);
            
    this._element.classList.add('nrgsoft-fileManager-view-directory-DoubleRead');
    
        super._constructor(properties);
        this.inactiveLeft = !!this.inactiveLeft; 
        this.inactiveRight = !!this.inactiveRight; 
    
    }
}
