const
  Component = nrgsoft.sdk.rx.Component,
  Template = nrgsoft.fileManager.view.directory.read.Tpl,
  SelectAll = nrgsoft.widget.grid.trait.SelectAll,
  CheckboxElement = nrgsoft.widget.form.CheckboxElement,
  FileFactory = nrgsoft.fileManager.entity.FileFactory

const path = Symbol()

nrgsoft.fileManager.view.directory.Read = class extends Component {

  static get services () {
    return {
      router: 'router',
      fileFactory: FileFactory
    }
  }

  get traits () {
    return [
      Template,
      SelectAll
    ]
  }

  get itemSelector () {
    return this.body.itemSelector
  }

  get properties () {
    return {
      isActiveWidget: {body: 'isActiveHotkeys', toolbar: 'isActiveHotkeys'},
      urlParams: {toolbar: 'urlParams'},
      fields: {head: 'fields', body: 'fields'},
      fileName: {body: 'fileName'},
      wrapper: {body: 'wrapper'},
      data: {body: 'data'},
      itemSelector: {body: 'itemSelector'},
      hasSelectedItems: {body: 'itemSelector.hasSelected'},
      selectedItems: {body: 'itemSelector.items'},
      hasActiveItem: {body: 'hasActiveItem'},
      activeItem: {body: 'activeItem'},
      storageList: {toolbar: 'storageList'}
    }
  }

  get events () {
    return {
      createDirectory: {toolbar: 'createDirectory'},
      createFile: {toolbar: 'createFile'},
      createHyperlink: {toolbar: 'createHyperlink'},
      copyFile: {toolbar: 'copyFile'},
      uploadFile: {toolbar: 'uploadFile'},
      downloadFile: {toolbar: 'downloadFile'},
      zipFile: {toolbar: 'zipFile'},
      renameFile: {toolbar: 'renameFile'},
      deleteFile: {toolbar: 'deleteFile'},
      beforeOpen: {body: 'beforeOpen'}
    }
  }

  get fieldList () {
    return [
      {name: 'selector', content: this.selectorOfAll, sortable: false},
      {name: 'name', content: 'name', sortable: true},
      {name: 'extension', content: 'type', sortable: true},
      {name: 'size', content: 'size', sortable: true},
      {name: 'lastModified', content: 'modified', sortable: true}
    ]
  }

  set path (value) {
    this[path] = value
    this.breadcrumbs.path = value
  }

  set storages ({data}) {
    this.storageList.data = data.map(storage => {
      if (storage.id === this[path].storageId) {
        this.storageList.caption = storage.name
      }
      return {
        url: this.router.createUrl('/file-manager', {path: `${storage.id}://`}),
        content: storage.name
      }
    })
  }

  set directory (directory) {
    this.selectorOfAll.value = false
    this.selectableItems = directory.children

    this.fields = this.fieldList
    this.data = this.createData(directory)
  }

  _constructor (...args) {
    this.selectorOfAll = new CheckboxElement()
    super._constructor(...args)
    this.body.header = this.head
  }

  createData (directory) {
    return directory.path.isRoot ? directory.children :
      [this.fileFactory.createParent(directory)].concat(directory.children)
  }
}
