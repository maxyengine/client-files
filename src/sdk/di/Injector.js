const
  Obj = nrgsoft.sdk.lang.utility.Object,
  Str = nrgsoft.sdk.lang.utility.String,
  Type = nrgsoft.sdk.lang.utility.Type,
  Definition = nrgsoft.sdk.lang.Definition

nrgsoft.sdk.di.Injector = class {

  constructor () {
    this.definitions = new Map()
    this.nonameInstances = new Set()

    this.setService('injector', this)
  }

  createObject (className, properties = {}, services = {}) {
    services.injector = this

    for (const [alias, definition] of Obj.toMap(className.services || {})) {
      if (services[alias]) {
        continue
      }

      if (!this.hasService(definition)) {
        throw new Error(Str.sprintf('Service \'%s\' was not found.', alias))
      }

      services[alias] = this.getService(definition)
    }

    return new className(properties, services)
  }

  loadServices (definitions = {}) {
    for (const [alias, definition] of Obj.toMap(definitions)) {
      this.setService(alias, definition)
    }

    return this
  }

  instanceServices (...names) {
    names.forEach(name => {
      this.getService(name)
    })

    return this
  }

  hasService (name) {
    return (Type.isString(name) && this.definitions.has(name)) ||
      Type.isClass(name) ||
      (Type.isArray(name) && Type.isClass(name[0]))
  }

  setService (name, definition) {
    this.definitions.set(name, new Definition(definition))

    return this
  }

  getService (name) {
    if (Type.isString(name)) {
      return this.definitions.get(name).getInstance(this)
    }

    const needle = new Definition(name)

    for (let [key, definition] of this.definitions) {
      if (needle.isEqual(definition)) {
        return this.getService(key)
      }
    }

    for (let nonameInstance of this.nonameInstances) {
      if (needle.isEqual(nonameInstance)) {
        return nonameInstance
      }
    }

    const instance = needle.getInstance(this)
    this.nonameInstances.add(instance)

    return instance
  }
}