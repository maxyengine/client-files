const
    Arr = nrgsoft.sdk.lang.utility.Array,
    Str = nrgsoft.sdk.lang.utility.String;

const self = nrgsoft.sdk.lang.utility.Type = class {

    static isSet(value) {
        return null !== value && undefined !== value;
    }

    static isEmpty(value) {
        return !self.isSet(value) ||
            (self.isString(value) && Str.isEmpty(value)) ||
            (self.isArray(value) && Arr.isEmpty(value));
    }

    static isAnyLetter(value) {
        return /^[А-ЯЁA-Z ]+$/i.test(value);
    }

    static isString(value) {
        return self.isSet(value) && (typeof value === 'string' || value instanceof String);
    }

    static isInteger(value) {
        return self.isSet(value) && (Number(value) === value || value % 1 === 0);
    }

    static isFloat(value) {
        return self.isSet(value) && (Number(value) === value || value % 1 !== 0);
    }

    static isNumber(value) {
        return self.isSet(value) && (typeof value === 'number' || value instanceof Number);
    }

    static isBoolean(value) {
        return self.isSet(value) && (typeof value === 'boolean' || value instanceof Boolean);
    }

    static isScalar(value) {
        return self.isString(value) || self.isNumber(value) || self.isBoolean(value);
    }

    static isObject(value, strict = true) {
        return self.isSet(value) && typeof value === 'object' && (!strict || !self.isArray(value));
    }

    static isClass(value) {
        return typeof value === 'function';
    }

    static isFunction(value) {
        return self.isSet(value) && typeof value === 'function';
    }

    static isArray(value) {
        return Array.isArray ? Array.isArray(value) : Object.prototype.toString.call(value) === '[object Array]';
    }

    static isDOMElement(value) {
        return value && typeof value === "object" && value.nodeType === 1 && typeof value.nodeName === "string";
    }

    static isDOMNode(value) {
        return value && typeof value === "object" && Arr.has([1, 3, 8], value.nodeType) && typeof value.nodeName === "string";
    }

    static isIterable(value) {
        return typeof value[Symbol.iterator] === 'function';
    }

    static arrayToObject(arr, recursively) {
        const obj = {};

        arr.forEach((value, i) => {
            if (recursively && self.isArray(value)) {
                value = self.arrayToObject(value, recursively);
            }
            obj[i] = value;
        });

        return obj;
    }

    static objectToArray(obj, recursively) {
        const arr = [];

        for (let i in obj) {
            if (obj.hasOwnProperty(i)) {
                if (recursively && self.isObject(obj[i])) {
                    obj[i] = self.objectToArray(obj[i], recursively);
                }
                arr.push(obj[i]);
            }
        }

        return arr;
    }
}
