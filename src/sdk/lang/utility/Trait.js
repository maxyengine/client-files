const
    Type = nrgsoft.sdk.lang.utility.Type,
    Definition = nrgsoft.sdk.lang.Definition;

const self = nrgsoft.sdk.lang.utility.Trait = class {

    static use(trait, owner) {
        const instance = self.createInstance(trait, owner);

        if (instance.assignments) {
            owner.mapProperties(self.extractAssignments(instance), instance);
        }
    }

    static extractAssignments(trait) {
        let assignments = trait.assignments;

        if (Type.isArray(trait.assignments)) {
            assignments = {};
            trait.assignments.forEach(assignment => {
                assignments[assignment] = assignment;
            });
        }

        return assignments;
    }

    static createInstance(definition, owner) {
        return new Definition(definition, {}, {owner: owner}).getInstance(owner.injector);
    }
}
