const self = nrgsoft.sdk.lang.utility.Number = class {

    static get maxSafeInteger() {
        return Number.MAX_SAFE_INTEGER || 9007199254740991;
    }
}
