const
  Type = nrgsoft.sdk.lang.utility.Type

const self = nrgsoft.sdk.lang.utility.Object = class {

  static toMap (object) {
    return new Map(Object.entries(object))
  }

  static isEqual (a, b) {
    const
      aProps = Object.getOwnPropertyNames(a),
      bProps = Object.getOwnPropertyNames(b)

    if (aProps.length !== bProps.length) {
      return false
    }

    for (const property of aProps) {
      if (Type.isObject(a[property])) {
        if (!Type.isObject(b[property])) {
          return false
        }
        if (!self.isEqual(a[property], b[property])) {
          return false
        }
        continue
      }

      if (a[property] !== b[property]) {
        return false
      }
    }

    return true
  }

  static contains (what, where) {
    const whatProps = Object.getOwnPropertyNames(what)

    for (const property of whatProps) {
      if (Type.isObject(what[property])) {
        if (!Type.isObject(where[property]) || !self.contains(what[property], where[property])) {
          return false
        }
        continue
      }

      if (what[property] !== where[property]) {
        return false
      }
    }

    return true
  }

  static filterUndefined (object) {
    const filtred = {}

    for (const [key, value] of self.toMap(object)) {
      if (undefined !== value) {
        filtred[key] = value
      }
    }

    return filtred
  }
}
