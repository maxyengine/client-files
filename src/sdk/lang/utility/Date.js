self = nrgsoft.sdk.lang.utility.Date = class {

    static createDateOnMonthAhead(date) {
        const month = date.getMonth();
        date = new Date(date.getTime());
        date.setMonth(date.getMonth() + 1);
        if (date.getMonth() === month + 2) {
            date.setDate(0);
        }

        return date;
    }

    static createMonthWeeks(year, month, fromMonday) {
        const
            date = new Date(year, month, 1),
            delta = fromMonday ? (date.getDay() === 0 ? 6 : date.getDay() - 1) : date.getDay(),
            i = 0,
            j = 0,
            weeks = [];
        date.setDate(date.getDate() - delta);
        while (6 > i++) {
            const week = [];
            while (7 > j++) {
                week.push(new Date(date.getTime()));
                date.setDate(date.getDate() + 1);
            }
            weeks.push(week);
            j = 0;
        }

        return weeks;
    }

    static createYearMonths(year, fromMonday) {
        const
            months = [],
            month = -1;
        while (11 > month++) {
            months.push(self.createMonthWeeks(year, month, fromMonday));
        }

        return months;
    }
}
