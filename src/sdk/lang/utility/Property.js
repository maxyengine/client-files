const
    Type = nrgsoft.sdk.lang.utility.Type,
    Obj = nrgsoft.sdk.lang.utility.Object;

nrgsoft.sdk.lang.utility.Property = class {

    static stringify(properties) {
        let strings = properties;

        if (Type.isString(properties)) {
            strings = [properties];
        } else if (Type.isObject(properties)) {
            strings = [];
            for (const [component, prop] of Obj.toMap(properties)) {
                strings.push(component + '.' + prop);
            }
        }

        return strings;
    }

    static parse(string, object) {
        const arr = string.split('.');

        let
            context = object,
            propName = arr[0],
            value = context[propName];

        for (let i = 1; i < arr.length; i++) {
            context = value;
            propName = arr[i];
            value = context[propName];
        }

        return {
            context: context,
            name: propName,
            value: value
        };
    }
}
