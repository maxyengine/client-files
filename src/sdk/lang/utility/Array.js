const self = nrgsoft.sdk.lang.utility.Array = class {

    static has(list, item) {
        return list.indexOf(item) > -1;
    }

    static remove(list, item) {
        self.removeByIndex(list, self.getIndex(list, item));
    }

    static removeByIndex(list, index) {
        list.splice(index, 1);
    }

    static getIndex(list, item) {
        return list.indexOf(item);
    }

    static isEmpty(list) {
        return !list.length;
    }
}
