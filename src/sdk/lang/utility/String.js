const self = nrgsoft.sdk.lang.utility.String = class {

  static capitalize (string) {
    return self.upperCaseFirst(string)
  }

  static sprintf (string, ...args) {
    let i = 0
    return string.replace(/%((%)|s|d)/g, function (m) {
      let val = null
      if (m[2]) {
        val = m[2]
      } else {
        val = args[i]
        switch (m) {
          case '%d':
            val = parseFloat(val)
            if (isNaN(val)) {
              val = 0
            }
            break
          default:
        }
        i++
      }
      return val
    })
  };

  static contains (string, substring, caseSensitive = true) {
    return caseSensitive ?
      string.indexOf(substring) >= 0 :
      string.toLocaleLowerCase().indexOf(substring.toLocaleLowerCase()) >= 0
  }

  static lowerCaseFirst (string) {
    return string.charAt(0).toLocaleLowerCase() + string.slice(1)
  }

  static upperCaseFirst (string) {
    return string.charAt(0).toLocaleUpperCase() + string.slice(1)
  }

  static trim (string) {
    return string.replace(/^\s+|\s+$/gm, '')
  }

  static isEmpty (string) {
    return '' === string
  }

  static sort (string) {
    return string
      .split('')
      .sort((a, b) => {
        if (a < b) {
          return -1
        }
        if (a > b) {
          return 1
        }

        return 0
      })
      .join('')
  }
}
