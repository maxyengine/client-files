const
    Obj = nrgsoft.sdk.lang.utility.Object,
    Type = nrgsoft.sdk.lang.utility.Type,
    PropertyMapper = nrgsoft.sdk.lang.PropertyMapper;

const propertyMapper = Symbol();

nrgsoft.sdk.lang.Value = class {

    constructor(properties = {}, services = {}) {
        this.set(services);
        this[propertyMapper] = new PropertyMapper(this);

        this._constructor(properties);
    }

    _constructor(properties) {
        this
            .mapProperties(this.properties || {})
            .set({...this.defaults || {}, ...properties || {}})
        ;

        if (this.initialize) {
            this.initialize();
        }
    }

    set(property, value) {
        if (Type.isObject(property)) {
            for (const [prop, value] of Obj.toMap(property)) {
                this.set(prop, value);
            }
        } else {
            this[property] = value;
        }

        return this;
    }

    mapProperties(properties, context = this) {
        for (let [name, strings] of Obj.toMap(properties)) {
            this.mapProperty(name, strings, context);
        }

        return this;
    }

    mapProperty(name, properties, context = this) {
        return this[propertyMapper].map(name, properties, context);
    }
}
