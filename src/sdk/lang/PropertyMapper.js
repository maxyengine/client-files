const
    Type = nrgsoft.sdk.lang.utility.Type,
    Property = nrgsoft.sdk.lang.utility.Property;

const
    object = Symbol(),
    map = Symbol();

nrgsoft.sdk.lang.PropertyMapper = class {

    constructor(obj) {
        this[object] = obj;
        this[map] = new Map();
    }

    map(name, properties, context = this[object]) {
        const isDefined = this[map].has(name);

        if (!isDefined) {
            this[map].set(name, new Set());
        }

        const
            strings = Property.stringify(properties),
            set = this[map].get(name);

        for (const string of strings) {
            set.add(Property.parse(string, context));
        }

        if (isDefined) {
            return this[object];
        }

        const first = Property.parse(strings[0], context);

        if (Type.isFunction(first.value)) {
            this[object][name] = (...args) => {
                let result = undefined;

                for (const prop of set) {
                    result = prop.value.call(prop.context, ...args);
                }

                return result;
            };
        } else {
            Object.defineProperty(this[object], name, {
                get: () => {
                    return first.context[first.name];
                },
                set: value => {
                    for (const prop of set) {
                        prop.context[prop.name] = value;
                    }
                }
            });
        }

        return this[object];
    }
}
