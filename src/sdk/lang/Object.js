const
  Value = nrgsoft.sdk.lang.Value,
  Trait = nrgsoft.sdk.lang.utility.Trait

const traitInstances = Symbol()

nrgsoft.sdk.lang.Object = class extends Value {

  _constructor (...args) {
    this[traitInstances] = new Set()
    this.use(...this.traits || [])

    super._constructor(...args)
  }

  use (...traits) {
    traits.forEach(trait => {
      if (this.hasTrait(trait)) {
        throw new Error('Object already uses this trait.')
      }
      this[traitInstances].add(trait)

      Trait.use(trait, this)
    })

    return this
  }

  hasTrait (trait) {
    return this[traitInstances].has(trait)
  }
}
