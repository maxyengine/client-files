const
    Component = nrgsoft.sdk.rx.Component,
    Type = nrgsoft.sdk.lang.utility.Type,
    Element = nrgsoft.sdk.gui.utility.Element;

nrgsoft.sdk.gui.Widget = class extends Component {

    get element() {
        if (!this._element) {
            this._element = document.createElement(this.tagName || 'div');
        }

        return this._element;
    }

    set className(className) {
        this.element.className = className;
    }

    get classList() {
        return this.element.classList;
    }

    constructor(tagName = 'div', properties = {}, services = {}) {
        if (Type.isString(tagName)) {
            properties.tagName = tagName;
        } else if (Type.isDOMElement(tagName)) {
            properties._element = tagName;
        } else {
            services = properties;
            properties = tagName;
        }

        super(properties, services);
    }

    insert(child) {
        return Element.insert(this, child);
    }

    append(child) {
        return Element.append(this, child);
    }

    appendTo(parent) {
        return Element.append(parent, this);
    }

    remove(child) {
        return Element.remove(this, child);
    }

    removeAll() {
        return Element.clear(this);
    }

    contains(child) {
        return Element.contains(this, child);
    }

    appendBefore(child, before) {
        return Element.appendBefore(this, child, before);
    }
}
