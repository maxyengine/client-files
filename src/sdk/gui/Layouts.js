const
    Component = nrgsoft.sdk.rx.Component,
    Widget = nrgsoft.sdk.gui.Widget,
    Element = nrgsoft.sdk.gui.utility.Element,
    Definition = nrgsoft.sdk.lang.Definition;

const
    wrapper = Symbol(),
    layouts = Symbol(),
    current = Symbol();

nrgsoft.sdk.gui.Layouts = class extends Component {

    get defaults() {
        return {
            minHeight: 500
        };
    }

    get wrapper() {
        return this[wrapper];
    }

    set wrapper(element) {
        let wrapElement = element;

        if (document.body !== element) {
            const clientHeight = element.clientHeight || 0;

            wrapElement = Element.insert(element, document.createElement('div'));
            wrapElement.style.position = 'relative';
            wrapElement.style.height = clientHeight >= this.minHeight ? clientHeight + 'px' : this.minHeight + 'px';
        }

        this[wrapper] = new Widget(wrapElement);
    }

    _constructor(...args) {
        this[layouts] = new Map();

        super._constructor(...args);
    }

    getLayout(def) {
        const
            definition = new Definition(def),
            className = definition.className;

        if (!this[layouts].has(className)) {
            this[layouts].set(className, definition);
        }

        const layout = this[layouts].get(className).getInstance(this.injector);

        if (this[current] !== className) {
            this[current] = className;
            layout.show(this[wrapper]);
        }

        return layout;
    }
}