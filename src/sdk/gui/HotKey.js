const
    Arr = nrgsoft.sdk.lang.utility.Array,
    Type = nrgsoft.sdk.lang.utility.Type,
    Str = nrgsoft.sdk.lang.utility.String;

const keys = Symbol();

const self = nrgsoft.sdk.gui.Hotkey = class {

    constructor(hotkey) {
        if (Type.isNumber(hotkey)) {
            hotkey = hotkey.toString();
        }

        this[keys] = hotkey.split('+');
    }

    isRecognized(keyboardEvent) {
        for (let i = 0; this[keys][i]; i++) {
            let key = this[keys][i];

            if (self.isSpecialKey(key)) {
                if (!keyboardEvent[key.toLocaleLowerCase() + 'Key']) {
                    return false;
                }
                continue;
            }

            if (Str.contains(key, '-')) {
                let exceptKeys = key.split('-');
                key = exceptKeys.shift();
                for (let j = 0; exceptKeys[j]; j++) {
                    let exceptKey = exceptKeys[j];
                    if (!self.isSpecialKey(exceptKey) || keyboardEvent[exceptKey.toLocaleLowerCase() + 'Key']) {
                        return false;
                    }
                }
            }

            if (keyboardEvent.keyCode !== self.codeFromKey(key)) {
                return false;
            }
        }

        return true;
    }

    static isSpecialKey(key) {
        return Arr.has([
            'alt',
            'ctrl',
            'shift',
            'meta',
            'access'
        ], key.toLocaleLowerCase());
    }

    static codeFromKey(key) {
        if (parseInt(key) == key) {
            return parseInt(key);
        }

        switch (key.toLocaleLowerCase()) {
            case 'enter':
                return 13;
            case 'esc':
                return 27;
            case 'delete':
                return 46;
            case 'tab':
                return 9;
        }

        return key.toLocaleUpperCase().charCodeAt(0);
    }
}
