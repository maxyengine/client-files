const
  Value = nrgsoft.sdk.lang.Value

const self = nrgsoft.sdk.gui.trigger.WidgetActivity = class extends Value {

  static get instances () {
    if (!self._instances) {
      self._instances = []
    }

    return self._instances
  }

  get assignments () {
    return [
      'activateWidget',
      'deactivateWidget'
    ]
  }

  get element () {
    return this.owner.element
  }

  initialize () {
    self.instances.push(this.owner)

    this.element.addEventListener('click', () => {
      this.activateWidget()
    }, true)

    document.addEventListener('click', event => {
      if (!this.element.contains(event.target)) {
        this.deactivateWidget()
      }
    }, true)
  }

  activateWidget (force = false) {
    if (this.owner.isActiveWidget && !force) {
      return
    }

    this.owner.isActiveWidget = true
    this.owner.trigger('activateWidget', {widget: this.owner})

    self.instances.forEach(instance => {
      if (instance !== this.owner) {
        instance.deactivateWidget()
      }
    })
  }

  deactivateWidget () {
    if (!this.owner.isActiveWidget) {
      return
    }

    this.owner.isActiveWidget = false
    this.owner.trigger('deactivateWidget')
  }
}
