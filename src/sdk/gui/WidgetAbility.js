const
    Widget = nrgsoft.sdk.gui.Widget;

nrgsoft.sdk.gui.WidgetAbility = class extends Widget {

    get assignments() {
        return [
            'element',
            'classList',
            'className',
            'insert',
            'append',
            'appendTo',
            'remove',
            'removeAll',
            'contains',
            'appendBefore'
        ];
    }

    get properties() {
        return {
            _element: {owner: '_element'},
            tagName: {owner: 'tagName'}
        };
    }
}
