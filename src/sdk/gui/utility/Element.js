const
  Type = nrgsoft.sdk.lang.utility.Type

const self = nrgsoft.sdk.gui.utility.Element = class {

  static append (to, what) {
    const
      toElement = self.getElement(to),
      whatElement = self.getElement(what)

    toElement.appendChild(whatElement)

    return what
  }

  static appendBefore (to, what, before) {
    const
      toElement = self.getElement(to),
      whatElement = self.getElement(what),
      beforeElement = self.getElement(before)

    toElement.insertBefore(whatElement, beforeElement)

    return what
  }

  static insert (to, what) {
    self.clear(to)

    return self.append(to, what)
  }

  static remove (from, what) {
    const
      fromElement = self.getElement(from),
      whatElement = self.getElement(what)

    fromElement.removeChild(whatElement)

    return what
  }

  static clear (component) {
    const element = self.getElement(component)

    while (element.hasChildNodes()) {
      element.removeChild(element.firstChild)
    }

    return component
  }

  static contains (where, what) {
    const
      whereElement = self.getElement(where),
      whatElement = self.getElement(what)

    return whereElement.contains(whatElement)
  }

  static getElement (component) {
    if (!Type.isSet(component)) {
      return document.createTextNode('')
    }

    if (Type.isDOMNode(component)) {
      return component
    }

    return component.element || document.createTextNode(component)
  }
}