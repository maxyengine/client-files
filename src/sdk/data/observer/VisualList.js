const
  Observer = nrgsoft.sdk.rx.Observer,
  Element = nrgsoft.sdk.gui.utility.Element

nrgsoft.sdk.data.observer.VisualList = class extends Observer {

  get list () {
    return this.owner;
  }

  get visualList () {
    return this.list.visualList || this.list
  }

  onClear () {
    Element.clear(this.visualList)
  }

  onAddItem (event) {
    Element.append(this.visualList, event.item)
  }

  onRemoveItem (event) {
    Element.remove(this.visualList, event.item)
  }

  onBeforeCreateItem (event) {
    event.services.t = this.owner.t
  }

  onAppendChild({child}) {
    this.list.appendItem(child)
  }
}
