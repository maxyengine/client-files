const
  Component = nrgsoft.sdk.rx.Component,
  Definition = nrgsoft.sdk.lang.Definition,
  Arr = nrgsoft.sdk.lang.utility.Array

const
  data = Symbol(),
  items = Symbol()

nrgsoft.sdk.data.List = class extends Component {

  set data (data) {
    this.clear()

    data.forEach(properties => {
      this.addItem(properties)
    })

    this.trigger('setData')
  }

  get length () {
    return this[items].length
  }

  get isEmpty () {
    return !this.length
  }

  get lastIndex () {
    return this.length - 1
  }

  get lastItem () {
    return this.getItem(this.lastIndex)
  }

  _constructor (...args) {
    this[data] = []
    this[items] = []

    super._constructor(...args)
  }

  getRaw (index, byDefault) {
    return this[data][index] || byDefault
  }

  getItem (index, byDefault) {
    return this[items][index] || byDefault
  }

  getItemByRaw (raw) {
    const index = Arr.getIndex(this[data], raw)

    return this.getItem(index)
  }

  addItem (raw = {}) {
    this[data].push(raw)

    const event = {
      item: this.createItem(raw),
      index: this.length
    }

    this[items].push(event.item)

    this.trigger('addItem', event)

    return event.item
  }

  appendItem (item) {
    this[data].push({})
    this[items].push(item)

    this.trigger('addItem', {
      item: item,
      index: this.lastIndex
    })

    return item
  }

  removeItem (item) {
    const index = Arr.getIndex(this[items], item)

    Arr.removeByIndex(this[items], index)
    Arr.removeByIndex(this[data], index)

    this.trigger('removeItem', {item: item})

    if (this.isEmpty) {
      this.clear()
    }

    return this
  }

  clear () {
    this.trigger('beforeClear')

    this[data] = []
    this[items] = []

    this.trigger('clear')

    return this
  }

  createItem (properties) {
    const event = {
      itemClass: this.itemClass,
      properties: properties,
      services: {}
    }

    this.trigger('beforeCreateItem', event)

    return new Definition(event.itemClass, event.properties, event.services).getInstance(this.injector)
  }

  [Symbol.iterator] () {
    return this[items].entries()
  }
}
