const
    Component = nrgsoft.sdk.rx.Component;

nrgsoft.sdk.data.ProgressBar = class extends Component {

    show() {
        this.classList.remove('d-none');
    }

    hide() {
        this.classList.add('d-none');
    }

    update(percent) {
        this.progress.style.width = percent + '%';
    }

    resetProgress() {
        this.update(0);
    }
}
