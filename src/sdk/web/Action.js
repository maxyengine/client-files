const
  Component = nrgsoft.sdk.rx.Component,
  TranslatorAbility = nrgsoft.sdk.i18n.TranslatorAbility,
  WidgetActivity = nrgsoft.sdk.gui.trigger.WidgetActivity,
  Layout = nrgsoft.widget.layout.Default

nrgsoft.sdk.web.Action = class extends Component {

  static get services () {
    return {
      controller: 'controller',
      client: 'client',
      layouts: 'layouts',
      router: 'router',
      translator: 'translator'
    }
  }

  get traits () {
    return [
      [TranslatorAbility, {dictionaries: this.dictionaries}]
    ]
  }

  get dictionaries () {
    return [
      'nrgsoft.sdk.form'
    ]
  }

  get layout () {
    return this.layouts.getLayout(Layout)
  }

  createView (viewClass, properties = {}) {
    return this.injector
      .createObject(viewClass, properties, {t: this.t})
      .use(WidgetActivity)
      .on(this)
  }

  createWidget (widgetClass, properties = {}) {
    return this.injector.createObject(widgetClass, properties, {t: this.t})
  }

  goTo (...args) {
    this.router.goTo(...args)
  }

  refresh (...args) {
    this.router.refresh(...args)
  }

  createUrl (...args) {
    return this.router.createUrl(...args)
  }

  executeAction (...args) {
    return this.controller.executeAction(...args)
  }
}
