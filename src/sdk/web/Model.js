const
    TypeObject = nrgsoft.sdk.lang.Object;

nrgsoft.sdk.web.Model = class extends TypeObject {

    static get services() {
        return {
            client: 'client'
        };
    }

    fetch(...args) {
        return this.client.fetch(...args);
    }

    upload(...args) {
        return this.client.upload(...args);
    }
}
