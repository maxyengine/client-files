const
  Query = nrgsoft.sdk.web.utility.Query,
  Obj = nrgsoft.sdk.lang.utility.Object,
  Str = nrgsoft.sdk.lang.utility.String

const self = nrgsoft.sdk.web.Url = class {

  constructor (href, baseUrl) {
    this.options = {}

    if (self.isRelative(href) && baseUrl) {
      baseUrl = baseUrl instanceof self ? baseUrl : new this.constructor(baseUrl)
      this.path = '/' + href.split('?')[0].replace(/^\/|\/$/g, '')
      this.basePath = baseUrl.path
      href = baseUrl.baseHref + '/' + href.replace(/^\/|\/$/g, '')
    }

    const link = document.createElement('a')
    link.href = href

    if (!this.path) {
      this.path = '/' + link.pathname.replace(/^\/|\/$/g, '')
    }

    this.protocol = link.protocol && link.protocol.slice(0, -1)
    this.hostname = link.hostname
    this.port = link.port ? parseInt(link.port) : 80
    this.search = link.search && link.search.slice(1)
    this.hash = link.hash && link.hash.slice(1)
  }

  static isRelative (href) {
    return !self.isAbsolute(href)
  }

  static isAbsolute (href) {
    return /^(f|ht)tps?:\/\//i.test(href)
  }

  get baseHref () {
    let str = ''

    if (this.protocol) {
      str += this.protocol + '://'
    }
    if (this.hostname) {
      str += this.hostname
    }
    if (this.port && 80 !== this.port) {
      str += ':' + this.port
    }
    if (this.basePath) {
      str += this.basePath
    }

    return str.replace(/\/$/g, '')
  }

  get hrefBeforeSearch () {
    return this.baseHref + this.path
  }

  get href () {
    let str = this.hrefBeforeSearch

    if (this.search) {
      str += '?' + this.search
    }
    if (this.hash) {
      str += '#' + this.hash
    }

    return str
  }

  get isRoot () {
    return '/' === this.path
  }

  get params () {
    const params = {}
    if (this.search) {
      Query.parse(this.search, params)
    }

    return params
  }

  set params (params) {
    this.search = Query.stringify(params)
  }

  getParam (name, byDefault = null) {
    return this.params[name] || byDefault
  }

  setParam (name, value) {
    const params = {}
    params[name] = value

    this.mergeParams(params)

    return this
  }

  unsetParam (name) {
    if (this.params[name]) {
      delete this.params[name]
    }

    return this
  }

  mergeParams (params) {
    this.params = {...this.params, ...params}

    return this
  }

  isEqual (url) {
    if (!(url instanceof self)) {
      url = new self(url)
    }

    return this.hrefBeforeSearch === url.hrefBeforeSearch &&
      Obj.isEqual(this.params, url.params) &&
      this.hash === url.hash
  }

  contains (url) {
    if (!(url instanceof self)) {
      url = new self(url)
    }

    return this.hrefBeforeSearch === url.hrefBeforeSearch &&
      Obj.contains(url.params, this.params) &&
      this.hash === url.hash
  }

  toString () {
    return this.href
  }

  clone (params) {
    const baseHref = this.baseHref

    const clonedUrl = new this.constructor(this.href.substr(baseHref.length), baseHref)

    if (params) {
      clonedUrl.params = params
    }

    return clonedUrl
  }
}
