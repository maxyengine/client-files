const
    Observable = nrgsoft.sdk.rx.Observable;

const observable = Symbol();

nrgsoft.sdk.web.jsonClient.Uploader = class {

    constructor(http, body) {
        this.http = http;
        this.body = body;
        this[observable] = new Observable();

        http.upload.onprogress = (...args) => {
            this[observable].notifyObservers('progress', ...args);
        };

        http.onload = (...args) => {
            this[observable].notifyObservers('load', JSON.parse(http.responseText), ...args);
        };

        http.onerror = (...args) => {
            this[observable].notifyObservers('error', ...args);
        };
    }

    onProgress(observer) {
        this[observable].addObserver('progress', observer);

        return this;
    }

    onLoad(observer) {
        this[observable].addObserver('load', observer);

        return this;
    }

    onError(observer) {
        this[observable].addObserver('error', observer);

        return this;
    }

    start() {
        this.http.send(this.body);
    }
}