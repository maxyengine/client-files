const
  Component = nrgsoft.sdk.rx.Component,
  Obj = nrgsoft.sdk.lang.utility.Object,
  Definition = nrgsoft.sdk.lang.Definition

const actions = Symbol()

nrgsoft.sdk.web.Controller = class extends Component {

  static get services () {
    return {
      router: 'router'
    }
  }

  _constructor (...args) {
    this[actions] = new Map()
    this.router.on(this)

    super._constructor(...args)
  }

  set actions (actions) {
    for (const [href, action] of Obj.toMap(actions)) {
      this.setAction(href, action)
    }
  }

  setAction (href, action) {
    let
      actionUrl = this.router.createUrl(href),
      actionDefinition = new Definition(action)

    for (const [, definition] of this[actions]) {
      if (definition.isEqual(actionDefinition)) {
        actionDefinition = definition
        break
      }
    }

    this[actions].set(actionUrl, actionDefinition)

    return this
  }

  getAction (byUrl) {
    for (const [url, action] of this[actions]) {
      if (byUrl.contains(url)) {
        return action.getInstance(this.injector)
      }
    }
  }

  executeAction (event) {
    const action = this.getAction(event.url)

    if (action) {
      return action.execute({body: {}, ...event})
    }
  }

  onGoTo (event) {
    this.executeAction(event)
  }

  goTo (...args) {
    this.router.goTo(...args)
  }
}
