const
  Component = nrgsoft.sdk.rx.Component,
  Url = nrgsoft.sdk.web.Url

const
  url = Symbol(),
  setUrl = Symbol(),
  saveState = Symbol()

const self = nrgsoft.sdk.web.Router = class extends Component {

  get url () {
    return this[url]
  }

  initialize () {
    window.onpopstate = event => {
      this[setUrl](event.state ? event.state.href : window.location.href)
    }
  }

  goTo (url, params, body) {
    this[setUrl](url, params, body)

    this[saveState]()
  }

  refresh (params) {
    this.goTo(this[url], params)
  }

  createUrl (href, params) {
    if (Url.isAbsolute(href)) {
      if (this.rootUrl) {
        href = href.substr(this.rootUrl.href.length)
      }
    } else if (this[url] && href[0] !== '/') {
      href = this[url].path.substr(0, this[url].path.lastIndexOf('/')) + '/' + href
    }

    return new Url(href, this.rootUrl).mergeParams(params || {})
  }

  [setUrl] (href, params, body) {
    params = params || {}
    body = body || {}
    const pageWasReloaded = !this[url]

    this[url] = href instanceof Url ? href.mergeParams(params) : this.createUrl(href, params)

    this.trigger('goTo', {url: this[url], body, pageWasReloaded})
  }

  [saveState] () {
    const event = {url: this[url]}

    this.trigger('saveState', event)

    if (!window.history.state || !event.url.isEqual(window.history.state.href)) {
      window.history.pushState({href: event.url.href}, '', event.url.href)
    }
  }
}
