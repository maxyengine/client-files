const
  Component = nrgsoft.sdk.rx.Component,
  Url = nrgsoft.sdk.web.Url,
  ValidationError = nrgsoft.sdk.web.exception.ValidationError,
  Uploader = nrgsoft.sdk.web.jsonClient.Uploader

const self = nrgsoft.sdk.web.JsonClient = class extends Component {

  static get status () {
    return {
      NO_CONTENT: 204,
      UNPROCESSABLE_ENTITY: 422
    }
  }

  async fetch (url, request = {}) {
    request.headers = request.headers || new Headers()
    request.headers.set('Content-Type', 'application/json; charset=utf-8')
    request.method = 'POST'
    request.body = JSON.stringify(request.body)

    const response = await fetch(this.createUrl(url), request)

    if (response.ok) {
      switch (response.status) {
        case self.status.NO_CONTENT:
          return
        default:
          return response.json()
      }
    }

    switch (response.status) {
      case self.status.UNPROCESSABLE_ENTITY:
        throw new ValidationError(await response.json())
      default:
        throw await response.json()
    }
  }

  upload (url, request = {}) {
    const http = new XMLHttpRequest()
    http.open('POST', this.createUrl(url), true)

    if (request.headers) {
      for (const [name, value] of request.headers) {
        http.setRequestHeader(name, value)
      }
    }

    return new Uploader(http, request.body)
  }

  /*    get(url, request) {
          url = this.createUrl(url);

          if (request.params) {
              url.mergeParams(request.params);
          }

          request.method = 'GET';
          url = this.createUrl(url);

          return fetch(url, request);
      }*/

  createUrl (url) {
    if (url instanceof Url) {
      return url
    }

    if (Url.isAbsolute(url)) {
      return new Url(url)
    }

    return new Url(url, this.apiUrl)
  }
}
