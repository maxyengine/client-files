const self = nrgsoft.sdk.web.utility.Cookie = class {

    static enabled() {
        return navigator.cookieEnabled;
    }

    static set(name, value, options = {}) {
        if (!self.enabled()) {
            return;
        }

        if (options.expires) {
            options.expires = options.expires.toUTCString();
        }

        let cookie = name + '=' + value + ';';
        for (let key in options) {
            cookie += (key !== 'secure' ? key + '=' + options[key]: key) + ';';
        }

        document.cookie = cookie;
    }

    static get(name, byDefault = null) {
        if (!self.enabled()) {
            return byDefault;
        }

        const matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));

        return matches ? matches[1] : byDefault;
    }

    static remove(name, options = {}) {
        if (!self.enabled()) {
            return;
        }

        self.set(name, '', {
            ...options,
            expires: new Date(Date.now() - 1000)
        });
    }
}
