const
    Str = nrgsoft.sdk.lang.utility.String;

nrgsoft.sdk.i18n.Message = class {

    constructor(text, ...params) {
        this.text = text;
        this.params = params;
    }

    toString() {
        return Str.sprintf(this.text, ...this.params);
    }
}