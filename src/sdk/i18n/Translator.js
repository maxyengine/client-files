const
    TypeObject = nrgsoft.sdk.lang.Object;

nrgsoft.sdk.i18n.Translator = class extends TypeObject {

    get canTranslate() {
        return this.locale &&
            this.i18n &&
            this.i18n.texts &&
            this.i18n.texts[this.locale];
    }

    translate(text, ...dictionaries) {
        if (!this.canTranslate) {
            return text;
        }

        const locale = this.i18n.texts[this.locale];

        for (let i = 0; dictionaries[i]; i++) {
            if (locale[dictionaries[i]][text]) {
                return locale[dictionaries[i]][text];
            }
        }

        for (let dictionary in locale) {
            if (locale.hasOwnProperty(dictionary)) {
                if (locale[dictionary][text]) {
                    return locale[dictionary][text];
                }
            }
        }

        return text;
    }
}