const
    TypeObject = nrgsoft.sdk.lang.Object,
    Str = nrgsoft.sdk.lang.utility.String,
    Type = nrgsoft.sdk.lang.utility.Type,
    Message = nrgsoft.sdk.i18n.Message;

nrgsoft.sdk.i18n.TranslatorAbility = class extends TypeObject {

    get assignments() {
        return [
            't',
        ];
    }

    get translator() {
        return this.owner.translator;
    }

    t(message, ...params) {
        if (message instanceof Message) {
            params = message.params;
            message = message.text;
        }

        if (!Type.isString(message)) {
            return message;
        }

        const translation = this.translator ?
            this.translator.translate(message, ...this.dictionaries || []) :
            message;

        return Str.sprintf(translation, ...params);
    }
}