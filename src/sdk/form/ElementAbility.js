const
    Component = nrgsoft.sdk.rx.Component,
    Message = nrgsoft.sdk.i18n.Message;

const
    filters = Symbol(),
    validators = Symbol();

nrgsoft.sdk.form.ElementAbility = class extends Component {

    _constructor(...args) {
        this[filters] = new Set();
        this[validators] = new Set();

        super._constructor(...args);
    }

    get assignments() {
        return [
            'hasError',
            'filters',
            'validators',
            'addFilter',
            'addValidator',
            'filter',
            'resetValue',
            'reset',
            'toString'
        ];
    }

    get properties() {
        return {
            errorMessage: 'owner.errorMessage',
            isValid: 'owner.isValid',
            value: 'owner.value'
        }
    }

    get hasError() {
        try {
            const value = this.filter();
            this.validate(value);
            this.errorMessage = null;

            return false;
        } catch (e) {
            if (e instanceof Message) {
                this.errorMessage = this.owner.t ? this.owner.t(e) : e.toString();

                return true;
            } else {
                throw e;
            }
        }
    }

    initialize() {
        this.owner.on(this);
    }

    onSetErrorMessage(event) {
        this.isValid = undefined === event.value ? undefined : !event.value;
    }

    addFilter(filter) {
        this.filters.add(filter);

        return this.owner
    }

    addValidator(validator) {
        this.validators.add(validator);

        return this.owner
    }

    filter() {
        let value = this.value;

        for (const filter of this.filters) {
            value = filter.apply(value);
        }

        return value;
    }

    validate(value) {
        for (const validator of this.validators) {
            if (!validator.isValid(value)) {
                throw validator.errorMessage;
            }
        }
    }

    resetValue() {
        this.value = '';
    }

    reset() {
        this.resetValue();
        this.errorMessage = undefined;
        this.isValid = undefined;
    }

    toString() {
        return this.value;
    }
}
