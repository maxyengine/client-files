const
    Observer = nrgsoft.sdk.rx.Observer;

nrgsoft.sdk.form.observer.OnAppendElement = class extends Observer {

    get form() {
        return this.owner;
    }

    onAppendElement(event) {
        this.form.addElement(event.widget);
    }
}
