const
    Str = nrgsoft.sdk.lang.utility.String;

nrgsoft.sdk.form.filter.Integer = class {

    apply(value) {
        const intValue = parseInt(value);

        return intValue.toString() === value ? intValue : value;
    }
}
