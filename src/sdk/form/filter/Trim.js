const
    Str = nrgsoft.sdk.lang.utility.String;

nrgsoft.sdk.form.filter.Trim = class {

    apply(value) {
        return Str.trim(value);
    }
}
