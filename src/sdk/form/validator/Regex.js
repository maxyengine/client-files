const
    TypeObject = nrgsoft.sdk.lang.Object,
    Type = nrgsoft.sdk.lang.utility.Type,
    Message = nrgsoft.sdk.i18n.Message;

nrgsoft.sdk.form.validator.Regex = class extends TypeObject {

    get defaults() {
        return {
            errorText: 'must validate against \'%s\''
        };
    }

    set pattern(pattern) {
        this._pattern = pattern;
    }

    get pattern() {
        return this._pattern;
    }

    isValid(value) {
        if (Type.isEmpty(value)) {
            return true;
        }

        this.errorMessage = new Message(this.errorText, this.pattern);

        return Type.isString(value) && this.pattern.exec(value);
    }
}
