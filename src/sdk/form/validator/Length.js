const
    TypeObject = nrgsoft.sdk.lang.Object,
    Type = nrgsoft.sdk.lang.utility.Type,
    Num = nrgsoft.sdk.lang.utility.Number,
    Str = nrgsoft.sdk.lang.utility.String,
    Message = nrgsoft.sdk.i18n.Message;

nrgsoft.sdk.form.validator.Length = class extends TypeObject {

    get defaults() {
        return {
            errorTextInvalidLength: 'must have a length between %d and %d',
            errorTextInvalidEqualLength: 'length must be equal %d',
            _min: 0,
            _max: Num.maxSafeInteger,
            _inclusive: true,
        };
    }

    set min(value) {
        if (!Type.isInteger(value) || value < 0) {
            throw new Error('The min value must be a positive integer.');
        }

        this._min = value;
    }

    set max(value) {
        if (!Type.isInteger(value)) {
            throw new Error('The max value must be a positive integer.');
        }

        if (value > Num.maxSafeInteger) {
            throw new Error(Str.sprintf('The max value must be less than %d.', Num.maxSafeInteger));
        }

        this._max = value;
    }

    set inclusive(inclusive) {
        if (!Type.isBoolean(inclusive)) {
            throw new Error('The inclusive value must be boolean.');
        }

        this._inclusive = inclusive;
    }

    validateMin(length) {
        if (this._inclusive) {
            return length.length >= this._min;
        }

        return length > this._min;
    }

    validateMax(length) {
        if (this._inclusive) {
            return length.length <= this._max;
        }

        return length < this._max;
    }

    isValid(value) {
        if (Type.isEmpty(value)) {
            return true;
        }

        if (!(this.validateMin(value) && this.validateMax(value))) {
            if (this._min === this._max) {
                this.errorMessage = new Message(this.errorTextInvalidEqualLength, this._min);
            } else {
                this.errorMessage = new Message(this.errorTextInvalidLength, this._min, this._max);
            }

            return false;
        }

        return true;
    }
}
