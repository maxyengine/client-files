const
    TypeObject = nrgsoft.sdk.lang.Object,
    Type = nrgsoft.sdk.lang.utility.Type,
    Message = nrgsoft.sdk.i18n.Message;

nrgsoft.sdk.form.validator.ValueTrue = class extends TypeObject {

    get defaults() {
        return {
            notStringErrorText: 'value is not considered as "True"'
        }
    }

    isValid(value) {
        if (Type.isEmpty(value)) {
            return true;
        }

        if (true !== value) {
            this.errorMessage = new Message(this.notStringErrorText);

            return false;
        }

        return true;
    }
}
