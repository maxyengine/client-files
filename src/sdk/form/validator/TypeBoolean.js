const
    TypeObject = nrgsoft.sdk.lang.Object,
    Type = nrgsoft.sdk.lang.utility.Type,
    Message = nrgsoft.sdk.i18n.Message;

nrgsoft.sdk.form.validator.TypeBoolean = class extends TypeObject {

    get defaults() {
        return {
            notStringErrorText: 'please provide a valid boolean'
        }
    }

    isValid(value) {
        if (Type.isEmpty(value)) {
            return true;
        }

        if (!Type.isBoolean(value)) {
            this.errorMessage = new Message(this.notStringErrorText);

            return false;
        }

        return true;
    }
}
