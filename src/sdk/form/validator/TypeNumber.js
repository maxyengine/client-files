const
    TypeObject = nrgsoft.sdk.lang.Object,
    Type = nrgsoft.sdk.lang.utility.Type,
    Message = nrgsoft.sdk.i18n.Message;

nrgsoft.sdk.form.validator.TypeNumber = class extends TypeObject {

    get defaults() {
        return {
            notIntegerErrorText: 'please provide a valid number'
        }
    }

    isValid(value) {
        if (Type.isEmpty(value)) {
            return true;
        }

        if (isNaN(value)) {
            this.errorMessage = new Message(this.notIntegerErrorText);

            return false;
        }

        return true;
    }
}
