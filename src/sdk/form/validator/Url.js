const
    TypeObject = nrgsoft.sdk.lang.Object,
    Type = nrgsoft.sdk.lang.utility.Type,
    Message = nrgsoft.sdk.i18n.Message;

const self = nrgsoft.sdk.form.validator.Url = class extends TypeObject {

    get defaults() {
        return {
            notUrlErrorText: 'please provide a valid URL'
        }
    }

    isValid(value) {
        if (Type.isEmpty(value)) {
            return true;
        }

        const regexp = /^(?:(?:https?|ftp):\/\/)?(?:(?!(?:10|127)(?:\.\d{1,3}){3})(?!(?:169\.254|192\.168)(?:\.\d{1,3}){2})(?!172\.(?:1[6-9]|2\d|3[0-1])(?:\.\d{1,3}){2})(?:[1-9]\d?|1\d\d|2[01]\d|22[0-3])(?:\.(?:1?\d{1,2}|2[0-4]\d|25[0-5])){2}(?:\.(?:[1-9]\d?|1\d\d|2[0-4]\d|25[0-4]))|(?:(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)(?:\.(?:[a-z\u00a1-\uffff0-9]-*)*[a-z\u00a1-\uffff0-9]+)*(?:\.(?:[a-z\u00a1-\uffff]{2,})))(?::\d{2,5})?(?:\/\S*)?$/;

        if (!regexp.test(value)) {
            this.errorMessage = new Message(this.notUrlErrorText);

            return false;
        }

        return true;
    }
}
