const
    TypeObject = nrgsoft.sdk.lang.Object,
    Type = nrgsoft.sdk.lang.utility.Type,
    Message = nrgsoft.sdk.i18n.Message;

nrgsoft.sdk.form.validator.TypeString = class extends TypeObject {

    get defaults() {
        return {
            notStringErrorText: 'please provide a valid string'
        }
    }

    isValid(value) {
        if (Type.isEmpty(value)) {
            return true;
        }

        if (!Type.isString(value)) {
            this.errorMessage = new Message(this.notStringErrorText);

            return false;
        }

        return true;
    }
}
