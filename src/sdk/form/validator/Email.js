const
    TypeObject = nrgsoft.sdk.lang.Object,
    Type = nrgsoft.sdk.lang.utility.Type,
    Message = nrgsoft.sdk.i18n.Message;

const self = nrgsoft.sdk.form.validator.Email = class extends TypeObject {

    get defaults() {
        return {
            errorText: 'please provide a valid email'
        };
    }

    static get pattern() {
        return /^[a-z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+(?:\.[a-z0-9\u007F-\uffff!#$%&'*+\/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z]{2,}$/i;
    }

    isValid(value) {
        if (Type.isEmpty(value)) {
            return true;
        }

        this.errorMessage = new Message(this.errorText);

        return Type.isString(value) && self.pattern.exec(value);
    }
}
