const
    TypeObject = nrgsoft.sdk.lang.Object,
    Type = nrgsoft.sdk.lang.utility.Type,
    Arr = nrgsoft.sdk.lang.utility.Array,
    Message = nrgsoft.sdk.i18n.Message;

nrgsoft.sdk.form.validator.Required = class extends TypeObject {

    get defaults() {
        return {
            errorText: 'please fill out this field',
            _restrictions: [null, '', undefined, 'undefined']
        };
    }

    set restrictions(data) {
        this._restrictions = this._restrictions.concat(data);
    }

    get restrictions() {
        return this._restrictions;
    }

    isValid(value) {
        this.errorMessage = new Message(this.errorText);

        if (Type.isArray(value)) {
            return !Arr.isEmpty(value);
        }
        for (let i = 0; i < this.restrictions.length; i++) {
            if (value === this.restrictions[i]) {
                return false;
            }
        }

        return true;
    }
}
