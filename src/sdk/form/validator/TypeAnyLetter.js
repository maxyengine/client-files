const
    TypeObject = nrgsoft.sdk.lang.Object,
    Type = nrgsoft.sdk.lang.utility.Type,
    Message = nrgsoft.sdk.i18n.Message;

nrgsoft.sdk.form.validator.TypeAnyLetter = class extends TypeObject {

    get defaults() {
        return {
            notStringErrorText: 'please provide a string'
        }
    }

    isValid(value) {
        if (Type.isEmpty(value)) {
            return true;
        }

        if (!Type.isAnyLetter(value)) {
            this.errorMessage = new Message(this.notStringErrorText);

            return false;
        }

        return true;
    }
}
