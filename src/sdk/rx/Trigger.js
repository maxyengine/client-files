const
    Observer = nrgsoft.sdk.rx.Observer;

nrgsoft.sdk.rx.Trigger = class extends Observer {

    on(...args) {
        return this.owner.on(...args);
    }

    trigger(...args) {
        this.owner.trigger(...args);
    }

    mapEvent(...args) {
        return this.owner.mapEvent(...args);
    }
}
