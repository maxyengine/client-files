const
  TypeObject = nrgsoft.sdk.lang.Object,
  Obj = nrgsoft.sdk.lang.utility.Object,
  Property = nrgsoft.sdk.lang.utility.Property,
  Observable = nrgsoft.sdk.rx.Observable,
  Observer = nrgsoft.sdk.rx.Observer,
  Event = nrgsoft.sdk.rx.utility.Event

const observable = Symbol()

nrgsoft.sdk.rx.Component = class extends TypeObject {

  _constructor (...args) {
    this[observable] = new Observable()
    this.on(this)

    super._constructor(...args)

    this.mapEvents(this.events || {})
  }

  on (...args) {
    this[observable].addObserver(...args)

    return this
  }

  off (...args) {
    this[observable].removeObserver(...args)

    return this
  }

  trigger (eventName, event, context = this, ...args) {
    this[observable].notifyObservers(eventName, event, context, ...args)
  }

  mapEvents (events) {
    for (const [toEventName, properties] of Obj.toMap(events)) {
      for (const [property, fromEventName] of Obj.toMap(properties)) {
        const component = Property.parse(property, this).value

        this.mapEvent(component, fromEventName, toEventName)
      }
    }

    return this
  }

  mapEvent (component, fromEventName, toEventName, eventFilter) {
    Event.map(this, component, fromEventName, toEventName, eventFilter)

    return this
  }

  set (property, value) {
    return Observer.isEventHandlerName(property) ?
      this.on(Observer.makeEventName(property), value) :
      super.set(property, value)
  }
}
