const
    Str = nrgsoft.sdk.lang.utility.String,
    TypeObject = nrgsoft.sdk.lang.Object;

nrgsoft.sdk.rx.Observer = class extends TypeObject {

    static makeEventHandlerName(eventName) {
        return 'on' + Str.capitalize(eventName);
    }

    static makeEventName(handlerName) {
        return Str.lowerCaseFirst(handlerName.substr(2));
    }

    static isEventHandlerName(name) {
        return name.length > 2 && name.substr(0, 2) === 'on' &&
            name.charAt(2) === name.charAt(2).toUpperCase();
    }

    get observeFor() {
        return this.owner;
    }

    initialize() {
        this.observeFor.on(this);
    }
}
