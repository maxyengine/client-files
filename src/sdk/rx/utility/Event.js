const
  Type = nrgsoft.sdk.lang.utility.Type

const self = nrgsoft.sdk.rx.utility.Event = class {

  static map (object, component, fromEventName, toEventName) {
    toEventName = toEventName || fromEventName

    if (Type.isArray(component)) {
      component.forEach(item => {
        self.map(object, item, fromEventName, toEventName)
      })
    } else if (component instanceof nrgsoft.sdk.rx.Component) {
      component.on(fromEventName, (event, ...contexts) => {
        object.trigger(toEventName, event, object, ...contexts)
      })
    } else {
      component.addEventListener(fromEventName, event => {
        object.trigger(toEventName, event, object, component)
      })
    }
  }
}
