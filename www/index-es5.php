<?php

function rootUrl()
{
    return sprintf("%s://%s%s",
        isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http',
        $_SERVER['SERVER_NAME'],
        str_replace('\\', '/', dirname($_SERVER['PHP_SELF']))
    );
}

$rootUrl = rootUrl();
$apiUrl = 'http://localhost/nrgsoft.pro/api-files/www';

?><!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?= $rootUrl; ?>/favicon.ico"/>
    <link href="<?= $rootUrl; ?>/styles/app.css" rel="stylesheet">
    <title>NrgSoft</title>
</head>
<body>
<script src="<?= $rootUrl; ?>/js/polyfill.js"></script>
<script src="<?= $rootUrl; ?>/js/fetch.js"></script>
<script src="<?= $rootUrl; ?>/js/app-es5.js"></script>
<script type="text/javascript">

    var app = new nrgsoft.fileManager.Application({
        rootUrl: '<?= $rootUrl ?>',
        apiUrl: '<?= $apiUrl ?>'
    });

    app.run();

</script>
</body>
</html>
