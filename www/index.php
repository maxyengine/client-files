<?php

$mode = 'es6';

switch ($mode) {
    case 'es5':
        require __DIR__.'/index-es5.php';
        break;
    default:
        require __DIR__.'/index-es6.php';
}