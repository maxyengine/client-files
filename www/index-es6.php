<?php

function buildApp($withCompile = true)
{
    $nrgsoft = realpath(__DIR__ . '/../vendor/bin/nrgsoft');
    $path = realpath(__DIR__ . '/../');
    $result = realpath(__DIR__ . '/js') . '/app.js';
    $command = "{$nrgsoft} build -path {$path} -result {$result}" . ($withCompile ? '' : ' --without-compile');

    exec($command, $out);

    if (!empty($out)) {
        echo "<b>{$command}</b>";
        var_dump($out);
        exit;
    }
}

function rootUrl()
{
    return sprintf("%s://%s%s",
        isset($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' ? 'https' : 'http',
        $_SERVER['SERVER_NAME'],
        str_replace('\\', '/', dirname($_SERVER['PHP_SELF']))
    );
}

buildApp();

$rootUrl = rootUrl();
$apiUrl = 'http://localhost/nrgsoft.pro/api-files/www';

?><!doctype html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="shortcut icon" href="<?= $rootUrl; ?>/favicon.ico"/>
    <link href="<?= $rootUrl; ?>/styles/app.css" rel="stylesheet">
    <title>NrgSoft</title>
</head>
<body>

<br>
<br>
<br>


<script src="<?= $rootUrl; ?>/js/app.js"></script>
<script type="text/javascript">

    var wrapper = document.getElementById('wrapper');



    // file-manager

    new nrgsoft.fileManager.Application({rootUrl: '<?= $rootUrl ?>', apiUrl: '<?= $apiUrl ?>'}).run();
    //new nrgsoft.fileManager.Application({rootUrl: '<?= $rootUrl ?>', apiUrl: '<?= $apiUrl ?>', wrapper: wrapper}).run();

    // text-editor
    //new nrgsoft.textEditor.Application({rootUrl: '<?//= $rootUrl ?>//', apiUrl: '<?//= $apiUrl ?>//'}).run();
    //new nrgsoft.textEditor.Application({rootUrl: '<?//= $rootUrl ?>//', apiUrl: '<?//= $apiUrl ?>//', wrapper: wrapper}).run();

    // file-uploader
    //new nrgsoft.fileUploader.Application({rootUrl: '<?//= $rootUrl ?>//', apiUrl: '<?//= $apiUrl ?>//'}).run();
    //new nrgsoft.fileUploader.Application({rootUrl: '<?//= $rootUrl ?>//', apiUrl: '<?//= $apiUrl ?>//', wrapper: wrapper}).run();

</script>
</body>
</html>
